$('#btn-submit').on('click', function(e){
	e.preventDefault();

	var form = $("form");
	var url  = $(form).prop('action');
	var data = $(form).serializeArray();

	$.ajax({
		url:url,
		type:'POST',
		data:data,
		dataType: 'json',
		success:function(result,status,xhr){
			if (result.error) {
				Swal.fire('Error!',result.error,'error');
			} else {
				window.location.replace(result.source);
			}
		},
		error:function(xhr,status,error){
			Swal.fire('Error!','Terjadi kesalahan pada sistem!','error');
		}
	});
});