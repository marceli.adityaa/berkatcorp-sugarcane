$(document).ready(function () {
    $(document).ajaxStart(function () {
        $(".ajaxloader").fadeIn(150);
    });
    $(document).ajaxComplete(function () {
        $(".ajaxloader").fadeOut(150);
    });
    $("form").on("submit", function (e) {
        loading_button('.btn-submit');
        $('.btn-submit').attr('disabled', true);
    });
    
});

$('.numeric').keyup(function (e) {
    if (/\D/g.test(this.value)) {
        this.value = this.value.replace(/\D/g, '');
    }
});

function reload_page() {
    window.location.reload();
}

$(window).load(function () {
    $(".preloader").fadeOut("slow");
});

function loading_button(element) {
    var $this = $(element);
    $this.data('original-text', $(element).html());
    $this.html('<i class="fa fa-circle-o-notch fa-spin"></i> loading..');
}


$('.modal:not(.modal-protected-on-close)').on('hidden.bs.modal', function () {
    $(this).find('form').trigger('reset');
    $(this).find('form [type=hidden]').val('');
    $(this).find('.is-invalid').removeClass('is-invalid');
});

// INISIASI PLUGINS //
$(".table-js").bootstrapTable();
$(".datetime").datetimepicker({ autoclose: true, todayHighlight: true });

// $("[type=time]").prop('type', 'text').clockpicker({
//     donetext: 'Selesai',
//     autoclose: true
// });

const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 3000,
    onBeforeOpen: function () {
        setTimeout(1000);
    }
});

function reformat_number(value, row, index, field){
    return index+1;
}