-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2019 at 09:30 AM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `berkatcorp-sugarcane`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `bank` varchar(100) NOT NULL,
  `no_rek` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `bank`, `no_rek`, `nama`, `status`) VALUES
(1, 'BCA', '1233211231', 'PT. BAS', 1),
(2, 'Mandiri', '1111111', 'CV. BFI', 1),
(3, 'BRI', '010101010101011', 'Samuel Boy', 1);

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `id` int(11) NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `nama_trx` varchar(100) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `is_paid` tinyint(2) NOT NULL,
  `is_paid_remark` text NOT NULL,
  `is_paid_by` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`id`, `id_lokasi`, `id_periode`, `tgl_transaksi`, `nama_trx`, `kuantitas`, `harga`, `subtotal`, `catatan`, `is_paid`, `is_paid_remark`, `is_paid_by`, `input_by`, `timestamp`) VALUES
(1, 0, 0, '2019-09-01', 'Gaji Steven', 1, 1000000, 1000000, 'abc', 2, 'ok', 2, 2, '2019-09-02 16:58:46'),
(4, 1, 3, '2019-08-29', 'Gaji Rio', 1, 1000000, 1000000, '', 2, '', 2, 2, '2019-09-10 14:02:02'),
(5, 1, 3, '2019-09-11', 'tes', 2, 500000, 1000000, '', 2, '', 2, 2, '2019-09-12 11:03:38'),
(6, 0, 0, '2019-09-01', 'aaaa', 1, 500000, 500000, '', 1, '', 0, 2, '2019-09-12 13:51:34'),
(7, 3, 5, '2019-09-02', 'Uang Kebersihan', 1, 500000, 500000, '', 2, '', 2, 2, '2019-09-16 14:20:13');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `satuan` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `nama`, `satuan`, `status`) VALUES
(1, 'Pupuk Urea', 1, 1),
(2, 'Pupuk Cair', 4, 1),
(3, 'Pasir', 3, 1),
(4, 'Material', 1, 1),
(5, 'Pupuk ZA', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `luas` int(11) NOT NULL,
  `jenis_kepemilikan` varchar(100) NOT NULL,
  `mulai_berlaku` date NOT NULL,
  `berlaku_hingga` date NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `is_global` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `nama`, `deskripsi`, `luas`, `jenis_kepemilikan`, `mulai_berlaku`, `berlaku_hingga`, `status`, `is_global`) VALUES
(0, 'Global', '', 1, 'milik pribadi', '0000-00-00', '0000-00-00', 1, 1),
(1, 'Lokasi A', 'ini keterangan lokasi A', 17500, 'gadai', '2019-07-31', '2020-07-30', 1, 0),
(2, 'Lokasi B', 'ini keterangan lokasi BA', 25000, 'milik pribadi', '0000-00-00', '0000-00-00', 1, 0),
(3, 'Lokasi C', 'ini keterangan lokasi C', 15000, 'milik pribadi', '0000-00-00', '0000-00-00', 1, 0),
(4, 'Lokasi D', 'Sewa pertama : Rp 20.000.000', 20000, 'sewa', '2019-07-31', '2021-07-31', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `uuid` varchar(10) NOT NULL,
  `label` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `tautan` text NOT NULL,
  `child_of` int(11) NOT NULL DEFAULT '0',
  `urutan` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `query_badge` text NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `uuid`, `label`, `icon`, `class`, `tautan`, `child_of`, `urutan`, `status`, `query_badge`, `is_deleted`) VALUES
(1, 'dsb', 'dashboard', 'icon-home', 'menu-dashboard', 'dashboard', 0, 1, 1, '', 0),
(2, 'loc', 'lokasi', 'icon-location-pin', 'menu-lokasi', 'lokasi', 0, 3, 1, '', 0),
(3, 'pkj', 'pekerja', 'icon-people', 'menu-pekerja', 'pekerja', 7, 5, 1, '', 0),
(4, 'splr', 'supplier', 'icon-support', 'menu-supplier', 'supplier', 7, 5, 1, '', 0),
(5, 'bnk', 'bank', 'icon-wallet', 'menu-bank', 'bank', 7, 7, 1, '', 0),
(6, 'usr', 'manajemen user', 'icon-user', 'menu-user', 'user', 7, 7, 1, '', 0),
(7, 'mstr', 'data master', 'icon-book-open', 'sub-master', '', 0, 2, 1, '', 0),
(8, 'lvl', 'hak akses', 'icon-rocket', 'menu-hak-akses', 'level', 7, 8, 1, '', 0),
(9, 'prd', 'periode', 'icon-calendar', 'menu-periode', 'periode', 0, 4, 1, '', 0),
(10, 'byr-pkj', 'pembayaran pekerja', 'icon-star', 'menu-pembayaran', 'pembayaran', 0, 9, 1, '', 0),
(11, 'pbli', 'pembelian global', 'icon-basket-loaded ', 'menu-pembelian', 'pembelian', 0, 10, 1, '', 0),
(12, 'plsn', 'pelunasan tempo', 'icon-hourglass', 'menu-pelunasan', 'pelunasan', 0, 12, 1, 'SELECT count(*) as total FROM `purchase` WHERE jenis_pembayaran = \'tempo\' and is_paid_tempo = 0 and is_paid = 2', 0),
(13, 'aprv', 'otorisasi pembelian', 'icon-note', 'menu-otorisasi-pembelian', 'otorisasi/pembelian', 0, 11, 1, 'SELECT count(*) as total FROM `purchase` WHERE is_paid = 1', 0),
(14, 'pdpt', 'pendapatan', 'icon-layers', 'menu-pendapatan', 'pendapatan', 0, 19, 1, '', 0),
(15, 'lpkeu', 'laporan', 'icon-pie-chart', 'menu-laporan', 'laporan', 0, 21, 1, '', 0),
(16, 'aprv', 'otorisasi pendapatan', 'icon-note', 'menu-otorisasi-pendapatan', 'otorisasi/pendapatan', 0, 20, 1, 'SELECT count(*) as total FROM `revenue` WHERE is_paid = 1', 0),
(17, 'sat', 'satuan', 'icon-mouse', 'menu-satuan', 'satuan', 7, 6, 1, '', 0),
(18, 'cat', 'barang', 'icon-social-dropbox', 'menu-barang', 'barang', 7, 5, 1, '', 0),
(19, 'stok', 'stok barang', 'icon-social-dropbox', 'menu-stok', 'stok', 0, 13, 1, '', 0),
(20, 'outstck', 'barang keluar', 'icon-logout', 'menu-barang-keluar', 'barangkeluar', 0, 15, 1, '', 0),
(21, 'otroutstck', 'otorisasi barang keluar', 'icon-note', 'menu-otorisasi-barangkeluar', 'otorisasi/barangkeluar', 0, 16, 1, 'SELECT count(*) as total FROM `outstock` WHERE is_acc = 1', 0),
(22, 'bbn', 'beban', 'icon-umbrella', 'menu-beban', 'beban', 0, 17, 1, '', 0),
(23, 'otbbn', 'otorisasi beban', 'icon-note', 'menu-otorisasi-beban', 'otorisasi/beban', 0, 18, 1, 'SELECT count(*) as total FROM `expense` WHERE is_paid = 1', 0),
(24, 'rstok', 'rekap stok', 'icon-grid', 'menu-rekap-stok', 'stok/rekap', 0, 14, 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `outstock`
--

CREATE TABLE `outstock` (
  `id` int(11) NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `tgl_keluar` date NOT NULL,
  `input_by` int(11) NOT NULL,
  `is_acc` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 = wait, 2 = acc, 3 = declined',
  `is_acc_remark` text NOT NULL,
  `is_acc_by` int(11) NOT NULL,
  `is_acc_timestamp` datetime NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outstock`
--

INSERT INTO `outstock` (`id`, `id_lokasi`, `id_periode`, `tgl_keluar`, `input_by`, `is_acc`, `is_acc_remark`, `is_acc_by`, `is_acc_timestamp`, `timestamp`) VALUES
(1, 1, 3, '2019-08-02', 2, 2, '', 2, '2019-08-29 16:08:14', '2019-08-20 11:47:48'),
(2, 1, 3, '2019-08-05', 2, 2, '', 2, '2019-08-29 16:25:38', '2019-08-29 16:24:31'),
(3, 1, 3, '2019-08-28', 2, 2, '', 2, '2019-08-30 14:12:14', '2019-08-30 13:38:32'),
(4, 2, 5, '2019-08-01', 2, 2, '', 2, '2019-08-30 16:40:04', '2019-08-30 14:21:16'),
(5, 2, 5, '2019-08-01', 2, 2, '', 2, '2019-08-30 16:59:01', '2019-08-30 16:58:48'),
(6, 3, 5, '2019-09-01', 2, 2, '', 2, '2019-09-16 14:19:22', '2019-09-16 14:18:53');

-- --------------------------------------------------------

--
-- Table structure for table `outstock_detail`
--

CREATE TABLE `outstock_detail` (
  `id` int(11) NOT NULL,
  `id_outstock` int(11) NOT NULL,
  `id_stock` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `satuan` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outstock_detail`
--

INSERT INTO `outstock_detail` (`id`, `id_outstock`, `id_stock`, `kuantitas`, `satuan`, `harga`, `subtotal`, `catatan`, `input_by`, `timestamp`) VALUES
(2, 1, 3, 1000, 'KG', 2000, 2000000, 'tes', 2, '2019-08-29 16:08:13'),
(3, 1, 5, 1500, 'KG', 2100, 3150000, 'tes', 2, '2019-08-29 16:08:13'),
(4, 1, 6, 200, 'KG', 2100, 420000, 'tes', 2, '2019-08-29 16:08:14'),
(5, 2, 6, 400, 'KG', 2100, 840000, '', 2, '2019-08-29 16:25:38'),
(6, 3, 6, 400, 'KG', 2100, 840000, '', 2, '2019-08-30 14:12:13'),
(7, 3, 7, 200, 'KG', 2200, 440000, '', 2, '2019-08-30 14:12:13'),
(8, 3, 7, 800, 'KG', 2200, 1760000, '', 2, '2019-08-30 14:12:14'),
(9, 4, 4, 500, 'KG', 1750, 875000, '', 2, '2019-08-30 16:40:04'),
(10, 4, 2, 1, 'KG', 1000000, 1000000, '', 2, '2019-08-30 16:40:04'),
(11, 5, 7, 250, 'KG', 2200, 550000, '', 2, '2019-08-30 16:59:01'),
(12, 6, 1, 1, 'Buah', 1500000, 1500000, '', 2, '2019-09-16 14:19:22'),
(13, 6, 8, 1, 'Liter', 12500, 12500, '', 2, '2019-09-16 14:19:22');

-- --------------------------------------------------------

--
-- Table structure for table `outstock_waiting`
--

CREATE TABLE `outstock_waiting` (
  `id` int(11) NOT NULL,
  `id_outstock` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `satuan` varchar(100) NOT NULL,
  `catatan` text NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outstock_waiting`
--

INSERT INTO `outstock_waiting` (`id`, `id_outstock`, `id_barang`, `kuantitas`, `satuan`, `catatan`, `input_by`, `timestamp`) VALUES
(9, 1, 1, 2700, 'KG', 'tes', 2, '2019-08-29 15:17:03'),
(10, 2, 1, 400, 'KG', '', 2, '2019-08-29 16:24:37'),
(11, 3, 1, 600, 'KG', '', 2, '2019-08-30 14:11:38'),
(12, 3, 1, 800, 'KG', '', 2, '2019-08-30 14:11:49'),
(13, 4, 5, 500, 'KG', '', 2, '2019-08-30 16:38:56'),
(14, 4, 4, 1, 'KG', '', 2, '2019-08-30 16:39:02'),
(15, 5, 1, 250, 'KG', '', 2, '2019-08-30 16:58:55'),
(16, 6, 3, 1, 'Buah', '', 2, '2019-09-16 14:19:01'),
(17, 6, 2, 1, 'Liter', '', 2, '2019-09-16 14:19:08');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `id_pekerja` int(11) NOT NULL,
  `bayaran` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `id_periode`, `tgl_bayar`, `id_pekerja`, `bayaran`, `input_by`, `timestamp`) VALUES
(5, 4, '2019-07-19', 1, 55000, 2, '2019-07-22 14:41:28'),
(6, 4, '2019-07-19', 2, 50000, 2, '2019-07-22 14:41:28'),
(7, 4, '2019-07-20', 1, 50000, 2, '2019-07-22 15:50:56'),
(8, 4, '2019-07-20', 2, 50000, 2, '2019-07-22 15:50:56'),
(9, 4, '2019-07-20', 3, 50000, 2, '2019-07-22 15:50:56'),
(10, 4, '2019-07-20', 4, 60000, 2, '2019-07-22 15:50:56'),
(11, 4, '2019-07-20', 6, 55000, 2, '2019-07-22 15:50:56'),
(12, 1, '2019-07-22', 3, 50000, 2, '2019-07-22 15:54:42'),
(13, 1, '2019-07-22', 5, 45000, 2, '2019-07-22 15:54:43'),
(14, 3, '2019-07-28', 1, 50000, 2, '2019-07-29 16:10:06'),
(15, 3, '2019-07-28', 2, 50000, 2, '2019-07-29 16:10:06'),
(16, 3, '2019-07-28', 3, 50000, 2, '2019-07-29 16:10:06'),
(17, 3, '2019-07-28', 5, 45000, 2, '2019-07-29 16:10:06'),
(18, 3, '2019-07-28', 4, 60000, 2, '2019-07-29 16:10:06'),
(19, 3, '2019-07-25', 1, 50000, 2, '2019-07-29 16:10:16'),
(20, 3, '2019-07-25', 2, 50000, 2, '2019-07-29 16:10:16'),
(21, 3, '2019-07-25', 4, 60000, 2, '2019-07-29 16:10:16'),
(22, 3, '2019-07-25', 6, 55000, 2, '2019-07-29 16:10:17'),
(28, 3, '2019-07-28', 6, 55000, 2, '2019-07-29 16:39:44'),
(29, 3, '2019-07-29', 7, 5000000, 2, '2019-07-29 16:40:46'),
(30, 0, '2019-08-29', 1, 50000, 2, '2019-09-13 14:29:51'),
(31, 0, '2019-08-29', 2, 50000, 2, '2019-09-13 14:29:51'),
(32, 0, '2019-08-29', 3, 50000, 2, '2019-09-13 14:29:51'),
(33, 3, '2019-09-01', 7, 5000000, 2, '2019-09-16 13:43:11'),
(38, 5, '2019-09-07', 7, 5000000, 2, '2019-09-16 14:25:14');

-- --------------------------------------------------------

--
-- Table structure for table `periode`
--

CREATE TABLE `periode` (
  `id` int(11) NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_berakhir` date NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `selesai` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periode`
--

INSERT INTO `periode` (`id`, `id_lokasi`, `nama`, `deskripsi`, `tgl_mulai`, `tgl_berakhir`, `status`, `selesai`) VALUES
(0, 0, 'Global', 'global', '0000-00-00', '0000-00-00', 1, 0),
(1, 1, 'Januari 2019', 'tess', '2019-01-01', '0000-00-00', 1, 1),
(3, 1, 'Juni 2019', 'tes12', '2019-06-01', '0000-00-00', 1, 0),
(4, 2, 'Maret 2019', '', '2019-03-01', '0000-00-00', 1, 0),
(5, 3, 'Juli 2019', '', '2019-07-01', '0000-00-00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE `purchase` (
  `id` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `invoice` varchar(100) NOT NULL,
  `tgl_beli` date NOT NULL,
  `grandtotal` int(11) NOT NULL,
  `jenis_pembayaran` enum('cash','tempo') NOT NULL DEFAULT 'cash',
  `input_by` int(11) NOT NULL,
  `is_paid` tinyint(2) NOT NULL DEFAULT '1',
  `is_paid_remark` text NOT NULL,
  `is_paid_tempo` tinyint(2) NOT NULL,
  `is_paid_by` int(11) NOT NULL,
  `is_paid_timestamp` datetime NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`id`, `id_supplier`, `invoice`, `tgl_beli`, `grandtotal`, `jenis_pembayaran`, `input_by`, `is_paid`, `is_paid_remark`, `is_paid_tempo`, `is_paid_by`, `is_paid_timestamp`, `timestamp`) VALUES
(1, 1, '000-20190801-001', '2019-08-01', 0, 'cash', 2, 2, '', 0, 2, '0000-00-00 00:00:00', '2019-08-23 14:45:35'),
(2, 3, '000-20190802-001', '2019-08-02', 0, 'tempo', 2, 2, '', 1, 2, '0000-00-00 00:00:00', '2019-08-23 14:54:16'),
(3, 3, '000-20190901-001', '2019-09-01', 0, 'cash', 2, 2, '', 0, 2, '0000-00-00 00:00:00', '2019-09-02 16:45:01'),
(5, 1, '20190901-001', '2019-09-01', 0, 'cash', 2, 2, 'ok', 0, 2, '0000-00-00 00:00:00', '2019-09-16 13:45:19'),
(6, 1, '20190902-001', '2019-09-02', 0, 'tempo', 2, 2, 'ok', 1, 2, '0000-00-00 00:00:00', '2019-09-16 13:58:51');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_detail`
--

CREATE TABLE `purchase_detail` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL DEFAULT '1',
  `id_pembelian` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `satuan` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_detail`
--

INSERT INTO `purchase_detail` (`id`, `id_barang`, `id_pembelian`, `kuantitas`, `satuan`, `harga`, `subtotal`, `catatan`, `input_by`, `timestamp`) VALUES
(1, 1, 1, 1000, 'KG', 2000, 2000000, '', 2, '2019-08-23 14:46:40'),
(2, 5, 1, 1000, 'KG', 1750, 1750000, '', 2, '2019-08-23 14:52:54'),
(3, 3, 2, 1, 'Buah', 1500000, 1500000, '', 2, '2019-08-23 14:54:28'),
(4, 4, 2, 1, 'KG', 1000000, 1000000, '', 2, '2019-08-23 14:54:51'),
(5, 1, 5, 1000, 'KG', 2000, 2000000, '', 2, '2019-09-16 13:45:41'),
(6, 5, 3, 500, 'KG', 1750, 875000, '', 2, '2019-09-16 13:50:29'),
(7, 1, 3, 500, 'KG', 2100, 1050000, '', 2, '2019-09-16 13:50:40'),
(8, 2, 6, 2, 'Liter', 150000, 300000, '', 2, '2019-09-16 13:59:25');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_repayment`
--

CREATE TABLE `purchase_repayment` (
  `id` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `tgl_pelunasan` date NOT NULL,
  `rek_pengirim` int(11) NOT NULL,
  `rek_tujuan` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_repayment`
--

INSERT INTO `purchase_repayment` (`id`, `id_pembelian`, `tgl_pelunasan`, `rek_pengirim`, `rek_tujuan`, `nominal`, `catatan`, `input_by`, `timestamp`) VALUES
(1, 2, '2019-09-01', 1, 2, 2500000, '', 2, '2019-09-16 13:56:11'),
(2, 6, '2019-09-04', 1, 3, 300000, '', 2, '2019-09-16 14:00:30');

-- --------------------------------------------------------

--
-- Table structure for table `revenue`
--

CREATE TABLE `revenue` (
  `id` int(11) NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `kode_faktur` varchar(100) NOT NULL,
  `catatan` text NOT NULL,
  `is_paid` tinyint(2) NOT NULL DEFAULT '1',
  `is_paid_remark` text NOT NULL,
  `is_paid_by` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `revenue`
--

INSERT INTO `revenue` (`id`, `id_lokasi`, `id_periode`, `tgl_transaksi`, `nama`, `kuantitas`, `harga`, `subtotal`, `kode_faktur`, `catatan`, `is_paid`, `is_paid_remark`, `is_paid_by`, `input_by`, `timestamp`) VALUES
(1, 1, 3, '2019-07-28', 'Jual tebu', 100, 23000, 2300000, 'A1', 'aa', 2, 'ok', 2, 2, '2019-07-29 13:46:02'),
(2, 1, 3, '2019-07-27', 'Jual beli tebu', 550, 22500, 12375000, 'A2', 'bb', 2, '', 2, 2, '2019-07-29 14:00:33'),
(4, 1, 3, '2019-07-28', 'Tebu berkualitas', 250, 23000, 5750000, 'A3', '', 2, '', 2, 2, '2019-07-29 14:07:09'),
(5, 1, 1, '2019-07-28', 'Jual tebu', 2700, 24000, 64800000, 'A4', '', 2, '', 2, 2, '2019-07-29 16:37:41'),
(6, 3, 5, '2019-09-09', 'Penjualan Gabah', 100, 65000, 6500000, '123', '', 2, '', 2, 2, '2019-09-16 14:21:08');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `id_purchase_detail` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `stok_awal` int(11) NOT NULL,
  `stok_sisa` int(11) NOT NULL,
  `satuan` varchar(25) NOT NULL,
  `harga` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `id_purchase_detail`, `id_barang`, `tgl_masuk`, `stok_awal`, `stok_sisa`, `satuan`, `harga`, `input_by`, `timestamp`, `update_by`, `update_timestamp`) VALUES
(1, 3, 3, '2019-08-02', 1, 0, 'Buah', 1500000, 2, '2019-08-23 16:57:31', 0, '0000-00-00 00:00:00'),
(2, 4, 4, '2019-08-02', 1, 0, 'KG', 1000000, 2, '2019-08-23 16:57:31', 0, '0000-00-00 00:00:00'),
(3, 1, 1, '2019-08-01', 1000, 0, 'KG', 2000, 2, '2019-08-24 13:49:13', 0, '0000-00-00 00:00:00'),
(4, 2, 5, '2019-08-01', 1000, 500, 'KG', 1750, 2, '2019-08-24 13:49:13', 0, '0000-00-00 00:00:00'),
(5, 0, 1, '2019-08-03', 1500, 0, 'KG', 2100, 2, '2019-08-24 13:55:52', 2, '2019-08-24 14:33:48'),
(6, 0, 1, '2019-08-06', 1000, 0, 'KG', 2100, 2, '2019-08-24 14:34:05', 0, '0000-00-00 00:00:00'),
(7, 0, 1, '2019-08-27', 2000, 750, 'KG', 2200, 2, '2019-08-29 16:15:59', 0, '0000-00-00 00:00:00'),
(8, 0, 2, '2019-08-28', 10, 9, 'Liter', 12500, 2, '2019-08-30 16:44:15', 0, '0000-00-00 00:00:00'),
(9, 0, 3, '2019-09-01', 1, 1, 'Buah', 1000000, 2, '2019-09-16 13:46:31', 0, '0000-00-00 00:00:00'),
(10, 5, 1, '2019-09-01', 1000, 1000, 'KG', 2000, 2, '2019-09-16 13:47:56', 0, '0000-00-00 00:00:00'),
(11, 6, 5, '2019-09-01', 500, 500, 'KG', 1750, 2, '2019-09-16 13:51:33', 0, '0000-00-00 00:00:00'),
(12, 7, 1, '2019-09-01', 500, 500, 'KG', 2100, 2, '2019-09-16 13:51:33', 0, '0000-00-00 00:00:00'),
(13, 8, 2, '2019-09-02', 2, 2, 'Liter', 150000, 2, '2019-09-16 14:00:05', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `nama`, `deskripsi`, `status`) VALUES
(1, 'Maju', 'Jember', 1),
(2, 'Toko B', 'Sukowono - Jember', 1),
(3, 'Toko A', 'Jember', 1);

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `nama`, `status`) VALUES
(1, 'KG', 1),
(2, 'Gram', 1),
(3, 'Buah', 1),
(4, 'Liter', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `id_level` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `id_level`, `status`) VALUES
(1, 'super', 'a878a719b3cf0d99a77efeb0035459d5', 'Superadmin', 1, 1),
(2, 'samuel', 'a878a719b3cf0d99a77efeb0035459d5', 'Samuel', 2, 1),
(3, 'iik', '5816ce4849acd8c84bc8642b9037d284', 'Iik', 3, 1),
(5, 'admin', '5816ce4849acd8c84bc8642b9037d284', 'Admin', 4, 1),
(6, 'indra', '5816ce4849acd8c84bc8642b9037d284', 'Indra', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_level`
--

CREATE TABLE `user_level` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `akses` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_level`
--

INSERT INTO `user_level` (`id`, `nama`, `akses`) VALUES
(1, 'Super', '*'),
(2, 'Owner', '[\"dsb\",\"mstr\",\"bnk\",\"usr\",\"lvl\",\"sat\",\"cat\",\"pkj\",\"splr\",\"loc\",\"prd\",\"byr-pkj\",\"pbli\",\"aprv\",\"plsn\",\"stok\",\"outstck\",\"rstok\",\"otroutstck\",\"bbn\",\"otbbn\",\"pdpt\",\"aprv\",\"lpkeu\"]'),
(3, 'Finance', '[\"dsb\",\"mstr\",\"bnk\",\"sat\",\"cat\",\"pkj\",\"splr\",\"byr-pkj\",\"aprv\",\"stok\",\"rstok\",\"otroutstck\",\"otbbn\",\"aprv\",\"lpkeu\"]'),
(4, 'Admin', '[\"dsb\",\"mstr\",\"bnk\",\"sat\",\"cat\",\"pkj\",\"splr\",\"byr-pkj\",\"pbli\",\"rstok\",\"outstck\",\"bbn\",\"pdpt\"]'),
(5, 'Manager', '[\"dsb\",\"mstr\",\"bnk\",\"usr\",\"sat\",\"cat\",\"pkj\",\"splr\",\"loc\",\"prd\",\"byr-pkj\",\"pbli\",\"aprv\",\"plsn\",\"stok\",\"rstok\",\"outstck\",\"otroutstck\",\"bbn\",\"otbbn\",\"pdpt\",\"aprv\",\"lpkeu\"]');

-- --------------------------------------------------------

--
-- Table structure for table `worker`
--

CREATE TABLE `worker` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `bayaran` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `worker`
--

INSERT INTO `worker` (`id`, `nama`, `deskripsi`, `bayaran`, `status`) VALUES
(1, 'Pak A', '', 50000, 1),
(2, 'Pak B', 'tes2', 50000, 1),
(3, 'Pak C', 'tes', 50000, 1),
(4, 'Pak E', 'borongan', 60000, 1),
(5, 'Pak D', 'borongan', 45000, 1),
(6, 'Pak F', 'borongan', 55000, 1),
(7, 'Borongan', '', 5000000, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`id`),
  ADD KEY `is_paid_by` (`is_paid_by`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `id_periode` (`id_periode`),
  ADD KEY `id_lokasi` (`id_lokasi`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `satuan` (`satuan`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outstock`
--
ALTER TABLE `outstock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_lokasi` (`id_lokasi`),
  ADD KEY `id_periode` (`id_periode`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `is_acc_by` (`is_acc_by`);

--
-- Indexes for table `outstock_detail`
--
ALTER TABLE `outstock_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_outstock` (`id_outstock`),
  ADD KEY `id_stock` (`id_stock`),
  ADD KEY `satuan` (`satuan`),
  ADD KEY `input_by` (`input_by`);

--
-- Indexes for table `outstock_waiting`
--
ALTER TABLE `outstock_waiting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_outstock` (`id_outstock`),
  ADD KEY `id_barang` (`id_barang`),
  ADD KEY `input_by` (`input_by`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_periode` (`id_periode`),
  ADD KEY `id_pekerja` (`id_pekerja`),
  ADD KEY `input_by` (`input_by`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_location` (`id_lokasi`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice` (`invoice`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `is_paid_by` (`is_paid_by`),
  ADD KEY `id_supplier` (`id_supplier`);

--
-- Indexes for table `purchase_detail`
--
ALTER TABLE `purchase_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pembelian` (`id_pembelian`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `satuan` (`satuan`),
  ADD KEY `id_kategori` (`id_barang`);

--
-- Indexes for table `purchase_repayment`
--
ALTER TABLE `purchase_repayment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `rek_pengirim` (`rek_pengirim`),
  ADD KEY `rek_tujuan` (`rek_tujuan`),
  ADD KEY `id_pembelian` (`id_pembelian`);

--
-- Indexes for table `revenue`
--
ALTER TABLE `revenue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `id_lokasi` (`id_lokasi`),
  ADD KEY `id_periode` (`id_periode`),
  ADD KEY `is_paid_by` (`is_paid_by`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_purchase_detail` (`id_purchase_detail`),
  ADD KEY `id_kategori` (`id_barang`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `update_by` (`update_by`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_level_id` (`id_level`);

--
-- Indexes for table `user_level`
--
ALTER TABLE `user_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `worker`
--
ALTER TABLE `worker`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `outstock`
--
ALTER TABLE `outstock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `outstock_detail`
--
ALTER TABLE `outstock_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `outstock_waiting`
--
ALTER TABLE `outstock_waiting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `periode`
--
ALTER TABLE `periode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `purchase_detail`
--
ALTER TABLE `purchase_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `purchase_repayment`
--
ALTER TABLE `purchase_repayment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `revenue`
--
ALTER TABLE `revenue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_level`
--
ALTER TABLE `user_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `worker`
--
ALTER TABLE `worker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`input_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `payment_ibfk_3` FOREIGN KEY (`id_pekerja`) REFERENCES `worker` (`id`);

--
-- Constraints for table `purchase`
--
ALTER TABLE `purchase`
  ADD CONSTRAINT `purchase_ibfk_1` FOREIGN KEY (`input_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `purchase_ibfk_3` FOREIGN KEY (`id_supplier`) REFERENCES `supplier` (`id`);

--
-- Constraints for table `purchase_detail`
--
ALTER TABLE `purchase_detail`
  ADD CONSTRAINT `purchase_detail_ibfk_1` FOREIGN KEY (`id_pembelian`) REFERENCES `purchase` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `user_level` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
