-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 16, 2019 at 07:38 AM
-- Server version: 10.2.23-MariaDB
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u198894986_sugar`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `bank` varchar(100) NOT NULL,
  `no_rek` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `id` int(11) NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `nama_trx` varchar(100) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `is_paid` tinyint(2) NOT NULL,
  `is_paid_remark` text NOT NULL,
  `is_paid_by` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `satuan` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `luas` int(11) NOT NULL,
  `jenis_kepemilikan` varchar(100) NOT NULL,
  `mulai_berlaku` date NOT NULL,
  `berlaku_hingga` date NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1,
  `is_global` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `nama`, `deskripsi`, `luas`, `jenis_kepemilikan`, `mulai_berlaku`, `berlaku_hingga`, `status`, `is_global`) VALUES
(1, 'Global', '', 0, '', '0000-00-00', '0000-00-00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `uuid` varchar(10) NOT NULL,
  `label` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `tautan` text NOT NULL,
  `child_of` int(11) NOT NULL DEFAULT 0,
  `urutan` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `query_badge` text NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `uuid`, `label`, `icon`, `class`, `tautan`, `child_of`, `urutan`, `status`, `query_badge`, `is_deleted`) VALUES
(1, 'dsb', 'dashboard', 'icon-home', 'menu-dashboard', 'dashboard', 0, 1, 1, '', 0),
(2, 'loc', 'lokasi', 'icon-location-pin', 'menu-lokasi', 'lokasi', 0, 3, 1, '', 0),
(3, 'pkj', 'pekerja', 'icon-people', 'menu-pekerja', 'pekerja', 7, 5, 1, '', 0),
(4, 'splr', 'supplier', 'icon-support', 'menu-supplier', 'supplier', 7, 5, 1, '', 0),
(5, 'bnk', 'bank', 'icon-wallet', 'menu-bank', 'bank', 7, 7, 1, '', 0),
(6, 'usr', 'manajemen user', 'icon-user', 'menu-user', 'user', 7, 7, 1, '', 0),
(7, 'mstr', 'data master', 'icon-book-open', 'sub-master', '', 0, 2, 1, '', 0),
(8, 'lvl', 'hak akses', 'icon-rocket', 'menu-hak-akses', 'level', 7, 8, 1, '', 0),
(9, 'prd', 'periode', 'icon-calendar', 'menu-periode', 'periode', 0, 4, 1, '', 0),
(10, 'byr-pkj', 'pembayaran pekerja', 'icon-star', 'menu-pembayaran', 'pembayaran', 0, 9, 1, '', 0),
(11, 'pbli', 'pembelian global', 'icon-basket-loaded ', 'menu-pembelian', 'pembelian', 0, 10, 1, '', 0),
(12, 'plsn', 'pelunasan tempo', 'icon-hourglass', 'menu-pelunasan', 'pelunasan', 0, 12, 1, 'SELECT count(*) as total FROM `purchase` WHERE jenis_pembayaran = \'tempo\' and is_paid_tempo = 0 and is_paid = 2', 0),
(13, 'aprv', 'otorisasi pembelian', 'icon-note', 'menu-otorisasi-pembelian', 'otorisasi/pembelian', 0, 11, 1, 'SELECT count(*) as total FROM `purchase` WHERE is_paid = 1', 0),
(14, 'pdpt', 'pendapatan', 'icon-layers', 'menu-pendapatan', 'pendapatan', 0, 19, 1, '', 0),
(15, 'lpkeu', 'laporan', 'icon-pie-chart', 'menu-laporan', 'laporan', 0, 21, 1, '', 0),
(16, 'aprv', 'otorisasi pendapatan', 'icon-note', 'menu-otorisasi-pendapatan', 'otorisasi/pendapatan', 0, 20, 1, 'SELECT count(*) as total FROM `revenue` WHERE is_paid = 1', 0),
(17, 'sat', 'satuan', 'icon-mouse', 'menu-satuan', 'satuan', 7, 6, 1, '', 0),
(18, 'cat', 'barang', 'icon-social-dropbox', 'menu-barang', 'barang', 7, 5, 1, '', 0),
(19, 'stok', 'stok barang', 'icon-social-dropbox', 'menu-stok', 'stok', 0, 13, 1, '', 0),
(20, 'outstck', 'barang keluar', 'icon-logout', 'menu-barang-keluar', 'barangkeluar', 0, 15, 1, '', 0),
(21, 'otroutstck', 'otorisasi barang keluar', 'icon-note', 'menu-otorisasi-barangkeluar', 'otorisasi/barangkeluar', 0, 16, 1, 'SELECT count(*) as total FROM `outstock` WHERE is_acc = 1', 0),
(22, 'bbn', 'beban', 'icon-umbrella', 'menu-beban', 'beban', 0, 17, 1, '', 0),
(23, 'otbbn', 'otorisasi beban', 'icon-note', 'menu-otorisasi-beban', 'otorisasi/beban', 0, 18, 1, 'SELECT count(*) as total FROM `expense` WHERE is_paid = 1', 0),
(24, 'rstok', 'rekap stok', 'icon-grid', 'menu-rekap-stok', 'stok/rekap', 0, 14, 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `outstock`
--

CREATE TABLE `outstock` (
  `id` int(11) NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `tgl_keluar` date NOT NULL,
  `input_by` int(11) NOT NULL,
  `is_acc` tinyint(2) NOT NULL DEFAULT 1 COMMENT '1 = wait, 2 = acc, 3 = declined',
  `is_acc_remark` text NOT NULL,
  `is_acc_by` int(11) NOT NULL,
  `is_acc_timestamp` datetime NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `outstock_detail`
--

CREATE TABLE `outstock_detail` (
  `id` int(11) NOT NULL,
  `id_outstock` int(11) NOT NULL,
  `id_stock` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `satuan` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `outstock_waiting`
--

CREATE TABLE `outstock_waiting` (
  `id` int(11) NOT NULL,
  `id_outstock` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `satuan` varchar(100) NOT NULL,
  `catatan` text NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `id_pekerja` int(11) NOT NULL,
  `bayaran` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `periode`
--

CREATE TABLE `periode` (
  `id` int(11) NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_berakhir` date NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1,
  `selesai` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periode`
--

INSERT INTO `periode` (`id`, `id_lokasi`, `nama`, `deskripsi`, `tgl_mulai`, `tgl_berakhir`, `status`, `selesai`) VALUES
(1, 1, 'Global', '', '0000-00-00', '0000-00-00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE `purchase` (
  `id` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `invoice` varchar(100) NOT NULL,
  `tgl_beli` date NOT NULL,
  `grandtotal` int(11) NOT NULL,
  `jenis_pembayaran` enum('cash','tempo') NOT NULL DEFAULT 'cash',
  `input_by` int(11) NOT NULL,
  `is_paid` tinyint(2) NOT NULL DEFAULT 1,
  `is_paid_remark` text NOT NULL,
  `is_paid_tempo` tinyint(2) NOT NULL,
  `is_paid_by` int(11) NOT NULL,
  `is_paid_timestamp` datetime NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_detail`
--

CREATE TABLE `purchase_detail` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL DEFAULT 1,
  `id_pembelian` int(11) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `satuan` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_repayment`
--

CREATE TABLE `purchase_repayment` (
  `id` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `tgl_pelunasan` date NOT NULL,
  `rek_pengirim` int(11) NOT NULL,
  `rek_tujuan` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `revenue`
--

CREATE TABLE `revenue` (
  `id` int(11) NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `kode_faktur` varchar(100) NOT NULL,
  `catatan` text NOT NULL,
  `is_paid` tinyint(2) NOT NULL DEFAULT 1,
  `is_paid_remark` text NOT NULL,
  `is_paid_by` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `id_purchase_detail` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `stok_awal` int(11) NOT NULL,
  `stok_sisa` int(11) NOT NULL,
  `satuan` varchar(25) NOT NULL,
  `harga` int(11) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `id_level` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `id_level`, `status`) VALUES
(1, 'super', 'a878a719b3cf0d99a77efeb0035459d5', 'Superadmin', 1, 1),
(2, 'samuel', 'a878a719b3cf0d99a77efeb0035459d5', 'Samuel', 2, 1),
(3, 'iik', '5816ce4849acd8c84bc8642b9037d284', 'Iik', 3, 1),
(5, 'admin', '5816ce4849acd8c84bc8642b9037d284', 'Admin', 4, 1),
(6, 'indra', '5816ce4849acd8c84bc8642b9037d284', 'Indra', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_level`
--

CREATE TABLE `user_level` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `akses` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_level`
--

INSERT INTO `user_level` (`id`, `nama`, `akses`) VALUES
(1, 'Super', '*'),
(2, 'Owner', '[\"dsb\",\"mstr\",\"bnk\",\"usr\",\"lvl\",\"sat\",\"cat\",\"pkj\",\"splr\",\"loc\",\"prd\",\"byr-pkj\",\"pbli\",\"aprv\",\"plsn\",\"stok\",\"outstck\",\"rstok\",\"otroutstck\",\"bbn\",\"otbbn\",\"pdpt\",\"aprv\",\"lpkeu\"]'),
(3, 'Finance', '[\"dsb\",\"mstr\",\"bnk\",\"sat\",\"cat\",\"pkj\",\"splr\",\"byr-pkj\",\"aprv\",\"stok\",\"rstok\",\"otroutstck\",\"otbbn\",\"aprv\",\"lpkeu\"]'),
(4, 'Admin', '[\"dsb\",\"mstr\",\"bnk\",\"sat\",\"cat\",\"pkj\",\"splr\",\"byr-pkj\",\"pbli\",\"rstok\",\"outstck\",\"bbn\",\"pdpt\"]'),
(5, 'Manager', '[\"dsb\",\"mstr\",\"bnk\",\"usr\",\"sat\",\"cat\",\"pkj\",\"splr\",\"loc\",\"prd\",\"byr-pkj\",\"pbli\",\"aprv\",\"plsn\",\"stok\",\"rstok\",\"outstck\",\"otroutstck\",\"bbn\",\"otbbn\",\"pdpt\",\"aprv\",\"lpkeu\"]');

-- --------------------------------------------------------

--
-- Table structure for table `worker`
--

CREATE TABLE `worker` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `bayaran` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`id`),
  ADD KEY `is_paid_by` (`is_paid_by`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `id_periode` (`id_periode`),
  ADD KEY `id_lokasi` (`id_lokasi`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `satuan` (`satuan`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outstock`
--
ALTER TABLE `outstock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_lokasi` (`id_lokasi`),
  ADD KEY `id_periode` (`id_periode`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `is_acc_by` (`is_acc_by`);

--
-- Indexes for table `outstock_detail`
--
ALTER TABLE `outstock_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_outstock` (`id_outstock`),
  ADD KEY `id_stock` (`id_stock`),
  ADD KEY `satuan` (`satuan`),
  ADD KEY `input_by` (`input_by`);

--
-- Indexes for table `outstock_waiting`
--
ALTER TABLE `outstock_waiting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_outstock` (`id_outstock`),
  ADD KEY `id_barang` (`id_barang`),
  ADD KEY `input_by` (`input_by`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_periode` (`id_periode`),
  ADD KEY `id_pekerja` (`id_pekerja`),
  ADD KEY `input_by` (`input_by`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_location` (`id_lokasi`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice` (`invoice`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `is_paid_by` (`is_paid_by`),
  ADD KEY `id_supplier` (`id_supplier`);

--
-- Indexes for table `purchase_detail`
--
ALTER TABLE `purchase_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pembelian` (`id_pembelian`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `satuan` (`satuan`),
  ADD KEY `id_kategori` (`id_barang`);

--
-- Indexes for table `purchase_repayment`
--
ALTER TABLE `purchase_repayment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `rek_pengirim` (`rek_pengirim`),
  ADD KEY `rek_tujuan` (`rek_tujuan`),
  ADD KEY `id_pembelian` (`id_pembelian`);

--
-- Indexes for table `revenue`
--
ALTER TABLE `revenue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `id_lokasi` (`id_lokasi`),
  ADD KEY `id_periode` (`id_periode`),
  ADD KEY `is_paid_by` (`is_paid_by`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_purchase_detail` (`id_purchase_detail`),
  ADD KEY `id_kategori` (`id_barang`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `update_by` (`update_by`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_level_id` (`id_level`);

--
-- Indexes for table `user_level`
--
ALTER TABLE `user_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `worker`
--
ALTER TABLE `worker`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `outstock`
--
ALTER TABLE `outstock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `outstock_detail`
--
ALTER TABLE `outstock_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `outstock_waiting`
--
ALTER TABLE `outstock_waiting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `periode`
--
ALTER TABLE `periode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_detail`
--
ALTER TABLE `purchase_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_repayment`
--
ALTER TABLE `purchase_repayment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revenue`
--
ALTER TABLE `revenue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_level`
--
ALTER TABLE `user_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `worker`
--
ALTER TABLE `worker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`input_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `payment_ibfk_3` FOREIGN KEY (`id_pekerja`) REFERENCES `worker` (`id`);

--
-- Constraints for table `purchase`
--
ALTER TABLE `purchase`
  ADD CONSTRAINT `purchase_ibfk_1` FOREIGN KEY (`input_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `purchase_ibfk_3` FOREIGN KEY (`id_supplier`) REFERENCES `supplier` (`id`);

--
-- Constraints for table `purchase_detail`
--
ALTER TABLE `purchase_detail`
  ADD CONSTRAINT `purchase_detail_ibfk_1` FOREIGN KEY (`id_pembelian`) REFERENCES `purchase` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `user_level` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
