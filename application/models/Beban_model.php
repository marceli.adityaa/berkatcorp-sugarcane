<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beban_model extends MY_Model {
	public $_table = 'expense';
	private $kolom = array();

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get_data($id = null){
        $get = $this->input->get();
        $this->db->select('a.*, b.username as input, ifnull(c.nama, "global") as periode, ifnull(d.nama,"global") as lokasi');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(!empty($start = $this->input->get('start')) && $start != ''){
			$this->db->where('tgl_transaksi >=', $start);
		}
		if(!empty($end = $this->input->get('end')) && $end != ''){
			$this->db->where('tgl_transaksi <=', $end);
        }
        if(!empty($status = $this->input->get('status')) && $status != 'all'){
            $this->db->where('is_paid', $status);
        }
        if (isset($get['lokasi']) && $get['lokasi'] != 'all') {
            $this->db->where('d.id', $get['lokasi']);
        }
        if (isset($get['periode']) && $get['periode'] != 'all') {
            $this->db->where('a.id_periode', $get['periode']);
        }
        $this->db->join('user b', 'a.input_by = b.id');
        $this->db->join('periode c', 'a.id_periode = c.id', 'left');
        $this->db->join('location d', 'c.id_lokasi = d.id', 'left');
        $this->db->order_by('a.is_paid');
        $this->db->order_by('a.tgl_transaksi', 'desc');
        $result = $this->db->get('expense a');
        if ($result->num_rows() > 0) {
            if(!empty($id)){
                return $result->row_array();
            }else{
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_rekap_beban($global = 0){
        $this->db->select('d.id as id_lokasi, c.id as id_periode, d.nama as lokasi, c.nama as periode, ifnull(sum(subtotal),0) as total, is_global');

        if(!empty($start = $this->input->get('start')) && $start != ''){
			$this->db->where('tgl_transaksi >=', $start);
		}
		if(!empty($end = $this->input->get('end')) && $end != ''){
			$this->db->where('tgl_transaksi <=', $end);
		}

        $this->db->join('periode c', 'a.id_periode = c.id');
        $this->db->join('location d', 'c.id_lokasi = d.id');
        $this->db->where('a.is_paid', 2);
        // $this->db->where('is_global', $global);
        $this->db->order_by('d.id');
        $this->db->order_by('c.id');
        $this->db->group_by('d.id');
        $this->db->group_by('c.id');
        $result = $this->db->get('expense a');
        if ($result->num_rows() > 0) {
            if(!empty($id)){
                return $result->row_array();
            }else{
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_laporan_beban_detail($id = null){
        $get = $this->input->get();
        $this->db->select('a.*, b.username as input, ifnull(c.nama, "global") as periode, ifnull(d.nama,"global") as lokasi');

        if(!empty($start = $this->input->get('start')) && $start != ''){
			$this->db->where('tgl_transaksi >=', $start);
		}
		if(!empty($end = $this->input->get('end')) && $end != ''){
			$this->db->where('tgl_transaksi <=', $end);
        }
        $this->db->where('is_paid', 2);
        $this->db->where('a.id_periode', $id);
        $this->db->join('user b', 'a.input_by = b.id');
        $this->db->join('periode c', 'a.id_periode = c.id', 'left');
        $this->db->join('location d', 'c.id_lokasi = d.id', 'left');
        $this->db->order_by('a.is_paid');
        $this->db->order_by('a.tgl_transaksi', 'desc');
        $result = $this->db->get('expense a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_total_beban($id = null){
        $get = $this->input->get();
        $this->db->select('ifnull(sum(subtotal), 0) as total');

        if(!empty($start = $this->input->get('start')) && $start != ''){
			$this->db->where('tgl_transaksi >=', $start);
		}
		if(!empty($end = $this->input->get('end')) && $end != ''){
			$this->db->where('tgl_transaksi <=', $end);
        }
        $this->db->where('is_paid', 2);
        $this->db->where('a.id_periode', $id);
        $this->db->order_by('a.is_paid');
        $this->db->order_by('a.tgl_transaksi', 'desc');
        $result = $this->db->get('expense a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    
}