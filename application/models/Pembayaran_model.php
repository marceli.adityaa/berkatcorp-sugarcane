<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_model extends MY_Model {
	public $_table = 'payment';
	private $kolom = array();

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get_ordered_date($periode){
        $this->db->select('tgl_bayar, COUNT(id_pekerja) as total_pekerja, SUM(bayaran) as total_bayar');
        $this->db->where('id_periode', $periode);
        $this->db->group_by('tgl_bayar');
        $this->db->order_by('tgl_bayar', 'desc');
        $result = $this->db->get('payment');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
	
	public function get_detail_payment($periode, $date){
		$this->db->select('a.*, b.nama as pekerja, c.username as input');
		$this->db->join('worker b', 'a.id_pekerja = b.id');
		$this->db->join('user c', 'a.input_by = c.id');
		$this->db->where('a.id_periode', $periode);
		$this->db->where('a.tgl_bayar', $date);
        $this->db->order_by('b.nama');
        $result = $this->db->get('payment a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    
    public function get_laporan_pembayaran($id = null){
        $this->db->select('b.id as periode, ifnull(sum(bayaran), 0) as bayaran, c.nama as lokasi, b.nama as nama_periode, b.selesai');
        $this->db->order_by('b.id_lokasi');
        $this->db->order_by('b.id');
        $this->db->group_by('b.id');
        if(!empty($lokasi = $this->input->get('lokasi')) && $lokasi != 'all'){
			$this->db->where('b.id_lokasi', $lokasi);
		}
		if(!empty($periode = $this->input->get('periode')) && $periode != 'all'){
			$this->db->where('b.id', $periode);
        }
        if(!empty($id)){
            $this->db->where('b.id', $id);
        }
        $this->db->join('periode b', 'a.id_periode = b.id', 'right');
        $this->db->join('location c', 'b.id_lokasi = c.id');
        $result = $this->db->get('payment a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_rekap_pembayaran($global = 0){
        $this->db->select('c.id as id_lokasi, b.id as id_periode, c.nama as lokasi, b.nama as periode, ifnull(sum(bayaran), 0) as total, is_global');
        $this->db->order_by('c.id');
        $this->db->order_by('b.id');
        $this->db->group_by('b.id');
        $this->db->group_by('c.id');
        if(!empty($start = $this->input->get('start'))){
			$this->db->where('tgl_bayar >=', $start);
		}
		if(!empty($end = $this->input->get('end'))){
			$this->db->where('tgl_bayar <=', $end);
        }
        $this->db->join('periode b', 'a.id_periode = b.id', 'right');
        $this->db->join('location c', 'b.id_lokasi = c.id');
        // $this->db->where('is_global', $global);
        $result = $this->db->get('payment a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_laporan_pembayaran_detail($id){
        $this->db->select('a.*, b.nama as pekerja');
        $this->db->order_by('a.tgl_bayar', 'desc');
        $this->db->order_by('b.nama');
        $this->db->where('a.id_periode', $id);
        if(!empty($start = $this->input->get('start'))){
			$this->db->where('tgl_bayar >=', $start);
		}
		if(!empty($end = $this->input->get('end'))){
			$this->db->where('tgl_bayar <=', $end);
        }
        $this->db->join('worker b', 'a.id_pekerja = b.id');
        $result = $this->db->get('payment a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_total_pembayaran($id){
        $this->db->select('ifnull(sum(a.bayaran), 0) as total');
        $this->db->order_by('a.tgl_bayar', 'desc');
        $this->db->order_by('b.nama');
        $this->db->where('a.id_periode', $id);
        if(!empty($start = $this->input->get('start'))){
			$this->db->where('tgl_bayar >=', $start);
		}
		if(!empty($end = $this->input->get('end'))){
			$this->db->where('tgl_bayar <=', $end);
        }
        $this->db->join('worker b', 'a.id_pekerja = b.id');
        $result = $this->db->get('payment a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

	public function get_total_payment($periode, $date){
		$this->db->select('sum(bayaran) as total');
		$this->db->where('a.id_periode', $periode);
		$this->db->where('a.tgl_bayar', $date);
		// $this->db->group_by('id_periode');
        $result = $this->db->get('payment a');
        if ($result->num_rows() > 0) {
            return $result->row_array()['total'];
        } else {
            return false;
        }
	}
}