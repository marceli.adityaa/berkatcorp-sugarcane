<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barangkeluar_model extends MY_Model
{
    public $_table = 'outstock';
    private $kolom = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        if(!empty($id)){
            $this->db->where('id', $id);
        }
        $this->db->order_by('id', 'desc');
        $result = $this->db->get('outstock');
        if ($result->num_rows() > 0) {
            if(!empty($id)){
                return $result->row_array();
            }else{
                return $result->result_array();
            }
        } else {
            return 0;
        }
    }

    public function get_data_barangkeluar($id = null)
    {
        $this->db->select('a.*, b.nama as input, c.nama as lokasi, d.nama as periode, sum(ifnull(e.subtotal, 0)) as total');
        $this->db->join('user b', 'a.input_by = b.id');
        $this->db->join('location c', 'a.id_lokasi = c.id');
        $this->db->join('periode d', 'a.id_periode = d.id');
        $this->db->join('outstock_detail e', 'a.id = e.id_outstock', 'left');
        $this->db->order_by('is_acc');
        $this->db->order_by('tgl_keluar', 'desc');
        $this->db->group_by('a.id');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $get = $this->input->get();
        if (isset($get['lokasi']) && $get['lokasi'] != 'all') {
            $this->db->where('a.id_lokasi', $get['lokasi']);
        }
        if (isset($get['periode']) && $get['periode'] != 'all') {
            $this->db->where('a.id_periode', $get['periode']);
        }
        if (!empty($status = $this->input->get('status')) && $status != 'all') {
            $this->db->where('a.is_acc', $status);
        }
        $result = $this->db->get('outstock a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return 0;
        }
    }

    public function get_rekap_barangkeluar($global = 0)
    {
        $this->db->select('c.id as id_lokasi, d.id as id_periode, c.nama as lokasi, d.nama as periode, sum(ifnull(e.subtotal, 0)) as total, is_global');
        $this->db->join('location c', 'a.id_lokasi = c.id');
        $this->db->join('periode d', 'a.id_periode = d.id');
        $this->db->join('outstock_detail e', 'a.id = e.id_outstock', 'left');
        $this->db->where('a.is_acc', 2);
        $this->db->order_by('c.id');
        $this->db->order_by('d.id');
        $this->db->group_by('c.id');
        $this->db->group_by('d.id');
        if(!empty($start = $this->input->get('start'))){
			$this->db->where('tgl_keluar >=', $start);
		}
		if(!empty($end = $this->input->get('end'))){
			$this->db->where('tgl_keluar <=', $end);
        }
        // $this->db->where('is_global', $global);
        $result = $this->db->get('outstock a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return 0;
        }
    }

    public function get_laporan_pemakaian_detail($id){
        $this->db->select('a.*, g.nama as item, ifnull(sum(subtotal), 0) as total, ifnull(sum(kuantitas), 0) as kuantitas, e.satuan');
        $this->db->join('outstock_detail e', 'a.id = e.id_outstock');
        $this->db->join('stock f', 'e.id_stock = f.id');
        $this->db->join('item g', 'f.id_barang = g.id');
        $this->db->order_by('tgl_keluar', 'desc');
        $this->db->group_by('a.id');
        $this->db->group_by('f.id_barang');
        $this->db->where('a.is_acc', 2);
        if(!empty($id)){
            $this->db->where('a.id_periode', $id);
        }
        $get = $this->input->get();
        if(!empty($start = $this->input->get('start'))){
			$this->db->where('tgl_keluar >=', $start);
		}
		if(!empty($end = $this->input->get('end'))){
			$this->db->where('tgl_keluar <=', $end);
        }

        $result = $this->db->get('outstock a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return 0;
        }
    }

    public function get_total_pemakaian($id){
        $this->db->select('ifnull(sum(ifnull(e.subtotal, 0)), 0) as total');
        $this->db->join('outstock_detail e', 'a.id = e.id_outstock', 'left');
        $this->db->order_by('tgl_keluar', 'desc');
        $this->db->where('a.is_acc', 2);
        if(!empty($id)){
            $this->db->where('a.id_periode', $id);
        }
        $get = $this->input->get();
        if(!empty($start = $this->input->get('start'))){
			$this->db->where('tgl_keluar >=', $start);
		}
		if(!empty($end = $this->input->get('end'))){
			$this->db->where('tgl_keluar <=', $end);
        }

        $result = $this->db->get('outstock a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return 0;
        }
    }

    public function get_data_pelunasan_pembelian($id = null)
    {
        $this->db->select('a.*, d.nama as supplier, ifnull(sum(subtotal),0) as total');
        $this->db->join('supplier d', 'a.id_supplier = d.id');
        $this->db->join('outstock_detail e', 'a.id = e.id_pembelian', 'left');
        $this->db->order_by('tgl_beli', 'desc');
        $this->db->group_by('a.id');
        
        if (!empty($supplier = $this->input->get('supplier')) && $supplier != 'all') {
            $this->db->where('d.id', $supplier);
        }
        if (!empty($status = $this->input->get('status')) && $status != 'all') {
            if ($status == 'tempo') {
                $this->db->where('a.is_paid_tempo', 0);
            } else {
                $this->db->where('a.is_paid_tempo', 1);
            }
        }
        $this->db->where('jenis_pembayaran', 'tempo');
        $this->db->where('is_paid', 2);
        $result = $this->db->get('outstock a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return 0;
        }
    }

    public function get_laporan_pembelian($id = null)
    {
        $this->db->select('b.id as periode, ifnull(sum(if(is_paid = 2, subtotal, 0)), 0) as pembelian');
        $this->db->order_by('b.id_lokasi');
        $this->db->order_by('b.id');
        $this->db->group_by('b.id');
        if (!empty($lokasi = $this->input->get('lokasi')) && $lokasi != 'all') {
            $this->db->where('b.id_lokasi', $lokasi);
        }
        if (!empty($periode = $this->input->get('periode')) && $periode != 'all') {
            $this->db->where('b.id', $periode);
        }
        if(!empty($id)){
            $this->db->where('b.id', $id);
        }
		$this->db->join('outstock_detail c', 'a.id = c.id_pembelian');
		$this->db->join('periode b', 'a.id_periode = b.id', 'right');
        $result = $this->db->get('outstock a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
	
	public function get_laporan_pembelian_detail($id)
    {
        $this->db->select('a.*, b.*, c.nama as supplier');
        $this->db->order_by('tgl_beli', 'desc');
        $this->db->order_by('nama_item');
        $this->db->where('a.id_periode', $id);
        $this->db->where('a.is_paid', '2');
        $this->db->join('outstock_detail b', 'a.id = b.id_pembelian');
        $this->db->join('supplier c', 'a.id_supplier = c.id');
        $result = $this->db->get('outstock a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

}
