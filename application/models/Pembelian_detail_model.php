<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian_detail_model extends MY_Model {
	public $_table = 'purchase_detail';
	private $kolom = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function get_total_purchase($id){
		$this->db->select('ifnull(sum(subtotal),0) as total');
		$this->db->where('id_pembelian', $id);
		$result = $this->db->get('purchase_detail');
		if ($result->num_rows() > 0) {
            return $result->row_array()['total'];
        } else {
            return 0;
        }
	}

	public function get_data($id){
		$this->db->select('a.*, b.nama as barang');
		$this->db->where('id_pembelian', $id);
		$this->db->join('item b', 'a.id_barang = b.id');
		$result = $this->db->get('purchase_detail a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

}