<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends MY_Model {
	public $_table = 'menu';
	// public $uuid = 'mstr';

	public function __construct()
	{
		parent::__construct();
	}

	public function get_data()
	{	
		$sess = $this->session->auth['akses'];
		$menu = $this->order_by('urutan')->get_many_by(array('child_of'=>0));
		if($sess != "*"){
			$akses = json_decode($sess);
		}else{
			$akses = array();
			$all = $this->order_by('urutan')->as_array()->get_all();
			foreach($all as $row){
				array_push($akses, $row['uuid']);
			}
		}
		# Generate menu
		foreach ($menu as $index=>$m) {
			if(!empty($m->query_badge)){
				$badge = $this->db->query($m->query_badge)->row_array()['total'];
				$menu[$index]->badge = $badge;
			}
			if (!in_array($m->uuid, $akses)) {
				unset($menu[$index]);
				continue;
			}
			
			$child = $this->order_by('urutan')->get_many_by(array('child_of'=>$m->id));
			if ($child) {
				$m->child = $child;
				foreach ($m->child as $key=>$c) {
					if (!in_array($c->uuid, $akses)) {
						unset($m->child[$key]);
						continue;
					}
				}
			}
		}
		return $menu;
	}
}