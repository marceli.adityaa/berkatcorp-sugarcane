<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User_model extends MY_Model
{
    public $_table = 'user';

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        $this->db->select('a.*, b.nama as level');
        $this->db->order_by('a.status', 'desc');
        $this->db->order_by('b.id', 'asc');
        if (!empty($id)) {
            $this->db->join('user_level b', 'a.id_level = b.id');
            $this->db->where('a.id', $id);
            $this->db->where('a.id_level <>', 1);
            return $this->db->get('user a')->row_array();
        } else {
            $this->db->join('user_level b', 'a.id_level = b.id');
            $this->db->where('a.id_level <>', 1);
            return $this->db->get('user a')->result_array();
        }
    }

    public function get_level()
    {
        $this->db->where('id_level <>', 1);
        return $this->db->get('user_level')->result_array(0);
    }

    public function auth($username, $password)
    {
        $this->db->select('a.*, b.akses');
        $this->db->join('user_level b', 'a.id_level = b.id');
        $result = $this->db->get_where('user a', array('username' => $username, 'password' => $password, 'status' => 1));
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function is_exist($user_id)
    {
        $this->db->where('input_by', $user_id);
        $result = $this->db->get('payment');
        if ($result->num_rows() > 0) {
            return true;
        } else {
            $this->db->where('input_by', $user_id);
            $result2 = $this->db->get('purchase');
            if ($result2->num_rows() > 0) {
                return true;
            } else {
                $this->db->where('is_paid_by', $user_id);
                $result3 = $this->db->get('purchase');
                if ($result3->num_rows() > 0) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}