<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_level_model extends MY_Model {
	public $_table = 'user_level';
	private $kolom = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function get_list_menu(){
		$sql = "SELECT a.uuid as parent_id, a.label as parent_label, b.uuid as child_id, b.label as child_label  FROM `menu` a left join menu b on a.id = b.child_of where a.child_of = 0 order by a.urutan";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}
}