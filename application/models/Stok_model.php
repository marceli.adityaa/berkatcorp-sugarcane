<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stok_model extends MY_Model
{
    public $_table = 'stock';
    private $kolom = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data_barang($id_barang = null){
        if(!empty($id_barang)){
            $this->db->where('id_barang', $id_barang);
        }
        $this->db->where('stok_sisa >', '0');
        $this->db->order_by('tgl_masuk');
        $result = $this->db->get('stock');
        return $result->result_array();
    }

    public function get_all_stok($id = null)
    {
        $this->db->select('a.*, b.nama as barang, c.username as input_by');
        if (!empty($id)) {
            $this->db->where('a.id', $id);
        }
        if (!empty($barang = $this->input->get('barang')) && $barang != 'all') {
            $this->db->where('a.id_barang', $barang);
        }
        if (!empty($start = $this->input->get('start')) && $start != '') {
            $this->db->where('a.tgl_masuk >=', $start);
        }
        if (!empty($end = $this->input->get('end')) && $end != '') {
            $this->db->where('a.tgl_masuk <=', $end);
        }
        if (!empty($status = $this->input->get('status')) && $status != 'all') {
            if ($status == 'ada') {
                $this->db->where('a.stok_sisa >', 0);
            } else if ($status == 'kosong') {
                $this->db->where('a.stok_sisa', 0);
            }
        }
        $this->db->join('item b', 'a.id_barang = b.id');
        $this->db->join('user c', 'a.input_by = c.id');
        $this->db->order_by('a.stok_sisa', 'desc');
        $result = $this->db->get('stock a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_rekap_stok($id = null)
    {
        $this->db->select('b.id, b.nama as barang, sum(stok_awal) as total_stok, sum(stok_sisa * harga) as nilai_stok, sum(stok_sisa) as total_sisa, sum((stok_awal-stok_sisa) * harga) as total_terpakai, a.satuan');
        if (!empty($id)) {
            $this->db->where('a.id', $id);
        }
        if (!empty($status = $this->input->get('status')) && $status != 'all') {
            if ($status == 'ada') {
                $this->db->where('a.stok_sisa >', 0);
            } else if ($status == 'kosong') {
                $this->db->where('a.stok_sisa', 0);
            }
        }
        $this->db->where('b.status', 1);
        $this->db->join('item b', 'a.id_barang = b.id', 'right');
        $this->db->group_by('b.id');
        $this->db->order_by('b.nama', 'asc');
        $result = $this->db->get('stock a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_stok($barang)
    {
        $this->db->select('a.*, b.nama as barang, sum(stok_sisa) as stok');
        if (!empty($barang)) {
            $this->db->where('id_barang', $barang);
        }
        $this->db->join('item b', 'a.id_barang = b.id', 'right');
        $this->db->where('stok_sisa >', 0);
        $this->db->group_by('b.id');
        $this->db->order_by('barang');
        $result = $this->db->get('stock a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_stok_detail($barang)
    {
        $this->db->select('a.*, b.nama as barang, sum(stok_awal) as stok');
        if (!empty($barang)) {
            $this->db->where('id_barang', $barang);
        }
        $this->db->join('item b', 'a.id_barang = b.id', 'right');
        $this->db->group_by('b.id');
        $this->db->order_by('barang');
        $result = $this->db->get('stock a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_riwayat_stok($id){
        $this->db->select('d.nama as lokasi, e.nama as periode, username as input, b.tgl_keluar, a.kuantitas, a.satuan, a.harga, a.subtotal');
        $this->db->join('outstock b', 'a.id_outstock = b.id');
        $this->db->join('user c', 'b.input_by = c.id');
        $this->db->join('location d', 'b.id_lokasi = d.id');
        $this->db->join('periode e', 'b.id_periode = e.id');
        $this->db->where('id_stock', $id);
        $this->db->order_by('b.tgl_keluar', 'desc');
        $result = $this->db->get('outstock_detail a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_rekap_riwayat_stok($id){
        $this->db->select('g.nama as barang, d.nama as lokasi, e.nama as periode, username as input, c.tgl_keluar, b.kuantitas, b.satuan, b.harga, b.subtotal');
        $this->db->join('outstock_detail b', 'a.id = b.id_stock');
        $this->db->join('outstock c', 'b.id_outstock = c.id');
        $this->db->join('location d', 'c.id_lokasi = d.id');
        $this->db->join('periode e', 'c.id_periode = e.id');
        $this->db->join('user f', 'c.input_by = f.id');
        $this->db->join('item g', 'a.id_barang = g.id');
        $this->db->where('id_barang', $id);
        $this->db->order_by('c.tgl_keluar', 'desc');
        $result = $this->db->get('stock a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}
