<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode_model extends MY_Model {
	public $_table = 'periode';
	private $kolom = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function get_periode_lokasi($lokasi){
		$this->db->select('a.*, b.nama as lokasi, ifnull(sum(c.bayaran),0) as total');
		$this->db->join('location b', 'a.id_lokasi = b.id');
		$this->db->join('payment c', 'a.id = c.id_periode', 'left');
		$this->db->order_by('selesai', 'asc');
		$this->db->order_by('tgl_mulai', 'desc');
		$this->db->group_by('a.id');
		$this->db->where('id_lokasi', $lokasi);
		$result = $this->db->get('periode a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_periode_pendapatan_lokasi($lokasi){
		$this->db->select('a.*, b.nama as lokasi, ifnull(sum(c.subtotal),0) as total, ifnull(sum(c.kuantitas), 0) as produksi');
		$this->db->join('location b', 'a.id_lokasi = b.id');
		$this->db->join('revenue c', 'a.id = c.id_periode', 'left');
		$this->db->order_by('selesai', 'asc');
		$this->db->order_by('tgl_mulai', 'desc');
		$this->db->group_by('a.id');
		$this->db->where('a.id_lokasi', $lokasi);
		$result = $this->db->get('periode a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
}