<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barangkeluar_detail_model extends MY_Model {
	public $_table = 'outstock_detail';
	private $kolom = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function get_total_barangkeluar($id){
		$this->db->select('ifnull(sum(subtotal),0) as total');
		$this->db->where('id_outstock', $id);
		$result = $this->db->get('outstock_detail');
		if ($result->num_rows() > 0) {
            return $result->row_array()['total'];
        } else {
            return 0;
        }
	}

	public function get_data($id){
		$this->db->select('a.*, c.nama as barang');
		$this->db->join('stock b', 'a.id_stock = b.id');
		$this->db->join('item c', 'b.id_barang = c.id');
		$this->db->where('id_outstock', $id);
		$result = $this->db->get('outstock_detail a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_data_waiting($id){
		$this->db->select('a.*, b.nama as barang');
		$this->db->join('item b', 'a.id_barang = b.id');
		$this->db->where('a.id_outstock', $id);
		$result = $this->db->get('outstock_waiting a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_barangkeluar(){
		$this->db->select('a.*');
		$this->db->where('is_acc <', 3);
		$this->db->join('outstock b', 'a.id_outstock = b.id');
		$result = $this->db->get('outstock_detail a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

}