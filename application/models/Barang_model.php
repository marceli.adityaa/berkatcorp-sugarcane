<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends MY_Model {
	public $_table = 'item';
	private $kolom = array();

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get_data($id = null){
        $this->db->select('a.*, b.nama as unit');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $this->db->join('unit b', 'a.satuan = b.id');
        $this->db->order_by('a.status', 'desc');
        $this->db->order_by('a.nama');
        $result = $this->db->get('item a');
        if ($result->num_rows() > 0) {
            if(!empty($id)){
                return $result->row_array();
            }else{
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_active()
    {
        $this->db->select('a.id, a.nama as barang, b.nama as satuan, sum(stok_sisa) as stok');
        $this->db->join('unit b', 'a.satuan = b.id');
        $this->db->join('stock c', 'a.id = c.id_barang', 'left');
        $this->db->where('a.status', 1);
        $this->db->group_by('a.id');
        $this->db->having('stok >', 0);
        $this->db->order_by('a.nama');
        $result = $this->db->get('item a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_all_active()
    {
        $this->db->select('a.id, a.nama as barang, b.nama as satuan, sum(stok_sisa) as stok');
        $this->db->join('unit b', 'a.satuan = b.id');
        $this->db->join('stock c', 'a.id = c.id_barang', 'left');
        $this->db->where('a.status', 1);
        $this->db->group_by('a.id');
        $this->db->order_by('a.nama');
        $result = $this->db->get('item a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}