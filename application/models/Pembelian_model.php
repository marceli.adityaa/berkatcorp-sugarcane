<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian_model extends MY_Model
{
    public $_table = 'purchase';
    private $kolom = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function get_last_transaction($tgl_beli)
    {
        $this->db->where('tgl_beli', $tgl_beli);
        $this->db->order_by('id', 'desc');
        $result = $this->db->get('purchase');
        if ($result->num_rows() > 0) {
            return $result->row_array()['invoice'];
        } else {
            return 0;
        }
    }

    public function get_data_pembelian($id = null)
    {
        $this->db->select('a.*, d.nama as supplier, ifnull(sum(subtotal),0) as total');
        $this->db->join('supplier d', 'a.id_supplier = d.id');
        $this->db->join('purchase_detail e', 'a.id = e.id_pembelian', 'left');
        $this->db->order_by('tgl_beli', 'desc');
        $this->db->group_by('a.id');
        
        if (!empty($supplier = $this->input->get('supplier')) && $supplier != 'all') {
            $this->db->where('d.id', $supplier);
        }
        if (!empty($type = $this->input->get('type')) && $type != 'all') {
            $this->db->where('a.jenis_pembayaran', $type);
        }
        if (!empty($status = $this->input->get('status')) && $status != 'all') {
            $this->db->where('a.is_paid', $status);
        }
        $result = $this->db->get('purchase a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return 0;
        }
    }

    public function get_data_pelunasan_pembelian($id = null)
    {
        $this->db->select('a.*, d.nama as supplier, ifnull(sum(subtotal),0) as total');
        $this->db->join('supplier d', 'a.id_supplier = d.id');
        $this->db->join('purchase_detail e', 'a.id = e.id_pembelian', 'left');
        $this->db->order_by('tgl_beli', 'desc');
        $this->db->group_by('a.id');
        
        if (!empty($supplier = $this->input->get('supplier')) && $supplier != 'all') {
            $this->db->where('d.id', $supplier);
        }
        if (!empty($status = $this->input->get('status')) && $status != 'all') {
            if ($status == 'tempo') {
                $this->db->where('a.is_paid_tempo', 0);
            } else {
                $this->db->where('a.is_paid_tempo', 1);
            }
        }
        $this->db->where('jenis_pembayaran', 'tempo');
        $this->db->where('is_paid', 2);
        $result = $this->db->get('purchase a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return 0;
        }
    }

    public function get_laporan_pembelian($id = null)
    {
        $this->db->select('b.id as periode, ifnull(sum(if(is_paid = 2, subtotal, 0)), 0) as pembelian');
        $this->db->order_by('b.id_lokasi');
        $this->db->order_by('b.id');
        $this->db->group_by('b.id');
        if (!empty($lokasi = $this->input->get('lokasi')) && $lokasi != 'all') {
            $this->db->where('b.id_lokasi', $lokasi);
        }
        if (!empty($periode = $this->input->get('periode')) && $periode != 'all') {
            $this->db->where('b.id', $periode);
        }
        if(!empty($id)){
            $this->db->where('b.id', $id);
        }
		$this->db->join('purchase_detail c', 'a.id = c.id_pembelian');
		$this->db->join('periode b', 'a.id_periode = b.id', 'right');
        $result = $this->db->get('purchase a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    
    public function get_rekap_pembelian($id = null)
    {
        $this->db->select('ifnull(sum(if(is_paid = 2, subtotal, 0)), 0) as pembelian');
        // $this->db->group_by('a.tgl_beli');
        if(!empty($start = $this->input->get('start'))){
			$this->db->where('tgl_beli >=', $start);
		}
		if(!empty($end = $this->input->get('end'))){
			$this->db->where('tgl_beli <=', $end);
        }
        if(!empty($id)){
            $this->db->where('b.id', $id);
        }
		$this->db->join('purchase_detail c', 'a.id = c.id_pembelian');
        $result = $this->db->get('purchase a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
	
	public function get_laporan_pembelian_detail($id)
    {
        $this->db->select('a.*, b.*, c.nama as supplier');
        $this->db->order_by('tgl_beli', 'desc');
        $this->db->order_by('nama_item');
        $this->db->where('a.id_periode', $id);
        $this->db->where('a.is_paid', '2');
        $this->db->join('purchase_detail b', 'a.id = b.id_pembelian');
        $this->db->join('supplier c', 'a.id_supplier = c.id');
        $result = $this->db->get('purchase a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

}
