<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_model extends MY_Model {
	public $_table = 'bank';
	private $kolom = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function get_active_bank()
    {
        $this->db->where('status', 1);
        $this->db->order_by('bank');
        $this->db->order_by('nama');
        $result = $this->db->get('bank');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_active_group_bank()
    {
        $this->db->where('status', 1);
        $this->db->order_by('bank');
        $this->db->group_by('bank');
        $result = $this->db->get('bank');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}