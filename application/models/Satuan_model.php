<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Satuan_model extends MY_Model
{
    public $_table = 'unit';
    private $kolom = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function get_active()
    {
		$this->db->where('status', 1);
		$this->db->order_by('nama');
        $result = $this->db->get('unit');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}
