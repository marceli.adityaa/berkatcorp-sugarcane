<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barangkeluar_waiting_model extends MY_Model {
	public $_table = 'outstock_waiting';
	private $kolom = array();

	public function __construct()
	{
		parent::__construct();
    }
    
    public function get_data($id){
		$this->db->select('a.*, b.nama as barang');
		$this->db->join('item b', 'a.id_barang = b.id');
		$this->db->where('id_outstock', $id);
		$result = $this->db->get('outstock_waiting a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_stok_waiting($barang){
		$this->db->select('sum(kuantitas) as stok');
		$this->db->join('outstock b', 'a.id_outstock = b.id');
		$this->db->where('b.is_acc', 1);
		$this->db->where('id_barang', $barang);
		$result = $this->db->get('outstock_waiting a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

}