<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi_model extends MY_Model {
	public $_table = 'location';
	private $kolom = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function get_active(){
		$this->db->where('status', 1);
		$this->db->order_by('nama');
		$result = $this->db->get('location');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_non_global(){
		$this->db->where('status', 1);
		$this->db->where('is_global', 0);
		$this->db->order_by('id');
		$result = $this->db->get('location');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_data_with_transaction(){
		$periode = array();
		$akun = array("pembayaran", "pemakaian", "beban", "pendapatan");

		# Cek pembayaran
		$this->db->select('id_periode');
		if(!empty($start = $this->input->get('start'))){
			$this->db->where('tgl_bayar >=', $start);
		}
		if(!empty($end = $this->input->get('end'))){
			$this->db->where('tgl_bayar <=', $end);
		}
		$this->db->group_by('id_periode');
		$data['pembayaran'] = $this->db->get('payment');
		if($data['pembayaran']->num_rows() > 0){
			$data['pembayaran'] = $data['pembayaran']->result_array();
		}else{
			$data['pembayaran'] = null;
		}

		# Cek Pemakaian
		$this->db->select('id_periode');
		if(!empty($start = $this->input->get('start'))){
			$this->db->where('tgl_keluar >=', $start);
		}
		if(!empty($end = $this->input->get('end'))){
			$this->db->where('tgl_keluar <=', $end);
		}
		$this->db->group_by('id_periode');
		$data['pemakaian'] = $this->db->get('outstock');
		if($data['pemakaian']->num_rows() > 0){
			$data['pemakaian'] = $data['pemakaian']->result_array();
		}else{
			$data['pemakaian'] = null;
		}

		# Cek Beban
		$this->db->select('id_periode');
		if(!empty($start = $this->input->get('start'))){
			$this->db->where('tgl_transaksi >=', $start);
		}
		if(!empty($end = $this->input->get('end'))){
			$this->db->where('tgl_transaksi <=', $end);
		}
		$this->db->group_by('id_periode');
		$data['beban'] = $this->db->get('expense');
		if($data['beban']->num_rows() > 0){
			$data['beban'] = $data['beban']->result_array();
		}else{
			$data['beban'] = null;
		}

		# Cek Pendapatan
		$this->db->select('id_periode');
		if(!empty($start = $this->input->get('start'))){
			$this->db->where('tgl_transaksi >=', $start);
		}
		if(!empty($end = $this->input->get('end'))){
			$this->db->where('tgl_transaksi <=', $end);
		}
		$this->db->group_by('id_periode');
		$data['pendapatan'] = $this->db->get('revenue');
		if($data['pendapatan']->num_rows() > 0){
			$data['pendapatan'] = $data['pendapatan']->result_array();
		}else{
			$data['pendapatan'] = null;
		}

		foreach($akun as $list){
			if(!empty($data[$list])){
				foreach($data[$list] as $row){
					if(!in_array($row['id_periode'], $periode)){
						array_push($periode, $row['id_periode']);
					}
				}
			}
		}
		if(!empty($periode)){
			$this->db->select('a.id as id_lokasi, b.id as id_periode, a.nama as lokasi, b.nama as periode, is_global');
			$this->db->join('periode b', 'a.id = b.id_lokasi');
			$this->db->where_in('b.id', $periode);
			$result = $this->db->get('location a');
			if ($result->num_rows() > 0) {
				return $result->result_array();
			} else {
				return false;
			}
		}else{
			return false;
		}
	}
}