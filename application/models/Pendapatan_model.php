<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pendapatan_model extends MY_Model
{
    public $_table = 'revenue';
    private $kolom = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data_pendapatan($id = null)
    {
        $get = $this->input->get();
        $this->db->order_by('tgl_transaksi', 'desc');
        $this->db->order_by('is_paid');
        if (isset($id)) {
            $this->db->where('id_periode', $id);
        }
        if (isset($get['lokasi']) && $get['lokasi'] != 'all') {
            $this->db->where('id_lokasi', $get['lokasi']);
        }
        if (isset($get['periode']) && $get['periode'] != 'all') {
            $this->db->where('id_periode', $get['periode']);
        }

        if (!empty($start = $this->input->get('start')) && $start != '') {
            $this->db->where('tgl_transaksi >=', $start);
        }
        if (!empty($end = $this->input->get('end')) && $end != '') {
            $this->db->where('tgl_transaksi <=', $end);
        }
        if (!empty($status = $this->input->get('status')) && $status != 'all') {
            $this->db->where('is_paid', $status);
        }
        $result = $this->db->get('revenue');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return 0;
        }
    }

    public function get_total_pendapatan($id = null)
    {
        $this->db->select('ifnull(SUM(subtotal), 0) as total');
        $this->db->order_by('tgl_transaksi', 'desc');
        $this->db->where('id_periode', $id);
        $this->db->where('is_paid !=', 3);
        if (!empty($start = $this->input->get('start')) && $start != '') {
            $this->db->where('tgl_transaksi >=', $start);
        }
        if (!empty($end = $this->input->get('end')) && $end != '') {
            $this->db->where('tgl_transaksi <=', $end);
        }
        if (!empty($status = $this->input->get('status')) && $status != 'all') {
            $this->db->where('is_paid', $status);
        }
        $result = $this->db->get('revenue');
        if ($result->num_rows() > 0) {
            return $result->row_array()['total'];
        } else {
            return 0;
        }
    }

    public function get_total_produksi($id = null)
    {
        $this->db->select('ifnull(SUM(kuantitas), 0) as total');
        $this->db->order_by('tgl_transaksi', 'desc');
        $this->db->where('id_periode', $id);
        $this->db->where('is_paid !=', 3);
        if (!empty($start = $this->input->get('start')) && $start != '') {
            $this->db->where('tgl_transaksi >=', $start);
        }
        if (!empty($end = $this->input->get('end')) && $end != '') {
            $this->db->where('tgl_transaksi <=', $end);
        }
        if (!empty($status = $this->input->get('status')) && $status != 'all') {
            $this->db->where('is_paid', $status);
        }
        $result = $this->db->get('revenue');
        if ($result->num_rows() > 0) {
            return $result->row_array()['total'];
        } else {
            return 0;
        }
    }

    public function get_laporan_pendapatan($id = null)
    {
        $this->db->select('b.id as periode, ifnull(sum(if(is_paid = 2, subtotal, 0)), 0) as pendapatan');
        $this->db->order_by('b.id_lokasi');
        $this->db->order_by('b.id');
        $this->db->group_by('b.id');
        $get = $this->input->get();
        if (isset($get['lokasi']) && $get['lokasi'] != 'all') {
            $this->db->where('b.id_lokasi', $get['lokasi']);
        }
        if (isset($get['periode']) && $get['periode'] != 'all') {
            $this->db->where('b.id', $get['periode']);
        }
        if (!empty($id)) {
            $this->db->where('b.id', $id);
        }
        $this->db->join('periode b', 'a.id_periode = b.id', 'right');
        $result = $this->db->get('revenue a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_rekap_pendapatan($global = 0)
    {
        $this->db->select('c.id as id_lokasi, b.id as id_periode, c.nama as lokasi, b.nama as periode, ifnull(sum(if(is_paid = 2, subtotal, 0)), 0) as total, is_global');
        $this->db->where('is_paid', 2);
        $this->db->order_by('c.id');
        $this->db->order_by('b.id');
        $this->db->group_by('c.id');
        $this->db->group_by('b.id');
        if (!empty($start = $this->input->get('start'))) {
            $this->db->where('tgl_transaksi >=', $start);
        }
        if (!empty($end = $this->input->get('end'))) {
            $this->db->where('tgl_transaksi <=', $end);
        }
        $this->db->join('periode b', 'a.id_periode = b.id', 'right');
        $this->db->join('location c', 'b.id_lokasi = c.id', 'right');
        // $this->db->where('is_global', $global);
        $result = $this->db->get('revenue a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_laporan_pendapatan_detail($id)
    {
		$get = $this->input->get();
		$this->db->select('a.*, b.nama as periode, c.nama as lokasi');
        $this->db->order_by('a.tgl_transaksi', 'desc');
        $this->db->order_by('a.is_paid');
        $this->db->where('a.id_periode', $id);
		$this->db->where('a.is_paid', 2);

        if (!empty($start = $this->input->get('start')) && $start != '') {
            $this->db->where('tgl_transaksi >=', $start);
        }
        if (!empty($end = $this->input->get('end')) && $end != '') {
            $this->db->where('tgl_transaksi <=', $end);
		}
		$this->db->join('periode b', 'a.id_periode = b.id');
		$this->db->join('location c', 'b.id_lokasi = c.id');
        $result = $this->db->get('revenue a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return 0;
        }
	}
	
	public function get_total_pendapatan_acc($id)
    {
		$get = $this->input->get();
		$this->db->select('ifnull(sum(subtotal), 0) as total');
        $this->db->order_by('a.tgl_transaksi', 'desc');
        $this->db->order_by('a.is_paid');
        $this->db->where('a.id_periode', $id);
		$this->db->where('a.is_paid', 2);

        if (!empty($start = $this->input->get('start')) && $start != '') {
            $this->db->where('tgl_transaksi >=', $start);
        }
        if (!empty($end = $this->input->get('end')) && $end != '') {
            $this->db->where('tgl_transaksi <=', $end);
		}
		$this->db->join('periode b', 'a.id_periode = b.id');
		$this->db->join('location c', 'b.id_lokasi = c.id');
        $result = $this->db->get('revenue a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return 0;
        }
    }

}
