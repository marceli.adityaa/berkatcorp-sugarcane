<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Api_controller extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function get($token = '')
	{
		# Autentikasi Token
		$this->auth($token);

		$id = $this->input->get('id');
		echo json_encode($this->model->get($id));
	}

	public function get_many_by($token = '')
	{
		# Autentikasi Token
		$this->auth($token);

		$filter = $this->input->get();
		echo json_encode($this->model->get_many_by($filter));
	}

	public function get_all($token = '')
	{
		# Autentikasi Token
		$this->auth($token);
		echo json_encode($this->model->get_all());
	}

	public function save($token = '')
	{
		# Autentikasi Token
		$this->auth($token);

		$data = $this->input->post();

		if (!$data['id']) {
			# Insert
			$this->model->insert($data);
			die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di simpan')));
		} else {
			# Update
			$id = $data['id'];
			unset($data['id']);
			$this->model->update($id, $data);
			die(json_encode(array('status' => 'success', 'message' => 'Data berhasil di sunting')));
		}
	}

	public function delete()
	{
		$id = $this->input->post('id');
		if ($id) {
			$this->model->delete($id);
			die(json_encode(array('status' => 'success', 'message' => 'data berhasil dihapus')));
		}

		show_404();
	}

	public function get_table_data($token = '')
	{
		# Autentikasi Token
		$this->auth($token);

		$filter = $this->input->get();
		$data = $this->model->get_data($filter);
		foreach ($data['rows'] as $k => $v) {
			$v->nomor = $k + 1;
			# Aksi==============================
			$v->aksi  = "<div class='d-flex'><button class='flex-fill btn btn-warning btn-sm' onclick='edit(" . $v->id . ")'><i class='icon-pencil'></i></button> ";
			$v->aksi .= "<button class='flex-fill btn btn-danger btn-sm' onclick='remove(" . $v->id . ")'><i class='icon-trash'></i></button></div>";
		}
		echo json_encode($data);
	}

	public function auth($token)
	{
		if ($token !== $this->session->auth['token']) {
			show_404();
		}
	}
}
