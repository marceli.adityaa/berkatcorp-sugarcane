<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MY_Base_controller {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function _remap($method, $params = array())
	{
		if (method_exists($this, $method)) {
			if ($this->session->userdata('auth')) {
				return call_user_func_array(array($this, $method), $params);
			} else {
				$this->go('login');
			}
		} else {
			show_404();
		}
	}

	protected function render($view, $data = array())
	{
		$data['menu'] = $this->generateMenu();
		$this->blade->render('modules/'.$view, $data);
	}

	private function generateMenu()
	{
		# Ambil menu
		$this->load->model('Menu_model', 'menu');

		$menu = $this->cache->get('menu');
		# Jika cache tidak terdaftar
		if (!$menu OR $menu) {
			$menu = $this->menu->get_data();
			$this->cache->save('menu', $menu);
		}
		return $menu;
	}
}