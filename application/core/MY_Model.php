<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends MY_Base_model {

    public $before_create = array( 'log_create' );
    public $before_update = array( 'log_update' );

    public function __construct()
    {
        parent::__construct();
    }

    public function select($field) {
        $this->_database->select($field);
        return $this;
    }

    public function join($with, $on) {
        $this->_database->join($with, $on);
        return $this;
    }

    public function group_start()
    {
        $this->_database->group_start();
        return $this;
    }

    public function group_end()
    {
        $this->_database->group_end();
        return $this;
    }

    public function like($field, $match = NULL, $side = 'both', $escape = NULL) {
        $this->_database->like($field, $match, $side, $escape);
        return $this;
    }

    public function or_like($field, $match = NULL, $side = 'both', $escape = NULL) {
        $this->_database->or_like($field, $match, $side, $escape);
        return $this;
    }

    public function where($field, $match = NULL) {
        $this->_database->where($field, $match);
        return $this;
    }

    public function or_where($field, $match = NULL) {
        $this->_database->or_where($field, $match);
        return $this;
    }

    public function where_in($field, $match = NULL) {
        $this->_database->where_in($field, $match);
        return $this;
    }

    public function where_not_in($field, $match = NULL) {
        $this->_database->where_not_in($field, $match);
        return $this;
    }

    public function group_by($field, $escape = NULL) {
        $this->_database->group_by($field, $escape);
        return $this;
    }

    public function batch_insert($data)
    {
        return $this->db->insert_batch($this->_table, $data);
    }

    public function batch_update($id, $data)
    {
        $this->db->where_in('id', $id);
        return $this->db->update($this->_table, $data);
    }

    public function batch_delete($id)
    {
        $this->db->where_in('id', $id);
        return $this->db->delete($this->_table);
    }

    protected function log_create($data)
    {
        // $data['insert_time'] = date('Y-m-d H:i:s');
        // $data['insert_by']   = isset($this->session->auth['id']) ? $this->session->auth['id'] : '';
        return $data;
    }

    protected function log_update($data)
    {
        // $data['update_time'] = date('Y-m-d H:i:s');
        // $data['update_by']   = isset($this->session->auth['id']) ? $this->session->auth['id'] : '';
        return $data;
    }
}