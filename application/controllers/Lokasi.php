<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lokasi extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('lokasi_model', 'lokasi');
    }

    public function index()
    {
        $data['lokasi'] = $this->lokasi->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
        $data['kepemilikan'] = array("sewa", "gadai", "milik pribadi");
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Lokasi' => 'active'));
        set_session('title', 'Lokasi');
        set_activemenu('', 'menu-lokasi');
        $this->render('lokasi/v-lokasi', $data);
    }

    public function submit_form()
    {
        $post = $this->input->post();
        $post['luas'] = intval(preg_replace('/[^\d.]/', '', $post['luas']));
        if (!isset($post['id'])) {
            # Insert Statement
            $result = $this->lokasi->insert($post);
            if ($result) {
                $this->message('Sukses memasukkan data', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
        } else {
            # Update Statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->lokasi->update($id, $post);
            if ($result) {
                $this->message('Sukses mengubah data', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
        }
        $this->go('lokasi');
    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $response = $this->lokasi->as_array()->get($id);
        echo json_encode($response);
    }

    public function set_aktif()
    {
        $id = $this->input->post('id');
        $response = $this->lokasi->update($id, array('status' => 1));
        echo json_encode($response);
    }

    public function set_nonaktif()
    {
        $id = $this->input->post('id');
        $response = $this->lokasi->update($id, array('status' => 0));
        echo json_encode($response);
    }
}
