<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pendapatan extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pendapatan_model', 'pendapatan');
        $this->load->model('lokasi_model', 'lokasi');
        $this->load->model('periode_model', 'periode');
    }

    public function index()
    {
        $data['lokasi'] = $this->lokasi->where('status', 1)->order_by(array('nama' => 'asc'))->as_array()->get_all();
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pendapatan' => 'active'));
        set_session('title', 'Pendapatan - Pilih Lokasi');
        set_activemenu('', 'menu-pendapatan');
        $this->render('pendapatan/v-pendapatan-lokasi', $data);
    }

    public function lokasi($id)
    {
        if (isset($id)) {
            $data['lokasi']     = $this->lokasi->as_array()->get($id);
            $data['periode']    = $this->periode->get_periode_pendapatan_lokasi($id);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pendapatan' => base_url('pendapatan'), $data['lokasi']['nama'] => 'active'));
            set_session('title', 'Pendapatan - Pilih Periode');
            set_activemenu('', 'menu-pendapatan');
            $this->render('pendapatan/v-pendapatan-periode', $data);
        }
    }

    public function periode($id)
    {
        if (isset($id)) {
            $data['periode']    = $this->periode->as_array()->get($id);
            $data['lokasi']     = $this->lokasi->as_array()->get($data['periode']['id_lokasi']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pendapatan' => base_url('pendapatan'), $data['lokasi']['nama'] => base_url('pendapatan/lokasi/' . $data['lokasi']['id']), $data['periode']['nama'] => 'active'));
            // $data['pembayaran'] = $this->pendapatan->get_ordered_date($id);
            $data['status']     = array(1 => 'Menunggu', 2 => 'Disetujui', 3 => 'Ditolak');
            $data['pendapatan'] = $this->pendapatan->get_data_pendapatan($id);
            $data['total']      = $this->pendapatan->get_total_pendapatan($id);
            set_session('title', 'Pendapatan');
            set_activemenu('', 'menu-pendapatan');
            // dump($data);
            $this->render('pendapatan/v-pendapatan', $data);
        }
    }

    public function submit_form()
    {
        $post = $this->input->post();
        if(!empty($post['url'])){
            $url = $post['url'];
        }else{
            $url = "";
        }
        unset($post['url']);
        $post['kuantitas'] = intval(preg_replace('/[^\d.]/', '', $post['kuantitas']));
        $post['harga'] = intval(preg_replace('/[^\d.]/', '', $post['harga']));
        $post['subtotal'] = intval(preg_replace('/[^\d.]/', '', $post['subtotal']));
        if(!$post['id']){
            # Insert Statement
            $post['input_by'] = get_session('auth')['user_id'];
            $post['timestamp'] = setNewDateTime();
			$result = $this->pendapatan->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
            }
		}else{
            # Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->pendapatan->update($id, $post);
			if($result){
                $this->message('Sukses mengubah data', 'success');
			}else{
                $this->message('Gagal', 'error');
			}
		}
        $this->go('pendapatan/periode/'.$post['id_periode'].'?'.$url);

    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $response = $this->pendapatan->as_array()->get($id);
        echo json_encode($response);
    }

    public function delete_data()
    {
        $id = $this->input->post('id');
        $result = $this->pendapatan->delete($id);
        if ($result) {
            $this->message('Berhasil menghapus data', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }
}
