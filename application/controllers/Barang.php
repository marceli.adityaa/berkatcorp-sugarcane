<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
        $this->load->model('barang_model', 'barang');
        $this->load->model('satuan_model', 'satuan');
	}

	public function index()
	{
        $data['barang'] = $this->barang->get_data();
        $data['satuan'] = $this->satuan->get_active();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master barang' => 'active'));
        set_session('title', 'barang');
        set_activemenu('sub-master', 'menu-barang');
		$this->render('barang/v-barang', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
            # Insert Statement
			$result = $this->barang->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->barang->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('barang');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->barang->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->barang->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->barang->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
