<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beban extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('beban_model', 'beban');
        $this->load->model('lokasi_model', 'lokasi');
        $this->load->model('periode_model', 'periode');
    }

    public function index()
    {
        $data['status']         = array(1 => 'Menunggu', 2 => 'Disetujui', 3 => 'Ditolak');
        $data['lokasi']         = $this->lokasi->get_active();
        if(!empty($this->input->get())){
            $data['beban']  = $this->beban->get_data();
        }else{
            $data['beban']  = '';
        }
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Beban' => 'active'));
        set_session('title', 'Beban');
        set_activemenu('', 'menu-beban');
        $this->render('beban/v-beban', $data);
    }

    public function submit_form()
    {
        $post = $this->input->post();
        if(!empty($post['url'])){
            $url = $post['url'];
        }else{
            $url = "status=all";
        }
        
        unset($post['url']);
        $post['kuantitas'] = intval(preg_replace('/[^\d.]/', '', $post['kuantitas']));
        $post['harga'] = intval(preg_replace('/[^\d.]/', '', $post['harga']));
        $post['subtotal'] = intval(preg_replace('/[^\d.]/', '', $post['subtotal']));
        if(!$post['id']){
            # Insert Statement
            $post['is_paid'] = 1;
            $post['input_by'] = get_session('auth')['user_id'];
            $post['timestamp'] = setNewDateTime();
			$result = $this->beban->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
            }
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->beban->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
        $this->go('beban?' . $url);

    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $response = $this->beban->as_array()->get($id);
        echo json_encode($response);
    }

    public function delete_data()
    {
        $id = $this->input->post('id');
        $result = $this->beban->delete($id);
        if ($result) {
            $this->message('Berhasil menghapus data', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }
}
