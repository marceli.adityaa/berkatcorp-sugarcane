<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Level extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
        $this->load->model('user_level_model', 'level');
	}

	public function index()
	{
        $data['level'] = $this->level->where('id <>', 1)->order_by('id', 'asc')->as_array()->get_all();
        $data['list_menu'] = $this->level->get_list_menu();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Hak Akses' => 'active'));
        set_session('title', 'Hak Akses');
        set_activemenu('sub-master', 'menu-hak-akses');
		$this->render('user/v-level', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!empty($post['uuid'])){
			$post['akses'] = json_encode($post['uuid']);
		}else{
			$post['akses'] = '';
		}
		unset($post['uuid']);
		if(!$post['id']){
            # Insert Statement
			$result = $this->level->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
            unset($post['id']);
			$result = $this->level->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('level');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->level->as_array()->get($id);
		echo json_encode($response);
	}
}
