<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pelunasan extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian_model', 'pembelian');
        $this->load->model('pembelian_detail_model', 'pembelian_detail');
        $this->load->model('pembelian_tempo_model', 'pembelian_tempo');
        $this->load->model('lokasi_model', 'lokasi');
        $this->load->model('periode_model', 'periode');
        $this->load->model('supplier_model', 'supplier');
        $this->load->model('bank_model', 'bank');
    }

    public function index()
    {
        $data['supplier']       = $this->supplier->where('status', 1)->order_by(array('nama' => 'asc'))->as_array()->get_all();
        $data['status']         = array('tempo', 'lunas');
        if(empty($this->input->get())){
            $_GET['status'] = 'tempo';
        }
        $data['pembelian']  = $this->pembelian->get_data_pelunasan_pembelian();
        $data['group_bank']     = $this->bank->get_active_group_bank();
        $data['bank']           = $this->bank->get_active_bank();
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pelunasan Tempo' => 'active'));
        set_session('title', 'Pelunasan Tempo');
        set_activemenu('', 'menu-pelunasan');
        $this->render('pembelian/v-pembelian-pelunasan', $data);
    }

    public function submit_form()
    {
        $post = $this->input->post();
        if(!empty($post['url'])){
            $url = $post['url'];
        }else{
            $url = "supplier=all&status=all";
        }
        $post['nominal'] = intval(preg_replace('/[^\d.]/', '', $post['nominal']));
        unset($post['url']);
        if(!$post['id']){
            # Insert Statement
            $post['input_by'] = get_session('auth')['user_id'];
            $post['timestamp'] = setNewDateTime();
            $result = $this->pembelian_tempo->insert($post);
			if($result){
                $update = array(
                    'is_paid_tempo' => 1,
                    'is_paid_by' => get_session('auth')['user_id']
                );
                $this->pembelian->update($post['id_pembelian'], $update);
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
            }
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->pembelian_tempo->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
        $this->go('pelunasan?' . $url);

    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $response = $this->pembelian_tempo->where('id_pembelian', $id)->as_array()->get_all()[0];
        echo json_encode($response);
    }

}
