<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Base_controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
    }

    public function _remap($method, $param = array())
    {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $param);
        } else {
            display_404();
        }
	}
	
    public function index()
    {
        $data['captcha'] = captcha();
        set_session('title', 'Login');
        $this->render('login/v-login', $data);
    }

    public function auth(){
        $username = secure($this->input->post('username'));
        $password = md5(secure($this->input->post('password')));
        $auth = $this->user->auth($username, $password);
        if($auth != false){
            # Jika auth berhasil
            $user = array(
                'user_id' => $auth['id'],
                'username' => $auth['username'],
                'nama' => $auth['nama'],
                'level' => $auth['id_level'],
                'akses' => $auth['akses']
                
            );
            $this->session->set_userdata('auth', $user);
            $response = array(
                'status' => 1,
                'message' => 'Login sukses, redirecting..',
                'redirect' => base_url('dashboard')
            );
            
            $this->message('Selamat datang '.ucwords($user['nama']), 'success');
        }else{
            # Jika auth gagal
            $response = array(
                'status' => 0,
                'message' => 'Username / Password salah!',
                'redirect' => '',
            );
        }

        echo json_encode($response);
    }

}
