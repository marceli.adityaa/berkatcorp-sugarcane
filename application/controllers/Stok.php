<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stok extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('stok_model', 'stok');
        $this->load->model('barang_model', 'barang');
    }

    public function index()
    {
        $data['stok'] = $this->stok->get_all_stok();
        $data['barang'] = $this->barang->get_all_active();
        $data['status'] = array('all', 'ada', 'kosong');
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Stok' => 'active'));
        set_session('title', 'Data Stok');
        set_activemenu('', 'menu-stok');
        $this->render('stok/v-stok', $data);
    }

    public function rekap()
    {
        $data['stok'] = $this->stok->get_rekap_stok();
        $data['barang'] = $this->barang->get_active();

        $data['status'] = array('all', 'ada', 'kosong');
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Rekap Stok' => 'active'));
        set_session('title', 'Rekap Stok');
        set_activemenu('', 'menu-rekap-stok');
        $this->render('stok/v-rekap-stok', $data);
    }

    public function submit_form()
    {
        $post = $this->input->post();
        // dump($post);
        if(!empty($post['url'])){
            $url = $post['url'];
        }else{
            $url = "";
        }
        unset($post['url']);
        $post['stok_awal'] = intval(preg_replace('/[^\d.]/', '', $post['stok_awal']));
        $post['harga'] = intval(preg_replace('/[^\d.]/', '', $post['harga']));
        if(!$post['id']){
            # Insert Statement
            $data['id_purchase_detail'] = 0;
            $data['id_barang'] = $post['id_barang'];
            $data['tgl_masuk'] = $post['tgl_masuk'];
            $data['stok_awal'] = $post['stok_awal'];
            $data['stok_sisa'] = $post['stok_awal'];
            $data['satuan'] = $post['satuan'];
            $data['harga'] = $post['harga'];
            $data['input_by'] = get_session('auth')['user_id'];
            $data['timestamp'] = setNewDateTime();
			$result = $this->stok->insert($data);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
            }
		}else{
            # Update Statement
			$data['id_barang'] = $post['id_barang'];
            $data['tgl_masuk'] = $post['tgl_masuk'];
            $data['stok_awal'] = $post['stok_awal'];
            $data['stok_sisa'] = $post['stok_awal'];
            $data['satuan'] = $post['satuan'];
            $data['harga'] = $post['harga'];
            $data['update_by'] = get_session('auth')['user_id'];
            $data['update_timestamp'] = setNewDateTime();
			$result = $this->stok->update($post['id'], $data);
			if($result){
                $this->message('Sukses mengubah data', 'success');
			}else{
                $this->message('Gagal', 'error');
			}
		}
        $this->go('stok?'.$url);

    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $response = $this->stok->as_array()->get($id);
        echo json_encode($response);
    }

    public function delete_data()
    {
        $id = $this->input->post('id');
        $result = $this->stok->delete($id);
        if ($result) {
            $this->message('Berhasil menghapus data', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }

    public function json_get_riwayat_stok(){
        $id = $this->input->post('id');
        $result = $this->stok->get_riwayat_stok($id);
        if ($result) {
            $this->message('Berhasil menghapus data', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }

    public function json_get_rekap_riwayat_stok(){
        $id = $this->input->post('id');
        $result = $this->stok->get_rekap_riwayat_stok($id);
        if ($result) {
            $this->message('Berhasil menghapus data', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }
}
