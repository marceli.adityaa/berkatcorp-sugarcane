<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barangkeluar extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('barangkeluar_model', 'barangkeluar');
        $this->load->model('barangkeluar_detail_model', 'barangkeluar_detail');
        $this->load->model('barangkeluar_waiting_model', 'barangkeluar_waiting');
        $this->load->model('stok_model', 'stok');
        $this->load->model('lokasi_model', 'lokasi');
        $this->load->model('periode_model', 'periode');
        $this->load->model('satuan_model', 'satuan');
        $this->load->model('barang_model', 'barang');
    }

    public function index()
    {
        $data['payment_type']   = array('cash', 'tempo');
        $data['lokasi']         = $this->lokasi->get_active();
        $data['status']         = array(1 => 'Menunggu', 2 => 'Disetujui', 3 => 'Ditolak');
        if(!empty($this->input->get())){
            $data['barangkeluar']  = $this->barangkeluar->get_data_barangkeluar();
        }else{
            $data['barangkeluar']  = '';
        }
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Barang Keluar' => 'active'));
        set_session('title', 'Barang Keluar');
        set_activemenu('', 'menu-barang-keluar');
        $this->render('barangkeluar/v-barangkeluar', $data);
    }

    public function detail($id)
    {
        if(!empty($id)){
            $data['barangkeluar']  = $this->barangkeluar->get_data_barangkeluar($id)[0];
            $data['satuan']     = $this->satuan->get_active();
            $data['barang']     = $this->barang->get_active();
            $data['detail']     = $this->barangkeluar_waiting->get_data($id);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Barang Keluar' => base_url('barangkeluar'), 'Detail' => 'active'));
            set_session('title', 'barangkeluar Global | Detail');
            set_activemenu('', 'menu-barangkeluar');
            $this->render('barangkeluar/v-barangkeluar-detail', $data);
        }
    }

    public function submit_form()
    {
        $post = $this->input->post();
        if(!empty($post['url'])){
            $url = $post['url'];
        }else{
            $url = "lokasi=all&periode=all&status=all";
        }
        unset($post['url']);
        if(!$post['id']){
            # Insert Statement
            $post['is_acc'] = 1;
            $post['input_by'] = get_session('auth')['user_id'];
            $post['timestamp'] = setNewDateTime();
			$result = $this->barangkeluar->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
            }
            $this->go('barangkeluar/detail/'.$this->db->insert_id());
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->barangkeluar->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
            $this->go('barangkeluar?' . $url);
		}

    }

    public function submit_detail()
    {
        $post = $this->input->post();
        $post['input_by'] = get_session('auth')['user_id'];
        $post['timestamp'] = setNewDateTime();
        # Insert Statement
        $result = $this->barangkeluar_waiting->insert($post);
        if($result){
            $this->message('Sukses menambahkan data', 'success');
        }else{
            $this->message('Gagal', 'error');
        }
        $this->go('barangkeluar/detail/'.$post['id_outstock']);

    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $response = $this->barangkeluar->as_array()->get($id);
        echo json_encode($response);
    }

    public function json_get_detail_transaksi()
    {
        $id = $this->input->post('id');
        $response = $this->barangkeluar_detail->get_data_waiting($id);
        echo json_encode($response);
    }

    public function json_get_available_stok(){
        $id = $this->input->post('id');
        $waiting = $this->barangkeluar_waiting->get_stok_waiting($id)[0]['stok'];
        $sisa = $this->stok->get_stok($id)[0]['stok'];
        $result = $sisa - $waiting;
        echo json_encode($result);
    }

    public function json_get_list_stok()
    {
        $barang = $this->input->post('barang');
        $data['barangkeluar'] = $this->barangkeluar_detail->get_barangkeluar();
        $data['stok'] = $this->stok->get_stok($barang);
        $stok = array();
        foreach($data['stok'] as $key => $row){
            $sisa = $row['stok'];
            if(!empty($data['barangkeluar'])){
                foreach($data['barangkeluar'] as $row2){
                    if($row['id'] == $row2['id_stock']){
                        $sisa = $sisa- $row2['kuantitas'];
                    }
                }
            }
            $stok[$key]['id_barang'] = $row['id_barang'];
            $stok[$key]['nama_item'] = $row['barang'];
            $stok[$key]['stok_awal'] = $row['stok_awal'];
            $stok[$key]['stok_sisa'] = $sisa;
            $stok[$key]['satuan'] = $row['satuan'];
        }
        // dump($data);
        $response = array();
        foreach($stok as $row){
            if($row['stok_sisa'] > 0){
                array_push($response, $row);
            }
        }
        echo json_encode($response);
    }

    public function delete_data()
    {
        $id = $this->input->post('id');
        $result = $this->barangkeluar->delete($id);
        if ($result) {
            $this->message('Berhasil menghapus data', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }

    public function delete_detail()
    {
        $id = $this->input->post('id');
        $result = $this->barangkeluar_waiting->delete($id);
        if ($result) {
            $this->message('Berhasil menghapus data', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }

    public function generate_invoice($tgl_beli)
    {
        $last = $this->barangkeluar->get_last_transaction($tgl_beli);
        $purchase_no = '';
        if ($last != 0) {
            $temp = explode('-', $last);
            $current = intval(end($temp));
            $purchase_no = zerofy($id_periode, 3) . '-' . date('Ymd', strtotime($tgl_beli)) . '-' . zerofy(($current + 1), 3);
        } else {
            $purchase_no = zerofy($id_periode, 3) . '-' . date('Ymd', strtotime($tgl_beli)) . '-' . zerofy(1, 3);
        }
        return $purchase_no;
    }
}
