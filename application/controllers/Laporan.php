<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian_model', 'pembelian');
        $this->load->model('pendapatan_model', 'pendapatan');
        $this->load->model('pembayaran_model', 'pembayaran');
        $this->load->model('barangkeluar_model', 'barangkeluar');
        $this->load->model('beban_model', 'beban');
        $this->load->model('lokasi_model', 'lokasi');
        $this->load->model('periode_model', 'periode');
    }

    public function index()
    {
        $data['lokasi'] = $this->lokasi->get_data_with_transaction();
        $data['pembayaran'] = $this->pembayaran->get_rekap_pembayaran();
        $data['pembelian'] = $this->pembelian->get_rekap_pembelian();
        $data['pemakaian'] = $this->barangkeluar->get_rekap_barangkeluar();
        $data['pendapatan'] = $this->pendapatan->get_rekap_pendapatan();
        $data['beban'] = $this->beban->get_rekap_beban();
        // dump($data);
        $data['laporan'] = array();
        $data['global'] = array();
        $akun = array("pembayaran", "pemakaian", "beban", "pendapatan");
        $index = 0;
        if($data['lokasi'] != false){
            foreach ($data['lokasi'] as $lokasi) {
                foreach ($akun as $list) {
                    if (!empty($data[$list])) {
                        foreach ($data[$list] as $row) {
                            if ($row['is_global'] == 1) {
                                $data['global'][$list] = $row['total'];
                            } else {
                                if ($lokasi['id_periode'] == $row['id_periode']) {
                                    $data['laporan'][$index]['id_lokasi'] = $lokasi['id_lokasi'];
                                    $data['laporan'][$index]['lokasi'] = $lokasi['lokasi'];
                                    $data['laporan'][$index]['id_periode'] = $row['id_periode'];
                                    $data['laporan'][$index]['periode'] = $row['periode'];
                                    $data['laporan'][$index][$list] = $row['total'];
                                }
                            }
                        }
                    }
                    if (!isset($data['laporan'][$index][$list]) && $lokasi['is_global'] == 0) {
                        $data['laporan'][$index][$list] = 0;
                    }
    
                    if (!isset($data['global'][$list])) {
                        $data['global'][$list] = 0;
                    }
    
                }
                if ($lokasi['is_global'] == 0) {
                    $index++;
                }
            }
        }

        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Laporan Keuangan' => 'active'));
        set_session('title', 'Laporan Keuangan');
        set_activemenu('', 'menu-laporan');
        $this->render('laporan/v-laporan', $data);
    }

    public function detail($id)
    {
        $data['periode'] = $this->periode->as_array()->get($id);
        $data['lokasi'] = $this->lokasi->as_array()->get($data['periode']['id_lokasi']);
        $data['pembayaran'] = $this->pembayaran->get_laporan_pembayaran_detail($id);
        $data['pemakaian'] = $this->barangkeluar->get_laporan_pemakaian_detail($id);
        $data['beban'] = $this->beban->get_laporan_beban_detail($id);
        $data['pendapatan'] = $this->pendapatan->get_laporan_pendapatan_detail($id);
        $data['total_pembayaran'] = $this->pembayaran->get_total_pembayaran($id)[0]['total'];
        $data['total_pemakaian'] = $this->barangkeluar->get_total_pemakaian($id)[0]['total'];
        $data['total_beban'] = $this->beban->get_total_beban($id)[0]['total'];
        $data['total_pendapatan'] = $this->pendapatan->get_total_pendapatan_acc($id)[0]['total'];
        // dump($data);
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Laporan Keuangan' => base_url('laporan?'.$_SERVER['QUERY_STRING']), 'Detail' => 'active'));
        set_session('title', 'Laporan Keuangan');
        set_activemenu('', 'menu-laporan');
        $this->render('laporan/v-laporan-detail', $data);
    }

}
