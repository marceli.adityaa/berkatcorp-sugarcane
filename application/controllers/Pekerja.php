<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pekerja extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pekerja_model', 'pekerja');
	}

	public function index()
	{
		$data['pekerja'] = $this->pekerja->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master pekerja' => 'active'));
        set_session('title', 'Pekerja');
        set_activemenu('sub-master', 'menu-pekerja');
		$this->render('pekerja/v-pekerja', $data);
	}

	public function submit_form(){
        $post = $this->input->post();
        $post['bayaran'] = intval(preg_replace('/[^\d.]/', '', $post['bayaran']));
		if(!$post['id']){
            # Insert Statement
			$result = $this->pekerja->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->pekerja->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('pekerja');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->pekerja->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->pekerja->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->pekerja->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
