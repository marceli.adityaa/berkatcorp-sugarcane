<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Otorisasi extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian_model', 'pembelian');
        $this->load->model('pembelian_detail_model', 'pembelian_detail');
        $this->load->model('pembelian_tempo_model', 'pembelian_tempo');
        $this->load->model('pendapatan_model', 'pendapatan');
        $this->load->model('beban_model', 'beban');
        $this->load->model('barangkeluar_model', 'barangkeluar');
        $this->load->model('barangkeluar_waiting_model', 'barangkeluar_waiting');
        $this->load->model('barangkeluar_detail_model', 'barangkeluar_detail');
        $this->load->model('stok_model', 'stok');
        $this->load->model('lokasi_model', 'lokasi');
        $this->load->model('periode_model', 'periode');
        $this->load->model('supplier_model', 'supplier');
        $this->load->model('bank_model', 'bank');
    }

    public function index()
    {

    }

    public function pembelian()
    {
        $data['lokasi'] = $this->lokasi->where('status', 1)->order_by(array('nama' => 'asc'))->as_array()->get_all();
        $data['supplier'] = $this->supplier->where('status', 1)->order_by(array('nama' => 'asc'))->as_array()->get_all();
        $data['payment_type'] = array('cash', 'tempo');
        $data['status'] = array(1 => 'Menunggu', 2 => 'Disetujui', 3 => 'Ditolak');
        $data['group_bank'] = $this->bank->get_active_group_bank();
        $data['bank'] = $this->bank->get_active_bank();
        if (empty($this->input->get())) {
            $_GET['status'] = 1;
        }
        $data['pembelian'] = $this->pembelian->get_data_pembelian();
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Otorisasi Pembelian' => 'active'));
        set_session('title', 'Otorisasi Pembelian');
        set_activemenu('', 'menu-otorisasi-pembelian');
        $this->render('pembelian/v-pembelian-otorisasi', $data);
    }

    public function barangkeluar()
    {
        $data['lokasi'] = $this->lokasi->where('status', 1)->order_by(array('nama' => 'asc'))->as_array()->get_all();
        $data['status'] = array(1 => 'Menunggu', 2 => 'Disetujui', 3 => 'Ditolak');
        if (empty($this->input->get())) {
            $_GET['status'] = 1;
        }
        $data['barangkeluar'] = $this->barangkeluar->get_data_barangkeluar();
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Otorisasi Barang Keluar' => 'active'));
        set_session('title', 'Otorisasi Barang Keluar');
        set_activemenu('', 'menu-otorisasi-barangkeluar');
        $this->render('barangkeluar/v-barangkeluar-otorisasi', $data);
    }

    public function pendapatan()
    {
        $data['lokasi'] = $this->lokasi->where('status', 1)->order_by(array('nama' => 'asc'))->as_array()->get_all();
        $data['status'] = array(1 => 'Menunggu', 2 => 'Disetujui', 3 => 'Ditolak');
        if (empty($this->input->get())) {
            $_GET['status'] = 1;
        } 
        $data['pendapatan'] = $this->pendapatan->get_data_pendapatan();
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Otorisasi Pendapatan' => 'active'));
        set_session('title', 'Otorisasi Pendapatan');
        set_activemenu('', 'menu-otorisasi-pendapatan');
        $this->render('pendapatan/v-pendapatan-otorisasi', $data);
    }

    public function beban()
    {
        $data['status'] = array(1 => 'Menunggu', 2 => 'Disetujui', 3 => 'Ditolak');
        $data['lokasi'] = $this->lokasi->get_active();
        if (empty($this->input->get())) {
            $_GET['status'] = 1;
        }
        $data['beban'] = $this->beban->get_data();
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Otorisasi Beban' => 'active'));
        set_session('title', 'Otorisasi Beban');
        set_activemenu('', 'menu-otorisasi-beban');
        $this->render('beban/v-beban-otorisasi', $data);
    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $response = $this->pembelian->as_array()->get($id);
        echo json_encode($response);
    }

    public function approve_pembelian()
    {
        $id = $this->input->post('id');
        $update = array(
            'is_paid' => 2,
            'is_paid_remark' => $this->input->post('remark'),
            'is_paid_by' => get_session('auth')['user_id'],
        );
        $result = $this->pembelian->update($id, $update);
        $pembelian = $this->pembelian->as_array()->get($id);
        $detail = $this->pembelian_detail->get_data($id);
        $stok = array();
        $index = 0;
        foreach ($detail as $row) {
            $stok[$index] = array(
                'id_purchase_detail' => $row['id'],
                'id_barang' => $row['id_barang'],
                'tgl_masuk' => $pembelian['tgl_beli'],
                'stok_awal' => $row['kuantitas'],
                'stok_sisa' => $row['kuantitas'],
                'satuan' => $row['satuan'],
                'harga' => $row['harga'],
                'input_by' => $pembelian['input_by'],
                'timestamp' => setNewDateTime(),
            );
            $index++;
        }
        $result2 = $this->db->insert_batch('stock', $stok);
        if ($result2) {
            $this->message('Berhasil menyetujui transaksi, stok barang bertambah.', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }

    public function decline_pembelian()
    {
        $id = $this->input->post('id');
        $update = array(
            'is_paid' => 3,
        );
        $result = $this->pembelian->update($id, $update);
        if ($result) {
            $this->message('Berhasil menolak transaksi', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }

    public function approve_barangkeluar()
    {
        $id = $this->input->post('id');
        $detail = $this->barangkeluar_waiting->get_data($id);
        // dump($detail);
        foreach ($detail as $row) {
            $qty = $row['kuantitas'];
            $stok = $this->stok->get_data_barang($row['id_barang']);
            foreach ($stok as $row2) {
                if ($qty > 0) {
                    $usage = 0;
                    if ($row2['stok_sisa'] > $qty) {
                        # Jika stok 1 record terpenuhi
                        $this->stok->update($row2['id'], array('stok_sisa' => ($row2['stok_sisa'] - $qty)));
                        $usage = $qty;
                        $qty = 0;
                    } else {
                        # Jika butuh stok > 1 record
                        $this->stok->update($row2['id'], array('stok_sisa' => 0));
                        $qty -= $row2['stok_sisa'];
                        $usage = $row2['stok_sisa'];
                    }

                    $transaksi = array(
                        'id_outstock' => $row['id_outstock'],
                        'id_stock' => $row2['id'],
                        'kuantitas' => $usage,
                        'satuan' => $row2['satuan'],
                        'harga' => $row2['harga'],
                        'subtotal' => $row2['harga'] * $usage,
                        'catatan' => $row['catatan'],
                        'input_by' => get_session('auth')['user_id'],
                        'timestamp' => setNewDateTime(),
                    );
                    $this->barangkeluar_detail->insert($transaksi);
                }

            }
        }

        # Update status transaksi
        $update = array(
            'is_acc' => 2,
            'is_acc_remark' => $this->input->post('remark'),
            'is_acc_by' => get_session('auth')['user_id'],
            'is_acc_timestamp' => setNewDateTime(),
        );

        $result = $this->barangkeluar->update($id, $update);
        if ($result) {
            $this->message('Berhasil menyetujui transaksi, stok barang berubah.', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }

    public function decline_barangkeluar()
    {
        $id = $this->input->post('id');
        $update = array(
            'is_acc' => 3,
        );
        $result = $this->barangkeluar->update($id, $update);
        if ($result) {
            $this->message('Berhasil menolak transaksi', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }

    public function approve_pendapatan()
    {
        $id = $this->input->post('id');
        $update = array(
            'is_paid' => 2,
            'is_paid_remark' => $this->input->post('remark'),
            'is_paid_by' => get_session('auth')['user_id'],
        );
        $result = $this->pendapatan->update($id, $update);
        if ($result) {
            $this->message('Berhasil menyetujui transaksi', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }

    public function decline_pendapatan()
    {
        $id = $this->input->post('id');
        $update = array(
            'is_paid' => 3,
        );
        $result = $this->pendapatan->update($id, $update);
        if ($result) {
            $this->message('Berhasil menolak transaksi', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }

    public function approve_beban()
    {
        $id = $this->input->post('id');
        $update = array(
            'is_paid' => 2,
            'is_paid_remark' => $this->input->post('remark'),
            'is_paid_by' => get_session('auth')['user_id'],
        );
        $result = $this->beban->update($id, $update);
        if ($result) {
            $this->message('Berhasil menyetujui transaksi', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }

    public function decline_beban()
    {
        $id = $this->input->post('id');
        $update = array(
            'is_paid' => 3,
        );
        $result = $this->beban->update($id, $update);
        if ($result) {
            $this->message('Berhasil menolak transaksi', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }
}
