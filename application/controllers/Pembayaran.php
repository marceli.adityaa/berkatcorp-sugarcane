<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembayaran_model', 'pembayaran');
        $this->load->model('lokasi_model', 'lokasi');
        $this->load->model('periode_model', 'periode');
        $this->load->model('pekerja_model', 'pekerja');
    }

    public function index()
    {
        $data['lokasi'] = $this->lokasi->where('status', 1)->order_by(array('nama' => 'asc'))->as_array()->get_all();
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembayaran Pekerja' => 'active'));
        set_session('title', 'Pembayaran Pekerja');
        set_activemenu('', 'menu-pembayaran');
        $this->render('pembayaran/v-pembayaran-lokasi', $data);
    }

    public function lokasi($id)
    {
        if (isset($id)) {
            $data['lokasi'] = $this->lokasi->as_array()->get($id);
            $data['periode'] = $this->periode->get_periode_lokasi($id);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembayaran Pekerja' => base_url('pembayaran'), $data['lokasi']['nama'] => 'active'));
            set_session('title', 'Pembayaran Pekerja');
            set_activemenu('', 'menu-pembayaran');
            $this->render('pembayaran/v-pembayaran-periode', $data);
        }
    }

    public function periode($id)
    {
        if (isset($id)) {
            $data['periode'] = $this->periode->as_array()->get($id);
            $data['lokasi'] = $this->lokasi->as_array()->get($data['periode']['id_lokasi']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembayaran Pekerja' => base_url('pembayaran'), $data['lokasi']['nama'] => base_url('pembayaran/lokasi/' . $data['lokasi']['id']), $data['periode']['nama'] => 'active'));
            $data['pekerja'] = $this->pekerja->where('status', 1)->order_by('nama')->as_array()->get_all();
            $data['pembayaran'] = $this->pembayaran->get_ordered_date($id);
            set_session('title', 'Pembayaran Pekerja');
            set_activemenu('', 'menu-pembayaran');
            $this->render('pembayaran/v-pembayaran', $data);
        }
    }

    public function detail($periode, $time)
    {
        $date = date('Y-m-d', $time);
        $data['periode'] = $this->periode->as_array()->get($periode);
        $data['lokasi'] = $this->lokasi->as_array()->get($data['periode']['id_lokasi']);
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembayaran Pekerja' => base_url('pembayaran'), $data['lokasi']['nama'] => base_url('pembayaran/lokasi/' . $data['lokasi']['id']), $data['periode']['nama'] => base_url('pembayaran/periode/'.$periode), $date => 'active'));
        $data['detail'] = $this->pembayaran->get_detail_payment($periode, $date);
        $data['total'] = $this->pembayaran->get_total_payment($periode, $date);
        $data['date'] = $date;
        set_session('title', 'Pembayaran Pekerja');
        set_activemenu('', 'menu-pembayaran');
        $this->render('pembayaran/v-pembayaran-detail', $data);
    }

    public function submit_form()
    {
        $post = $this->input->post();

        foreach ($post['id_pekerja'] as $key => $val) {
            $insert = array(
                'id_periode' => $post['id_periode'],
                'tgl_bayar' => $post['tgl_bayar'],
                'id_pekerja' => $val,
                'bayaran' => intval(preg_replace('/[^\d.]/', '', $post['bayaran'][$val])),
                'input_by' => get_session('auth')['user_id'],
                'timestamp' => setNewDateTime(),
            );
            $result = $this->pembayaran->insert($insert);
        }

        if ($result) {
            $this->message('Sukses memasukkan data', 'success');
        } else {
            $this->message('Gagal', 'error');
        }

        $this->go('pembayaran/periode/' . $post['id_periode']);
    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $response = $this->pembayaran->as_array()->get($id);
        echo json_encode($response);
    }

    public function delete_data()
    {
        $id = $this->input->post('id');
        $result = $this->pembayaran->delete($id);
        if ($result) {
            $this->message('Berhasil menghapus data', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }
}
