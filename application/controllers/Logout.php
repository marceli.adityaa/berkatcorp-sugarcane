<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Logout extends MY_Base_controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function _remap($method, $param = array())
    {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $param);
        } else {
            display_404();
        }
	}
	
    public function index()
    {
        // if(!empty(get_session('user'))){
        //     unset($this->session->userdata('user'));
        // }
        flashdata('info', 'Logout success! Have a good day.. :)');
        unset($_SESSION['auth']);
        redirect(base_url());
    }
}
