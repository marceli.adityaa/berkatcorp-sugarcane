<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Periode extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('periode_model', 'periode');
        $this->load->model('lokasi_model', 'lokasi');
    }

    public function index()
    {
        $data['lokasi'] = $this->lokasi->where('status', 1)->order_by(array('nama' => 'asc'))->as_array()->get_all();
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Periode' => 'active'));
        set_session('title', 'Periode');
        set_activemenu('', 'menu-periode');
        $this->render('periode/v-periode-lokasi', $data);
    }

    public function lokasi($id)
    {
        if (isset($id)) {
            $data['lokasi'] = $this->lokasi->as_array()->get($id);
            $data['periode'] = $this->periode->get_periode_lokasi($id);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Periode' => base_url('periode'), $data['lokasi']['nama'] => 'active'));
            set_session('title', 'Periode');
            set_activemenu('', 'menu-periode');
            $this->render('periode/v-periode', $data);
        }
    }

    public function submit_form()
    {
        $post = $this->input->post();
        if (!isset($post['id'])) {
            # Insert Statement
            $result = $this->periode->insert($post);
            if ($result) {
                $this->message('Sukses memasukkan data', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
        } else {
            # Update Statement
            $id = $post['id'];
            unset($post['id']);
            $result = $this->periode->update($id, $post);
            if ($result) {
                $this->message('Sukses mengubah data', 'success');
            } else {
                $this->message('Gagal', 'error');
            }
        }
        $this->go('periode/lokasi/'.$post['id_lokasi']);
    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $response = $this->periode->as_array()->get($id);
        echo json_encode($response);
    }

    public function set_aktif()
    {
        $id = $this->input->post('id');
        $response = $this->periode->update($id, array('selesai' => 1));
        echo json_encode($response);
    }

    public function set_nonaktif()
    {
        $id = $this->input->post('id');
        $response = $this->periode->update($id, array('selesai' => 0));
        echo json_encode($response);
    }

    public function get_periode_by_lokasi(){
        $id = $this->input->post('id');
        $response = $this->periode->where(array('id_lokasi' => $id, 'selesai' => 0))->order_by('tgl_mulai', 'desc')->as_array()->get_all();
        echo json_encode($response);
    }
}
