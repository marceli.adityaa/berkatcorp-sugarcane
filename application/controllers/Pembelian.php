<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian_model', 'pembelian');
        $this->load->model('pembelian_detail_model', 'pembelian_detail');
        $this->load->model('lokasi_model', 'lokasi');
        $this->load->model('periode_model', 'periode');
        $this->load->model('supplier_model', 'supplier');
        $this->load->model('bank_model', 'bank');
        $this->load->model('satuan_model', 'satuan');
        $this->load->model('barang_model', 'barang');
    }

    public function index()
    {
        $data['supplier']       = $this->supplier->where('status', 1)->order_by(array('nama' => 'asc'))->as_array()->get_all();
        $data['payment_type']   = array('cash', 'tempo');
        $data['status']         = array(1 => 'Menunggu', 2 => 'Disetujui', 3 => 'Ditolak');
        $data['group_bank']     = $this->bank->get_active_group_bank();
        $data['bank']           = $this->bank->get_active_bank();
        if(!empty($this->input->get())){
            $data['pembelian']  = $this->pembelian->get_data_pembelian();
        }else{
            $data['pembelian']  = '';
        }
        set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Global' => 'active'));
        set_session('title', 'Pembelian Global');
        set_activemenu('', 'menu-pembelian');
        $this->render('pembelian/v-pembelian', $data);
    }

    public function detail($id)
    {
        if(!empty($id)){
            $data['pembelian']  = $this->pembelian->as_array()->get($id);
            $data['supplier']   = $this->supplier->as_array()->get($data['pembelian']['id_supplier']);
            $data['satuan']     = $this->satuan->get_active();
            $data['barang']     = $this->barang->get_active();
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Pembelian Global' => base_url('pembelian'), 'Kode : '.$data['pembelian']['invoice'] => 'active'));
            $data['total']      = $this->pembelian_detail->get_total_purchase($id);
            $data['detail']     = $this->pembelian_detail->get_data($id);
            set_session('title', 'Pembelian Global | Detail');
            set_activemenu('', 'menu-pembelian');
            $this->render('pembelian/v-pembelian-detail', $data);
        }
    }

    public function submit_form()
    {
        $post = $this->input->post();
        if(!empty($post['url'])){
            $url = $post['url'];
        }else{
            $url = "supplier=all&type=all&status=all";
        }
        unset($post['url']);
        if(!$post['id']){
            # Insert Statement
            $post['invoice'] = $this->generate_invoice($post['tgl_beli']);
            $post['input_by'] = get_session('auth')['user_id'];
            $post['timestamp'] = setNewDateTime();
			$result = $this->pembelian->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
            }
            $this->go('pembelian/detail/'.$this->db->insert_id());
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->pembelian->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
            $this->go('pembelian?' . $url);
		}

    }

    public function submit_detail()
    {
        $post = $this->input->post();
        $post['kuantitas'] = intval(preg_replace('/[^\d.]/', '', $post['kuantitas']));
        $post['harga'] = intval(preg_replace('/[^\d.]/', '', $post['harga']));
        $post['subtotal'] = intval(preg_replace('/[^\d.]/', '', $post['subtotal']));
        $post['input_by'] = get_session('auth')['user_id'];
        $post['timestamp'] = setNewDateTime();
        # Insert Statement
        $result = $this->pembelian_detail->insert($post);
        if($result){
            $this->message('Sukses memasukkan data', 'success');
        }else{
            $this->message('Gagal', 'error');
        }
        $this->go('pembelian/detail/'.$post['id_pembelian']);

    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        $response = $this->pembelian->as_array()->get($id);
        echo json_encode($response);
    }

    public function json_get_detail_transaksi()
    {
        $id = $this->input->post('id');
        $response = $this->pembelian_detail->get_data($id);
        echo json_encode($response);
    }

    public function delete_data()
    {
        $id = $this->input->post('id');
        $result = $this->pembelian->delete($id);
        if ($result) {
            $this->message('Berhasil menghapus data', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }

    public function delete_detail()
    {
        $id = $this->input->post('id');
        $result = $this->pembelian_detail->delete($id);
        if ($result) {
            $this->message('Berhasil menghapus data', 'success');
        } else {
            $this->message('Gagal', 'error');
        }
        echo json_encode($result);
    }

    public function generate_invoice($tgl_beli)
    {
        $last = $this->pembelian->get_last_transaction($tgl_beli);
        $purchase_no = '';
        if ($last != 0) {
            $temp = explode('-', $last);
            $current = intval(end($temp));
            $purchase_no = date('Ymd', strtotime($tgl_beli)) . '-' . zerofy(($current + 1), 3);
        } else {
            $purchase_no = date('Ymd', strtotime($tgl_beli)) . '-' . zerofy(1, 3);
        }
        return $purchase_no;
    }
}
