<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Supplier extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('supplier_model', 'supplier');
	}

	public function index()
	{
		$data['supplier'] = $this->supplier->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Supplier' => 'active'));
        set_session('title', 'supplier');
        set_activemenu('sub-master', 'menu-supplier');
		$this->render('supplier/v-supplier', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
            # Insert Statement
			$result = $this->supplier->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->supplier->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('supplier');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->supplier->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->supplier->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->supplier->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
