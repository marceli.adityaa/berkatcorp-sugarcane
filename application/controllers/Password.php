<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
    }

	public function index()
	{
        set_session('title', 'Password');
        set_session('breadcrumb', array('Home' => base_url('dashboard'), 'Password' => 'active'));
        set_activemenu('', '');
        $this->render('password/v-password', $data = array());
	}

	public function submit_form(){
		$username       = get_session('auth')['username'];
		$old_password   = md5($this->input->post('old-password'));
		if($this->input->post('new-password') === $this->input->post('retype-new-password')){
			if($this->user->auth($username, $old_password) != false){
				$update['password']	= md5($this->input->post('new-password'));
				$result = $this->user->update(get_session('auth')['id'], $update);
				if($result){
					$this->message('Sukses', 'success');
				}else{
					$this->message('Gagal!', 'error');
				}
			}else{
				$this->message('Password lama salah!', 'error');
			}	
		}else{
			$this->message('Password tidak cocok!', 'error');
		}

		$this->go('password');
	}

	

}