<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Satuan extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('satuan_model', 'satuan');
	}

	public function index()
	{
		$data['satuan'] = $this->satuan->order_by(array('status' => 'desc', 'nama' => 'asc'))->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Satuan' => 'active'));
        set_session('title', 'satuan');
        set_activemenu('sub-master', 'menu-satuan');
		$this->render('satuan/v-satuan', $data);
	}

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
            # Insert Statement
			$result = $this->satuan->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->satuan->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('satuan');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->satuan->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->satuan->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->satuan->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
