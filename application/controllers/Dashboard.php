<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('stok_model', 'stok');
	}

	public function index()
	{
		$_GET['status'] = "ada";
		$data['stok'] = $this->stok->get_rekap_stok();
		set_session('breadcrumb', array('Dashboard' => 'active'));
		set_session('title', 'Dashboard');
		set_activemenu('', 'menu-dashboard');
		$this->render('dashboard/v-dashboard', $data);
	}

	public function tes()
	{
		$link = parse_url('https://berkatcorp.com/beras');
		echo $link['host'];
	}
}
