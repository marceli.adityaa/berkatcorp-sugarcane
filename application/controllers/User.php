<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('user_level_model', 'level');
	}

	public function index()
	{
        $data['user'] = $this->user->get();
        $data['level'] = $this->level->where('id <>', 1)->order_by('id', 'asc')->as_array()->get_all();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master User' => 'active'));
        set_session('title', 'user');
        set_activemenu('sub-master', 'menu-user');
		$this->render('user/v-user', $data);
	}

	public function submit_form(){
        $post = $this->input->post();
		if(!$post['id']){
            # Insert Statement
            $post['password'] = md5($post['password']);
			$result = $this->user->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
            unset($post['id']);
            if(!empty($post['password'])){
                $post['password'] = md5($post['password']);
            }else{
                unset($post['password']);
            }
			$result = $this->user->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('user');
	}

	public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->user->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->user->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->user->update($id, array('status' => 0));
		echo json_encode($response);
	}
}
