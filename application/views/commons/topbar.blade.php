<div class="am-header bg-midnightblack">
    <div class="am-header-left">
        <a id="naviconLeft" href="" class="am-navicon d-none d-lg-flex"><i class="icon ion-navicon-round"></i></a>
        <a id="naviconLeftMobile" href="" class="am-navicon d-lg-none"><i class="icon ion-navicon-round"></i></a>
        <a href="#" class="am-logo"><img src="<?=base_url('assets/img/logo/berkat-group-horizontal.png')?>" class="img-fluid wd-40p"></a>
    </div>

    <div class="am-header-right">
        <div class="dropdown dropdown-profile">
            <a href="#" class="nav-link nav-link-profile" data-toggle="dropdown">
                <img src="{{base_url('assets/img/blank_user2.png')}}" class="wd-32 rounded-circle" alt="">
                <span class="logged-name"><span class="hidden-xs-down">{{ucwords(get_session('auth')['username'])}}</span> <i class="fa fa-angle-down mg-l-3"></i></span>
            </a>
            <div class="dropdown-menu wd-200">
                <ul class="list-unstyled user-profile-nav">
                    <li><a href="<?= base_url('password')?>"><i class="icon fa fa-lock"></i> Change Password</a></li>
                    <li><a href="{{site_url('logout')}}"><i class="icon ion-power"></i> Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>