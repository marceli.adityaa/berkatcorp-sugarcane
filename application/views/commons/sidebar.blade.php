<div class="am-sideleft">
    <div class="tab-content">
        <div id="mainMenu" class="tab-pane active">
            <ul class="nav am-sideleft-menu">
                @foreach($menu AS $m)
                <li class="nav-item">
                    <a href="{{!empty($m->tautan)?site_url($m->tautan):'#'}}" class="nav-link {{isset($m->child)?'with-sub ':''}} {{$m->class}}">
                        <i class="{{$m->icon}}"></i><span>{{ucwords($m->label)}}</span>
                        @if(isset($m->badge) && $m->badge > 0)
                        &nbsp;<span class="badge badge-info">{{$m->badge}}</span>
                        @endif
                    </a>
                    @if(isset($m->child))
                    <ul class="nav-sub">
                        @foreach($m->child AS $c)
                        <li class="nav-item"><a href="{{site_url($c->tautan)}}" class="nav-link {{$c->class}}"><i class="{{$c->icon}} mr-2"></i>{{ucwords($c->label)}}</a></li>
                        @endforeach
                    </ul>
                    @endif
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>