<!DOCTYPE html>
<html lang="en">

<head>
    @include('commons/head')
</head>

<body>
    <div class="preloader">
        <div class="loader">
            <img src="<?= base_url('assets/img/spinner.gif')?>" class="img-fluid">
        </div>
    </div>
    <div class="ajaxloader" style="display:none;">
        <div class="loader">
            <img src="<?= base_url('assets/img/spinner2.gif')?>" class="img-fluid"> Loading...
        </div>
    </div>

    @yield('content')
    @include('commons/js')
</body>
</html>
