@layout('commons/layout-login')

@section('content')
<div class="am-signin-wrapper bg-midnightblack">
    <div class="am-signin-box">
        <div class="row no-gutters">
            <div class="col-lg-5 bg-browngold">
                <div>
                    <img src="<?=base_url('assets/img/logo/berkat-group-white.png')?>" class="img-fluid" alt="Berkat Group">
                </div>
            </div>
            <div class="col-lg-7">
                <form id="form-login">
                    <h5 class="tx-gray-800 mg-b-25">Berkat Group - <b class="tx-bold ">Sugarcane</b></h5>

                    <div class="form-group">
                        <label class="form-control-label">Username:</label>
                        <input type="text" name="username" class="form-control form-input" placeholder="Enter your username" autocomplete="off">
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="form-control-label">Password:</label>
                        <input type="password" name="password" class="form-control form-input" placeholder="Enter your password">
                    </div><!-- form-group -->

                    <div class="form-group mg-b-20">
                        <img class="img-fluid" src="<?=base_url('assets/captcha/' . $captcha['filename'])?>"><br>
                        <input type="text" class="form-control form-input mg-t-10" name="captcha" placeholder="Retype the characters from picture" autocomplete="off">
                    </div>

                    <button type="button" class="btn btn-block bg-midnightblack" id="btn-login">Login</button>
                </form>
            </div><!-- col-7 -->
        </div><!-- row -->
        <p class="tx-center tx-white-5 tx-12 mg-t-25">Copyright &copy; 2019. Berkat Group. <br>All Rights Reserved.
        </p>
    </div><!-- signin-box -->
</div><!-- am-signin-wrapper -->
@end


@section('js')
<script>
$('.form-input').on('keypress', function(e) {
    if (e.which === 13) {
        auth_login();
    }
});

// Script pre-loader page
$(window).load(function() {
    $(".preloader").fadeOut("slow");
});

// Script action login
$('#btn-login').on('click', function(e) {
    auth_login();
});

function auth_login() {
    var username = $('[name=username]').val();
    var password = $('[name=password]').val();
    var captcha = $('[name=captcha]').val();
    var verif = "<?= get_session('captcha')['word']?>";
    if (username != '' && password != '' && captcha != '') {
        if (captcha === verif) {
            loading_button('#btn-login');
            $('#btn-login').attr('disabled', true);
            $.ajax({
                url: "<?= base_url('login/auth')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'username': username,
                    'password': password
                },
                success: function(result) {
                    if (result.status == 1) {
                        // Success
                        show_alert('success', result.message);
                        window.location.href = result.redirect;
                    } else {
                        // Failed
                        show_alert('error', result.message);
                        $("#form-login").trigger('reset');
                        $("#btn-login").html('Login');
                        $("[name=username]").focus();
                    }
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {
                    $('#btn-login').attr('disabled', false);
                }
            });
        } else {
            show_alert('error', 'Wrong captcha');
            $('[name=captcha]').val('').focus();
        }
    } else {
        if (username == '') {
            $('[name=username]').focus();
        } else if (password == '') {
            $('[name=password]').focus();
        } else if (captcha == '') {
            $('[name=captcha]').focus();
        }
    }
}

function loading_button(element) {
    var $this = $(element);
    $this.data('original-text', $(element).html());
    $this.html('<i class="fa fa-circle-o-notch fa-spin"></i> loading..');
}

function show_alert(type, message) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'bottom-right',
            showConfirmButton: false,
            timer: 3000
        });

        Toast.fire({
            type: type,
            title: message
        })
    }
</script>
@end