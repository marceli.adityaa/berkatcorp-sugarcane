@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('laporan?')?>">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="start" class="form-control datepicker" autocomplete="off" value="{{(!empty($_GET['start']) ? $_GET['start'] : '')}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Berakhir</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="end" class="form-control datepicker" autocomplete="off" value="{{(!empty($_GET['end']) ? $_GET['end'] : '')}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Laporan Keuangan</h6>
    </div>
    <div class="card-body">
        <div class="col-12">
            <div id="toolbar" class="mg-b-10">
                <h5>Lokasi</h5>
            </div>
            <table class="table table-striped mg-t-10 table-white" id="tabel_lokasi">
                <thead>
                    <tr>
                        <th data-formatter="reformat_number" class="text-center">No.</th>
                        <th data-searchable="false">Aksi</th>
                        <th data-sortable="true">Lokasi</th>
                        <th data-sortable="true">Periode</th>
                        <th data-sortable="true">Pembayaran Pekerja</th>
                        <th data-sortable="true">Pemakaian Stok</th>
                        <th data-sortable="true">Beban</th>
                        <th data-sortable="true">Pendapatan</th>
                        <th data-sortable="true">Margin</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        $total_margin = 0;
                        $margin_global = 0;
                        $margin = 0;  
                    ?>
                    @foreach ($laporan as $row)
                    <tr>
                        <td>{{$no++}}</td>
                        <td><a href="{{base_url('laporan/detail/'.$row['id_periode'].'?'.$_SERVER['QUERY_STRING'])}}"><button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='right' title='Detail Transaksi'><i class='icon-eye'></i></button></a></td>
                        <td>{{$row['lokasi']}}</td>
                        <td>{{$row['periode']}}</td>
                        <td>Rp {{monefy($row['pembayaran'], false)}}</td>
                        <td>Rp {{monefy($row['pemakaian'], false)}}</td>
                        <td>Rp {{monefy($row['beban'], false)}}</td>
                        <td>Rp {{monefy($row['pendapatan'], false)}}</td>
                        <?php 
                                $margin = $row['pendapatan'] - ($row['pembayaran'] + $row['pemakaian'] + $row['beban']);
                                $total_margin += $margin;
                            ?>
                        @if ($margin >= 0)
                        <td><label class="badge badge-success">Rp {{monefy($margin, false)}}</label></td>
                        @else
                        <td><label class="badge badge-danger">Rp {{monefy($margin, false)}}</label></td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if ($total_margin >= 0)
            <h5 class="pull-right tx-normal">Margin Total Lokasi : <b class="badge badge-success">Rp {{monefy($total_margin, false)}}</b></h5>
            @else
            <h5 class="pull-right tx-normal">Margin Total Lokasi : <b class="badge badge-danger">Rp {{monefy($total_margin, false)}}</b></h5>
            @endif
        </div>
        <div class="col-12 mg-t-50 bd-t pd-t-20">
            <h5>Global</h5>
            <?php 
                if(!isset($global['pembayaran'])){
                    $global['pembayaran'] = 0;
                }
                if(!isset($global['pemakaian'])){
                    $global['pemakaian'] = 0;
                }
                if(!isset($global['beban'])){
                    $global['beban'] = 0;
                }
                if(!isset($global['pendapatan'])){
                    $global['pendapatan'] = 0;
                }
            ?>
            <div class="row">
                <label class="col-sm-4 form-control-label">Pembayaran Pekerja</label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <label class="form-control">Rp {{monefy($global['pembayaran'], false)}}</label>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-4 form-control-label">Pemakaian Stok</label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <label class="form-control">Rp {{monefy($global['pemakaian'], false)}}</label>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-4 form-control-label">Beban</label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <label class="form-control">Rp {{monefy($global['beban'], false)}}</label>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-4 form-control-label">Pendapatan</label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <label class="form-control">Rp {{monefy($global['pendapatan'], false)}}</label>
                </div>
            </div>
            <?php 
                $margin_global = $global['pendapatan'] - ($global['pembayaran'] + $global['pemakaian'] + $global['beban']);
            ?>
            @if ($margin_global >= 0)
            <h5 class="pull-right mg-t-10 tx-normal">Margin Total Global : <b class="badge badge-success">Rp {{monefy($margin_global, false)}}</b></h5>
            @else
            <h5 class="pull-right mg-t-10 tx-normal">Margin Total Global : <b class="badge badge-danger">Rp {{monefy($margin_global, false)}}</b></h5>
            @endif
        </div>
        <div class="col-12 mg-t-50 bd-t pd-t-10">
            <?php 
                $margin = $total_margin + $margin_global;
            ?>
            @if ($margin >= 0)
            <h4 class="pull-right mg-t-10 tx-bold">LABA : <b class="badge badge-success">Rp {{monefy($margin, false)}}</b></h4>
            @else
            <h4 class="pull-right mg-t-10 tx-bold">RUGI : <b class="badge badge-danger">Rp {{monefy($margin, false)}}</b></h4>
            @endif
        </div>
    </div>
</div>
@end

@section('js')
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.theme.min.css">
<script src="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_lokasi').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar',
    });

    $('#tabel_detail').bootstrapTable({});
    $('[data-toggle="tooltip"]').tooltip();
});

$(".datepicker").datepicker({
    todayHighlight: !0,
    dateFormat: 'yy-mm-dd',
    autoclose: !0,
});

function monefy(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
</script>
@end