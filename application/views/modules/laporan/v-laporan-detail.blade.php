@layout('commons/index')

@section('content')
<div class="card">
    <div class="card-body">
        <div id="accordion" class="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">
                <div class="card-header" role="tab" id="headingOne">
                    <h6 class="mg-b-0">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                            Rekap Data Pembayaran Pekerja
                        </a>
                    </h6>
                </div><!-- card-header -->

                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                    <div class="card-block pd-20">
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Lokasi</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= $lokasi['nama']?>" readonly>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Periode</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= $periode['nama']?>" readonly>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Total Pembayaran </label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= 'Rp '.monefy($total_pembayaran, false)?>" readonly>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped mg-t-10 table-white" id="tabel_bayaran">
                        <thead>
                            <tr>
                                <th data-formatter="reformat_number" class="text-center">No.</th>
                                <th data-sortable="true">Tanggal</th>
                                <th data-sortable="true">Pekerja</th>
                                <th data-sortable="true">Bayaran</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                    $no = 1;
                    if($pembayaran){
                        foreach($pembayaran as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";

                            echo "<td>".$row['tgl_bayar']."</td>";
                            echo "<td>".$row['pekerja']."</td>";
                            echo "<td>Rp ".monefy($row['bayaran'], false)."</td>";
                            echo "</tr>";
                        }
                    }
				?>
                        </tbody>
                    </table>
                </div>
            </div><!-- card -->
        </div><!-- accordion -->

        <div id="accordion2" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
            <div class="card">
                <div class="card-header" role="tab" id="headingTwo">
                    <h6 class="mg-b-0">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                            Rekap Data Pemakaian
                        </a>
                    </h6>
                </div><!-- card-header -->

                <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="card-block pd-20">
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Lokasi</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= $lokasi['nama']?>" readonly>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Periode</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= $periode['nama']?>" readonly>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Total Pembelian </label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= 'Rp '.monefy($total_pemakaian, false)?>" readonly>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped mg-t-10 table-white" id="tabel_bayaran">
                        <thead>
                            <tr>
                                <th data-formatter="reformat_number" class="text-center">No.</th>
                                <th data-sortable="true">Tanggal</th>
                                <th data-sortable="true">Item</th>
                                <th data-sortable="true">Kuantitas</th>
                                <th data-sortable="true">Satuan</th>
                                <th data-sortable="true">Nilai Barang</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                    $no = 1;
                    if($pemakaian){
                        foreach($pemakaian as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td>".$row['tgl_keluar']."</td>";
                            echo "<td>".$row['item']."</td>";
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            echo "<td>".$row['satuan']."</td>";
                            echo "<td>Rp ".monefy($row['total'], false)."</td>";
                            echo "</tr>";
                        }
                    }
				?>
                        </tbody>
                    </table>
                </div>
            </div><!-- card -->
        </div><!-- accordion -->

        <div id="accordion4" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
            <div class="card">
                <div class="card-header" role="tab" id="headingFour">
                    <h6 class="mg-b-0">
                        <a data-toggle="collapse" data-parent="#accordion4" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour" class="tx-gray-800 transition">
                            Rekap Data Beban
                        </a>
                    </h6>
                </div><!-- card-header -->

                <div id="collapseFour" class="collapse show" role="tabpanel" aria-labelledby="headingFour">
                    <div class="card-block pd-20">
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Lokasi</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= $lokasi['nama']?>" readonly>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Periode</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= $periode['nama']?>" readonly>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Total Pembelian </label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= 'Rp '.monefy($total_beban, false)?>" readonly>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped mg-t-10 table-white" id="tabel_bayaran">
                        <thead>
                            <tr>
                                <th data-formatter="reformat_number" class="text-center">No.</th>
                                <th data-sortable="true">Tanggal</th>
                                <th data-sortable="true">Transaksi</th>
                                <th data-sortable="true">Kuantitas</th>
                                <th data-sortable="true">Harga</th>
                                <th data-sortable="true">Subtotal</th>
                                <th data-sortable="true">Catatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                    $no = 1;
                    if($beban){
                        foreach($beban as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td>".$row['tgl_transaksi']."</td>";
                            echo "<td>".$row['nama_trx']."</td>";
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            echo "<td>Rp ".monefy($row['harga'], false)."</td>";
                            echo "<td>Rp ".monefy($row['subtotal'], false)."</td>";
                            echo "<td>".$row['catatan']."</td>";
                            echo "</tr>";
                        }
                    }
				?>
                        </tbody>
                    </table>
                </div>
            </div><!-- card -->
        </div><!-- accordion -->

        <div id="accordion3" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
            <div class="card">
                <div class="card-header" role="tab" id="headingThree">
                    <h6 class="mg-b-0">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                            Rekap Data Pendapatan
                        </a>
                    </h6>
                </div><!-- card-header -->

                <div id="collapseThree" class="collapse show" role="tabpanel" aria-labelledby="headingThree">
                    <div class="card-block pd-20">
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Lokasi</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= $lokasi['nama']?>" readonly>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Periode</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= $periode['nama']?>" readonly>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Total Pendapatan </label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= 'Rp '.monefy($total_pendapatan, false)?>" readonly>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped mg-t-10 table-white" id="tabel_bayaran">
                        <thead>
                            <tr>
                                <th data-formatter="reformat_number" class="text-center">No.</th>
                                <th data-sortable="true">Tanggal</th>
                                <th data-sortable="true">Transaksi</th>
                                <th data-sortable="true">Kode Faktur</th>
                                <th data-sortable="true">Kuantitas</th>
                                <th data-sortable="true">Harga</th>
                                <th data-sortable="true">Subtotal</th>
                                <th data-sortable="true">Catatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                    $no = 1;
                    if($pendapatan){
                        foreach($pendapatan as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td>".$row['tgl_transaksi']."</td>";
                            echo "<td>".$row['nama']."</td>";
                            echo "<td>".$row['kode_faktur']."</td>";
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            echo "<td>Rp ".monefy($row['harga'], false)."</td>";
                            echo "<td>Rp ".monefy($row['subtotal'], false)."</td>";
                            echo "<td>".$row['catatan']."</td>";
                            echo "</tr>";
                        }
                    }
				?>
                        </tbody>
                    </table>
                </div>
            </div><!-- card -->
        </div><!-- accordion -->
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_bayaran, #tabel_pembelian, #tabel_pendapatan').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
@end