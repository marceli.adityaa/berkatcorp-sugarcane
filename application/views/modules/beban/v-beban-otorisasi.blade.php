@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('otorisasi/beban?')?>">
                <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Lokasi</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="lokasi" class="form-control" id="filter_lokasi">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$lokasi as $row){
                                        $get = $this->input->get();
                                        if(isset($get['lokasi']) && $get['lokasi'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Periode</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="periode" class="form-control" id="filter_periode">
                                <option value="all">All</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="start" class="form-control datepicker" autocomplete="off" value="{{(!empty($_GET['start']) ? $_GET['start'] : '')}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Berakhir</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="end" class="form-control datepicker" autocomplete="off" value="{{(!empty($_GET['end']) ? $_GET['end'] : '')}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $val){
                                        if(!empty($this->input->get('status')) && intval($this->input->get('status')) === intval($key)){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Beban</h6>
    </div>
    <div class="card-body">

        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_transaksi">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Status</th>
                    <th>Lokasi</th>
                    <th data-sortable="true">Tgl Transaksi</th>
                    <th data-sortable="true">Nama Transaksi</th>
                    <th data-sortable="true">Kuantitas</th>
                    <th data-sortable="true">Harga</th>
                    <th data-sortable="true">Subtotal</th>
                    <th data-sortable="true">Catatan</th>
                    <th data-sortable="true">Input By</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $beban_acc = 0;
                    if($beban){
                        foreach($beban as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($row['is_paid'] != 2){
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='right' title='Setujui' data-id='".$row['id']."' onclick='accept(this)'><i class='icon-check'></i></button> ";
                            }
                            if($row['is_paid'] != 3){
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='right' title='Tolak' data-id='".$row['id']."' onclick='reject(this)'><i class='icon-close'></i></button>
                                ";
                            }
                            echo "</td>";
                            echo "<td>";
                            if($row['is_paid'] == 1){
                                # Menunggu
                                echo "<label class='badge badge-secondary'>Menunggu</label>";
                            }else if($row['is_paid'] == 2){
                                # Disetujui
                                $beban_acc += $row['subtotal'];
                                echo "<label class='badge badge-success'>Disetujui</label>";
                                if(!empty($row['is_paid_remark'])){
                                    echo "<br><label class='badge badge-light'>".$row['is_paid_remark']."</label>";
                                }
                            }else{
                                # Ditolak
                                echo "<label class='badge badge-danger'>Ditolak</label>";
                            }
                            echo "</td>";
                            if($row['periode'] == 'global'){
                                echo "<td><label class='badge badge-info'>".$row['lokasi']."</label></td>";
                            }else{
                                echo "<td><label class='badge badge-info'>".$row['lokasi']."</label><br>
                                <label class='badge badge-light'>".$row['periode']."</label></td>";
                            }
                            echo "<td>".$row['tgl_transaksi']."</td>";
                            echo "<td>".$row['nama_trx']."</td>";
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            echo "<td>Rp ".monefy($row['harga'], false)."</td>";
                            echo "<td>Rp ".monefy($row['subtotal'], false)."</td>";
                            echo "<td>".$row['catatan']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['input']."</label><br><label class='badge badge-light'>".$row['timestamp']."</label></td>";
                            echo "</tr>";
                        }

                    }
				?>
            </tbody>
        </table>
        <h4 class="float-right tx-black"><small>Total Beban (disetujui): Rp </small><b>{{monefy($beban_acc, false)}}</b></h4>
        @else
        <p class="text-center">Klik filter untuk menampilkan data beban</p>
        @endif
    </div>
</div>
@end

@section('js')
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.theme.min.css">
<script src="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_transaksi').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar',
    });

    $('#tabel_detail').bootstrapTable({});
    $('[data-toggle="tooltip"]').tooltip();
    init_periode();
});

$(".datepicker").datepicker({
    todayHighlight: !0,
    dateFormat: 'yy-mm-dd',
    autoclose: !0,
});

$('#filter_lokasi').change(function() {
    var id = $(this).val();
    if (id != 'all') {
        $.ajax({
            url: "<?= base_url('periode/get_periode_by_lokasi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#filter_periode').empty().append('<option value="all">All</option>');
                $.each(result, function(key, val) {
                    $('#filter_periode').append('<option value="' + val.id + '">' + val.nama + '</option>');
                });
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    } else {
        $('#filter_periode').empty().append('<option value="all">All</option>');
    }
});

$('#form_lokasi').change(function() {
    init_periode_form();
});

function init_periode() {
    var id = $('#filter_lokasi').val();
    if (id != 'all') {
        $.ajax({
            url: "<?= base_url('periode/get_periode_by_lokasi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                var periode = "{{(isset($get['periode'])) ? $get['periode'] : 'all'}}";
                $('#filter_periode').empty().append('<option value="all">All</option>');
                $.each(result, function(key, val) {
                    $('#filter_periode').append('<option value="' + val.id + '">' + val.nama + '</option>');
                });
                $('#filter_periode').val(periode).change();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    } else {
        $('#filter_periode').empty().append('<option value="all">All</option>');
    }
}

function accept(el) {
    var id = $(el).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Setujui?',
            text: 'Jika tidak ada catatan khusus, field input bisa di kosongi',
            input: 'text',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            inputValidator: (value) => {
                $.ajax({
                    url: "<?= base_url('otorisasi/approve_beban')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                        'remark': value
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}

function reject(el) {
    var id = $(el).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Tolak?',
            text: "Data transaksi akan diubah statusnya, lanjutkan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('otorisasi/decline_beban')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}

function monefy(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
</script>
@end