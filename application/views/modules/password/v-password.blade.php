@layout('commons/index')

@section('content')
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Ubah Password</h6>
    </div>
    <div class="card-body">
        <div class="form-layout">
            <form method="post" action="<?=base_url('password/submit_form')?>">
                <div class="form-layout form-layout-4">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">Old Password <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="password" name="old-password" class="form-control" autocomplete="off" required="">
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">New Password <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="password" name="new-password" class="form-control" autocomplete="off" required="">
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Retype New Password <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="password" name="retype-new-password" class="form-control" autocomplete="off" required="">
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-primary pd-x-20 btn-submit"><i class="fa fa-lock mg-r-10"></i>Change Password</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@end

@section('js')

@end