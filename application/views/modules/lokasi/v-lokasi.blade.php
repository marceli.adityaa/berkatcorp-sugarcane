@layout('commons/index')

@section('content')
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Master Lokasi</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_lokasi">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Nama</th>
                    <th data-sortable="true">Luas (M2)</th>
                    <th data-sortable="true">Kepemilikan</th>
                    <th data-sortable="true">Mulai Berlaku</th>
                    <th data-sortable="true">Berlaku Hingga</th>
                    <th data-sortable="true">Deskripsi</th>
                    <th data-sortable="true" data-searchable="false">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php 
					$no = 1;
					foreach($lokasi as $row){
                        if(empty($luas)){
                            $luas = 0;
                        }
						echo "<tr>";
						echo "<td class='text-center'>".$no++."</td>";
						echo "<td class='text-nowrap'>";
						echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='left' title='Edit' onclick='edit(this)' data-id='".$row['id']."'><i class='icon-pencil'></i></button>";
						if($row['status'] == 1){
							echo " <button type='button' class='btn btn-secondary' data-toggle='tooltip' data-placement='left' title='Set Nonaktif' onclick='nonaktif(this)' data-id='".$row['id']."'><i class='fa fa-times'></i></button>";
						}else{
							echo " <button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='left' title='Set Aktif' onclick='aktif(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button>";
						}
						
						echo "</td>";
                        echo "<td>".$row['nama']."</td>";
                        echo "<td>".monefy($row['luas'], false)."</td>";
                        echo "<td>".ucwords($row['jenis_kepemilikan'])."</td>";
                        echo "<td>".$row['mulai_berlaku']."</td>";
                        echo "<td>".$row['berlaku_hingga']."</td>";
                        echo "<td>".$row['deskripsi']."</td>";
						if($row['status'] == 1){
							echo "<td><label class='badge badge-success'>Aktif</label></td>";
						}else{
							echo "<td><label class='badge badge-secondary'>Nonaktif</label></td>";
						}
						echo "</tr>";
					}
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('lokasi/submit_form')?>">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Formulir Data Lokasi</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Lokasi <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="nama" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Luas (M2) <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control autonumeric" name="luas" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Jenis Kepemilikan <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select class="form-control" name="jenis_kepemilikan" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($kepemilikan as $row)
                                    <option value="{{$row}}">{{ucwords($row)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Mulai Berlaku <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control datepicker" name="mulai_berlaku" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Berlaku Hingga <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control datepicker" name="berlaku_hingga" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Deskripsi <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <textarea name="deskripsi" class="form-control" autocomplete="off"></textarea>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.theme.min.css">
<script src="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_lokasi').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });

    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('form').trigger('reset');
    $("#modal_form").modal('show');
});


$(".datepicker").datepicker({
    todayHighlight: !0,
    dateFormat: 'yy-mm-dd',
    autoclose: !0,
    changeMonth: true,
    changeYear: true
});


function edit(el) {
    var id = $(el).data().id;
    if (id !== '') {
        $.ajax({
            url: "<?= base_url('lokasi/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('form').trigger('reset');
                $('[name=id]').val(result.id);
                $('[name=nama]').val(result.nama);
                $('[name=luas]').val(result.luas);
                $('[name=jenis_kepemilikan]').val(result.jenis_kepemilikan).change();
                $('[name=mulai_berlaku]').val(result.mulai_berlaku);
                $('[name=berlaku_hingga]').val(result.berlaku_hingga);
                $('[name=deskripsi]').val(result.deskripsi);
                $('[name=luas]').autoNumeric('destroy');
                $('[name=luas]').autoNumeric('init', {
                    'mDec': 0
                });
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function aktif(el) {
    var id = $(el).data().id;
    if (id !== '') {
        $.ajax({
            url: "<?= base_url('lokasi/set_aktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function nonaktif(el) {
    var id = $(el).data().id;
    console.log(id);
    if (id !== '') {
        $.ajax({
            url: "<?= base_url('lokasi/set_nonaktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}
</script>
@end