@layout('commons/index')

@section('content')
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Pembayaran | {{$lokasi['nama']}} | {{$periode['nama']}}</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            @if($periode['selesai'] == 0)
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
            @endif
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_periode">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-searchable="false">Lokasi / Periode</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true" class="text-center">Jumlah Pekerja</th>
                    <th data-sortable="true">Total Pembayaran</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if($pembayaran){
                        foreach($pembayaran as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<a href='".base_url('pembayaran/detail/'.$periode['id'].'/'.strtotime($row['tgl_bayar']))."'><button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='right' title='Detail'><i class='icon-eye'></i></button></a>";
                            echo "</td>";
                            echo "<td>".$lokasi['nama']."<br>".$periode['nama']."</td>";
                            echo "<td>".$row['tgl_bayar']."</td>";
                            echo "<td class='text-center'>".monefy($row['total_pekerja'],false)."</td>";
                            echo "<td>Rp ".monefy($row['total_bayar'],false)."</td>";
                            echo "</tr>";
                        }

                    }
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('pembayaran/submit_form')?>">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Pembayaran Pekerja</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id_periode" value="{{$periode['id']}}">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Lokasi <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" autocomplete="off" value="{{$lokasi['nama']}}" readonly="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Periode <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" autocomplete="off" value="{{$periode['nama']}}" readonly="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Tgl Bayar <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control datepicker" name="tgl_bayar" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="table-responsive mg-t-20">
                            <table class="table table-stripped w-100" id="table_worker">
                                <thead>
                                    <tr>
                                        <th class='text-center'>No</th>
                                        <th class='text-center'>Tandai</th>
                                        <th class='text-center'>Pekerja</th>
                                        <th class='text-center'>Bayaran</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                $no = 1;
                                if(!empty($pekerja)){
                                    foreach($pekerja as $row){
                                        echo "<tr>";
                                        echo "<td class='text-center'>".$no++."</td>";
                                        echo "<td class='text-center'><label class='ckbox'><input type='checkbox' name='id_pekerja[]' value='".$row['id']."'><span>Tandai</span></label></td>";
                                        echo "<td nowrap class='text-center'>".$row['nama']."</td>";
                                        echo "<td class='text-center'>
                                        <div class='input-group'>
                                        <span class='input-group-addon'>Rp</span>
                                        <input type='text' name='bayaran[".$row['id']."]' class='form-control autonumeric' value='".monefy($row['bayaran'], false)."'>
                                        </div>
                                        </td>";
                                        echo "</tr>";
                                    }
                                }
                            ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.theme.min.css">
<script src="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_periode').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('form').trigger('reset');
    $("#modal_form").modal('show');
});

$(".datepicker").datepicker({
    todayHighlight: !0,
    dateFormat: 'yy-mm-dd',
    autoclose: !0,
});


function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('periode/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('form').trigger('reset');
                $('[name=id]').val(result.id);
                $('[name=nama]').val(result.nama);
                $('[name=deskripsi]').val(result.deskripsi);
                $('[name=tgl_mulai]').val(result.tgl_mulai);
                $('[name=tgl_berakhir]').val(result.tgl_berakhir);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function aktif(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('periode/set_aktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function nonaktif(el) {
    var id = $(el).data().id;
    console.log(id);
    if (id != '') {
        $.ajax({
            url: "<?= base_url('periode/set_nonaktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}
</script>
@end