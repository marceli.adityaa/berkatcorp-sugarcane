@layout('commons/index')

@section('content')
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Pilih Periode - {{$lokasi['nama']}}</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_periode">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Periode</th>
                    <th data-sortable="true">Deskripsi</th>
                    <th data-sortable="true">Tgl Mulai</th>
                    <th data-sortable="true">Luas (M2)</th>
                    <th data-sortable="true">Total Produksi (KG)</th>
                    <th data-sortable="true">Total Pendapatan</th>
                    <th data-sortable="true" data-searchable="false">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if($periode){
                        foreach($periode as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<a href='".base_url('pendapatan/periode/'.$row['id'])."'><button type='button' class='btn btn-primary' data-toggle='tooltip' data-placement='right' title='Pilih Periode'><i class='icon-control-play'></i></button></a>";
                            
                            echo "</td>";
                            echo "<td>".$row['nama']."</td>";
                            echo "<td>".$row['deskripsi']."</td>";
                            echo "<td>".$row['tgl_mulai']."</td>";
                            echo "<td>".monefy($lokasi['luas'], false)."</td>";
                            echo "<td>".monefy($row['produksi'], false)."</td>";
                            echo "<td>Rp ".monefy($row['total'], false)."</td>";
                            if($row['selesai'] == 1){
                                echo "<td><label class='badge badge-success'>Selesai</label></td>";
                            }else{
                                echo "<td><label class='badge badge-primary'>Aktif</label></td>";
                            }
                            echo "</tr>";
                        }

                    }
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_periode').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('[data-toggle="tooltip"]').tooltip();
});

</script>
@end