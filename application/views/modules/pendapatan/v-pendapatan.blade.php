@layout('commons/index')

@section('content')

<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('pendapatan/periode/'.$periode['id'].'?')?>">
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Lokasi</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" autocomplete="off" value="{{$lokasi['nama']}}" readonly="">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Periode</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" class="form-control" autocomplete="off" value="{{$periode['nama']}}" readonly="">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Total Pendapatan </label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= 'Rp '.monefy($total, false)?>" readonly>
                            </div>
                        </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="start" class="form-control datepicker" autocomplete="off" value="{{(!empty($_GET['start']) ? $_GET['start'] : '')}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Berakhir</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="end" class="form-control datepicker" autocomplete="off" value="{{(!empty($_GET['end']) ? $_GET['end'] : '')}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $val){
                                        if(!empty($this->input->get('status')) && intval($this->input->get('status')) === intval($key)){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>

<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Pendapatan</h6>
    </div>
    <div class="card-body">

        <div id="toolbar" class="mg-b-10">
        @if($periode['selesai'] == 0)
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
        @endif
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_periode">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Tgl Transaksi</th>
                    <th data-sortable="true">Kode Faktur</th>
                    <th data-sortable="true">Nama Transaksi</th>
                    <th data-sortable="true">Kuantitas (KG)</th>
                    <th data-sortable="true">Harga</th>
                    <th data-sortable="true">Subtotal</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Catatan</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if($pendapatan){
                        foreach($pendapatan as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($row['is_paid'] != 2){
                                echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='right' title='Edit' data-id='".$row['id']."' onclick='edit(this)'><i class='icon-pencil'></i></button> ";
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='right' title='Hapus' data-id='".$row['id']."' onclick='hapus(this)'><i class='icon-trash'></i></button>
                                ";
                            }
                            echo "</td>";
                            echo "<td>".$row['tgl_transaksi']."</td>";
                            echo "<td><label class='badge badge-light'>".$row['kode_faktur']."</label></td>";
                            echo "<td>".$row['nama']."</td>";
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            echo "<td>Rp ".monefy($row['harga'], false)."</td>";
                            echo "<td>Rp ".monefy($row['subtotal'], false)."</td>";
                            echo "<td>";
                            if($row['is_paid'] == 1){
                                # Menunggu
                                echo "<label class='badge badge-secondary'>Menunggu</label>";
                            }else if($row['is_paid'] == 2){
                                # Disetujui
                                echo "<label class='badge badge-success'>Disetujui</label>";
                                if(!empty($row['is_paid_remark'])){
                                    echo "<br><label class='badge badge-light
                                    '>".$row['is_paid_remark']."</label>";
                                }
                            }else{
                                # Ditolak
                                echo "<label class='badge badge-danger'>Ditolak</label>";
                            }
                            echo "</td>";
                            echo "<td>".$row['nama']."</td>";
                            echo "</tr>";
                        }

                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pembelian</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('pendapatan/submit_form')?>">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Formulir Data Transaksi</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="id_lokasi" value="{{$lokasi['id']}}">
                    <input type="hidden" name="id_periode" value="{{$periode['id']}}">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Lokasi <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" autocomplete="off" value="{{$lokasi['nama']}}" readonly="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Periode <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" autocomplete="off" value="{{$periode['nama']}}" readonly="">
                            </div>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Tanggal Transaksi <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="tgl_transaksi" class="form-control datepicker" autocomplete="off" required="">
                            </div>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Kode Faktur <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="kode_faktur" class="form-control" autocomplete="off">
                            </div>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Nama Transaksi <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="nama" class="form-control" autocomplete="off" required="">
                            </div>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Kuantitas (KG) <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="kuantitas" class="form-control autonumeric" autocomplete="off" required="">
                            </div>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Harga <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control autonumeric" name="harga" autocomplete="off" required="">
                                </div>
                            </div>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Subtotal <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" name="subtotal" class="form-control autonumeric" autocomplete="off" required="" value="">
                                </div>
                            </div>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Catatan <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="catatan" autocomplete="off" value="">
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

@end

@section('js')
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.theme.min.css">
<script src="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_periode').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar',
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('#modal_form form').trigger('reset');
    $("#modal_form").modal('show');
    init_form();
});

$('[name=kuantitas], [name=harga]').on('keyup', function() {
    sum_subtotal();
});

$('[name=subtotal]').on('keyup', function() {
    sum_harga();
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function sum_subtotal() {
    var subtotal = parseInt($('[name=kuantitas]').autoNumeric('get')) * parseInt($('[name=harga]').autoNumeric('get'));
    if (subtotal >= 0) {
        $('[name=subtotal').val(numberWithCommas(subtotal));
    } else {
        $('[name=subtotal').val(0);
    }
}

function sum_harga() {
    var harga = parseInt($('[name=subtotal]').autoNumeric('get')) / parseInt($('[name=kuantitas]').autoNumeric('get'));
    if (harga >= 0) {
        $('[name=harga').val(numberWithCommas(harga));
    } else {
        $('[name=harga').val(0);
    }
}

$(".datepicker").datepicker({
    todayHighlight: !0,
    dateFormat: 'yy-mm-dd',
    autoclose: !0,
});

function init_form() {
    var lokasi = "{{$lokasi['id']}}";
    var periode = "{{$periode['id']}}";
    var url = "{{$_SERVER['QUERY_STRING']}}";
    $('[name=id_lokasi]').val(lokasi);
    $('[name=id_periode]').val(periode);
    $('[name=url]').val(url);
}

function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('pendapatan/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#modal_form form').trigger('reset');
                init_form();
                $('#modal_form [name=id]').val(result.id);
                $('#modal_form [name=tgl_transaksi]').val(result.tgl_transaksi);
                $('#modal_form [name=kode_faktur]').val(result.kode_faktur);
                $('#modal_form [name=nama]').val(result.nama);
                $('#modal_form [name=kuantitas]').val(result.kuantitas);
                $('#modal_form [name=harga]').val(result.harga);
                $('#modal_form [name=subtotal]').val(result.subtotal);
                $('#modal_form [name=catatan]').val(result.catatan);
                $('[name=kuantitas], [name=harga], [name=subtotal]').autoNumeric('destroy');
                $('[name=kuantitas], [name=harga], [name=subtotal]').autoNumeric('init', {
                    'mDec': 0
                });
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus(el) {
    var id = $(el).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data akan dihapus, lanjutkan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('pendapatan/delete_data')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}
</script>
@end