@layout('commons/index')

@section('content')
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Pilih Lokasi</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">

        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_periode">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Nama</th>
                    <th data-sortable="true">Luas (M2)</th>
                    <th data-sortable="true">Deskripsi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
					$no = 1;
					foreach($lokasi as $row){
						echo "<tr>";
						echo "<td class='text-center'>".$no++."</td>";
						echo "<td class='text-nowrap'>";
						echo "<a href='".base_url('pendapatan/lokasi/'.$row['id'])."'><button type='button' class='btn btn-primary' data-toggle='tooltip' data-placement='right' title='Pilih Lokasi'><i class='icon-control-play'></i></button></a>";
						echo "</td>";
                        echo "<td>".$row['nama']."</td>";
                        echo "<td>".monefy($row['luas'], false)."</td>";
                        echo "<td>".$row['deskripsi']."</td>";
						echo "</tr>";
					}
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_periode').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
@end