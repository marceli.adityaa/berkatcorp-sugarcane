@layout('commons/index')

@section('content')
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Master Hak Akses</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_level">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Level</th>
                    <th data-sortable="true">Akses</th>
                </tr>
            </thead>
            <tbody>
                <?php 
					$no = 1;
					foreach($level as $row){
						echo "<tr>";
						echo "<td class='text-center'>".$no++."</td>";
						echo "<td class='text-nowrap'>";
						echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='left' title='Edit' onclick='edit(this)' data-id='".$row['id']."'><i class='icon-pencil'></i></button>";
						echo "</td>";
                        echo "<td>".$row['nama']."</td>";
                        echo "<td>".$row['akses']."</td>";
						echo "</tr>";
					}
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('level/submit_form')?>">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Formulir Data level</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Level <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="nama" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Akses <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <?php 
                                    $temp = "";
                                    foreach($list_menu as $key => $val){
                                        if(!empty($val['child_id'])){
                                            # jika ada submenu
                                            if($temp == $val['parent_id']){
                                                echo "<label class='ckbox mg-l-25'><input type='checkbox' name='uuid[]' value='".$val['child_id']."'><span>".ucwords($val['child_label'])."</span></label>";
                                            }else{
                                                echo "<label class='ckbox'><input type='checkbox' name='uuid[]' value='".$val['parent_id']."'><span>".ucwords($val['parent_label'])."</span></label>";
                                                echo "<label class='ckbox mg-l-25'><input type='checkbox' name='uuid[]' value='".$val['child_id']."'><span>".ucwords($val['child_label'])."</span></label>";
                                                $temp = $val['parent_id'];
                                            }
                                        }else{
                                            # jika tidak ada submenu
                                            echo "<label class='ckbox'><input type='checkbox' name='uuid[]' value='".$val['parent_id']."'><span>".ucwords($val['parent_label'])."</span></label>";
                                            $temp = $val['parent_id'];
                                        }
                                        
                                    }

                                
                                ?>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_level').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('form').trigger('reset');
    $('[type=checkbox]').attr('checked', false);
    $("#modal_form").modal('show');
});


function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('level/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('[type=checkbox]').attr('checked', false);
                $('form').trigger('reset');
                $('[name=id]').val(result.id);
                $('[name=nama]').val(result.nama);
                if(result.akses != '' && result.akses != '*'){
                    $.each(jQuery.parseJSON(result.akses), function(key, value) {
                        $('[type=checkbox][value='+value+']').attr('checked', true);
                    });
                }
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}
</script>
@end