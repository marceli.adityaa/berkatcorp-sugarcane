@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('otorisasi/pembelian?')?>">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">Supplier</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="supplier" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$supplier as $row){
                                        if(!empty($this->input->get('supplier')) && $this->input->get('supplier') == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Jenis Pembayaran</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="type" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$payment_type as $key => $val){
                                        if(!empty($this->input->get('type')) && $this->input->get('type') == $val){
                                            echo '<option value="'.$val.'" selected>'.ucwords($val).'</option>';
                                        }else{
                                            echo '<option value="'.$val.'">'.ucwords($val).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $val){
                                        if(!empty($this->input->get('status')) && intval($this->input->get('status')) === intval($key)){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Pembelian</h6>
    </div>
    <div class="card-body">

        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_periode">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Kode Pembelian</th>
                    <th data-sortable="true">Tgl beli</th>
                    <th data-sortable="true">Supplier</th>
                    <th data-sortable="true">Pembayaran</th>
                    <th data-sortable="true">Total</th>
                    <th data-sortable="true">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if($pembelian){
                        foreach($pembelian as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='right' title='Detail Transaksi' data-id='".$row['id']."' onclick='detail(this)'><i class='icon-eye'></i></button> ";
                            if($row['is_paid'] != 2){
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='right' title='Setujui' data-id='".$row['id']."' onclick='accept(this)'><i class='icon-check'></i></button> ";
                            }
                            if($row['is_paid'] != 2){
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='right' title='Tolak' data-id='".$row['id']."' onclick='reject(this)'><i class='icon-close'></i></button>
                                ";
                            }
                            echo "</td>";
                            echo "<td><label class='badge badge-light'>".$row['invoice']."</label></td>";
                            echo "<td>".$row['tgl_beli']."</td>";
                            echo "<td>".ucwords($row['supplier'])."</td>";
                            echo "<td>".ucwords($row['jenis_pembayaran']);
                            if($row['is_paid_tempo'] == 1){
                                echo "<br><label class='badge badge-success' onclick='view_pelunasan(this)' data-id='".$row['id']."'>Lunas</label>";
                            }
                            echo "</td>";
                            echo "<td>Rp ".monefy($row['total'], false)."</td>";
                            echo "<td>";
                            if($row['is_paid'] == 1){
                                # Menunggu
                                echo "<label class='badge badge-secondary'>Menunggu</label>";
                            }else if($row['is_paid'] == 2){
                                # Disetujui
                                echo "<label class='badge badge-success'>Disetujui</label>";
                                if(!empty($row['is_paid_remark'])){
                                    echo "<br><label class='badge badge-light'>".$row['is_paid_remark']."</label>";
                                }
                            }else{
                                # Ditolak
                                echo "<label class='badge badge-danger'>Ditolak</label>";
                            }
                            echo "</td></tr>";
                        }

                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pembelian</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_detail" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Detail Pembelian</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped mg-t-10 table-white" id="tabel_detail">
                    <thead>
                        <tr>
                            <th data-formatter="reformat_number" class="text-center">No.</th>
                            <th data-sortable="true">Nama Barang</th>
                            <th data-sortable="true">Kuantitas</th>
                            <th data-sortable="true">Satuan</th>
                            <th data-sortable="true">Harga</th>
                            <th data-sortable="true">Subtotal</th>
                            <th data-sortable="true">Catatan</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <h5 class="pull-right mg-t-10 tx-light">Total : <span id="total" class="tx-bold"></span></h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" id="modal_pelunasan" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Data Pembayaran Tempo</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-layout form-layout-4">
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Tanggal Pelunasan <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="tgl_pelunasan" class="form-control datepicker" id="tgl_pelunasan" autocomplete="off" required="" placeholder="Masukkan tanggal pelunasan" readonly="">
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Rek. Pengirim <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control select2" name="rek_pengirim" required="" width="100%" readonly="">
                                <option value="">- Pilih Salah Satu -</option>
                                <?php 
                                        if(!empty($group_bank)){
                                            foreach($group_bank as $row){
                                                echo "<optgroup label='".$row['bank']."'></optgroup>";
                                                foreach($bank as $row2){
                                                    if($row2['bank'] == $row['bank']){
                                                        echo '<option value="'.$row2['id'].'">'.$row2['nama'].' | '.$row2['no_rek'].'</option>';
                                                    }
                                                }
                                            }
                                        }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Rek. Tujuan <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control select2" name="rek_tujuan" required="" width="100%" readonly="">
                                <option value="">- Pilih Salah Satu -</option>
                                <?php 
                                        if(!empty($group_bank)){
                                            foreach($group_bank as $row){
                                                echo "<optgroup label='".$row['bank']."'></optgroup>";
                                                foreach($bank as $row2){
                                                    if($row2['bank'] == $row['bank']){
                                                        echo '<option value="'.$row2['id'].'">'.$row2['nama'].' | '.$row2['no_rek'].'</option>';
                                                    }
                                                }
                                            }
                                        }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Total <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input type="text" class="form-control autonumeric" name="nominal" autocomplete="off" required="" readonly="">
                            </div>
                        </div>

                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Catatan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <textarea name="catatan" class="form-control" placeholder="Masukkan catatan tambahan" readonly=""></textarea>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_periode').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar',
    });

    $('#tabel_detail').bootstrapTable({});
    $('[data-toggle="tooltip"]').tooltip();
    init_periode();
});

$('#filter_lokasi').change(function() {
    var id = $(this).val();
    if (id != 'all') {
        $.ajax({
            url: "<?= base_url('periode/get_periode_by_lokasi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#filter_periode').empty().append('<option value="all">All</option>');
                $.each(result, function(key, val) {
                    $('#filter_periode').append('<option value="' + val.id + '">' + val.nama + '</option>');
                });
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    } else {
        $('#filter_periode').empty().append('<option value="all">All</option>');
    }
});

$('#form_lokasi').change(function() {
    init_periode_form();
});

function init_periode() {
    var id = $('#filter_lokasi').val();
    if (id != 'all') {
        $.ajax({
            url: "<?= base_url('periode/get_periode_by_lokasi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                var periode = "{{(!empty($_GET['periode'])) ? $_GET['periode'] : 'all'}}";
                $('#filter_periode').empty().append('<option value="all">All</option>');
                $.each(result, function(key, val) {
                    $('#filter_periode').append('<option value="' + val.id + '">' + val.nama + '</option>');
                });
                $('#filter_periode').val(periode).change();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    } else {
        $('#filter_periode').empty().append('<option value="all">All</option>');
    }
}

function detail(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('pembelian/json_get_detail_transaksi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                if (result != false) {
                    $('#tabel_detail > tbody').empty();
                    var no = 1;
                    var total = 0;
                    $.each(result, function(key, val) {
                        total += parseInt(val.subtotal);
                        $("#tabel_detail").find('tbody').append('\
                        <tr><td class="text-center">' + no + '</td>\
                        <td>' + val.barang.toUpperCase() + '</td>\
                        <td>' + monefy(val.kuantitas) + '</td>\
                        <td>' + val.satuan + '</td>\
                        <td>Rp ' + monefy(val.harga) + '</td>\
                        <td>Rp ' + monefy(val.subtotal) + '</td>\
                        <td>' + monefy(val.catatan) + '</td>\
                        </tr>');
                        no++;
                    });
                    $('#total').empty().html('Rp ' + monefy(total));
                    $('#modal_detail').modal('show');
                }else{
                    swal.fire("Info", "Data transaksi kosong", "warning");
                }
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function accept(el) {
    var id = $(el).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Setujui?',
            text: 'Jika tidak ada catatan khusus, field input bisa di kosongi',
            input: 'text',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            inputValidator: (value) => {
                $.ajax({
                    url: "<?= base_url('otorisasi/approve_pembelian')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                        'remark': value
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}

function reject(el) {
    var id = $(el).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Tolak?',
            text: "Data transaksi akan diubah statusnya, lanjutkan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('otorisasi/decline_pembelian')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}

function view_pelunasan(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('pelunasan/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#modal_pelunasan form').trigger('reset');
                $('#modal_pelunasan [name=tgl_pelunasan]').val(result.tgl_pelunasan);
                $('#modal_pelunasan [name=rek_pengirim').val(result.rek_pengirim).change();
                $('#modal_pelunasan [name=rek_tujuan').val(result.rek_tujuan).change();
                $('#modal_pelunasan [name=nominal').val(result.nominal);
                $('#modal_pelunasan [name=catatan').val(result.catatan);
                $('#modal_pelunasan [name=nominal]').autoNumeric('destroy');
                $('#modal_pelunasan [name=nominal]').autoNumeric('init', {
                    'mDec': 0
                });
                $('#modal_pelunasan').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function monefy(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
</script>
@end