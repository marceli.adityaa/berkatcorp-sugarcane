@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('pelunasan?')?>">
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Supplier</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="supplier" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$supplier as $row){
                                        if(!empty($this->input->get('supplier')) && $this->input->get('supplier') == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $val){
                                        if(!empty($this->input->get('status')) && $this->input->get('status') == $val ){
                                            echo '<option value="'.$val.'" selected>'.ucwords($val).'</option>';
                                        }else{
                                            echo '<option value="'.$val.'">'.ucwords($val).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Pembelian</h6>
    </div>
    <div class="card-body">

        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_periode">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Kode Pembelian</th>
                    <th data-sortable="true">Tgl Pelunasan</th>
                    <th data-sortable="true">Supplier</th>
                    <th data-sortable="true">Total</th>
                    <th data-sortable="true">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if($pembelian){
                        foreach($pembelian as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='right' title='Detail Transaksi' data-id='".$row['id']."' onclick='detail(this)'><i class='icon-eye'></i></button> ";
                            if($row['is_paid_tempo'] == 0){
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='right' title='Bayar' data-id='".$row['id']."' data-total='".$row['total']."' onclick='bayar(this)'><i class='icon-pin'></i></button> ";
                            }else{
                                echo "<button type='button' class='btn btn-primary' data-toggle='tooltip' data-placement='right' title='Data Pelunasan' data-id='".$row['id']."' onclick='view_data(this)'><i class='icon-paper-clip'></i></button> ";
                            }
                            echo "</td>";
                            echo "<td><label class='badge badge-light'>".$row['invoice']."</label></td>";
                            echo "<td>".$row['tgl_beli']."</td>";
                            echo "<td>".ucwords($row['supplier'])."</td>";
                            echo "<td>Rp ".monefy($row['total'], false)."</td>";
                            echo "<td>";
                            if($row['is_paid_tempo'] == 1){
                                # Lunas
                                echo "<label class='badge badge-success'>Lunas</label>";
                            }else if($row['is_paid_tempo'] == 0){
                                # Tempo
                                echo "<label class='badge badge-secondary'>Tempo</label>";
                            }
                            echo "</td>";
                            echo "</tr>";
                        }

                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data pembelian tempo</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_detail" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Detail Pembelian</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped mg-t-10 table-white" id="tabel_detail">
                    <thead>
                        <tr>
                            <th data-formatter="reformat_number" class="text-center">No.</th>
                            <th data-sortable="true">Nama Barang</th>
                            <th data-sortable="true">Kuantitas</th>
                            <th data-sortable="true">Satuan</th>
                            <th data-sortable="true">Harga</th>
                            <th data-sortable="true">Subtotal</th>
                            <th data-sortable="true">Catatan</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <h5 class="pull-right mg-t-10 tx-light">Total : <span id="total" class="tx-bold"></span></h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('pelunasan/submit_form')?>">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Data Pembayaran Tempo</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-layout form-layout-4">
                        <input type="hidden" name="id" value="">
                        <input type="hidden" name="id_pembelian" value="">
                        <input type="hidden" name="url" value="<?= $_SERVER['QUERY_STRING']?>">
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Tanggal Pelunasan <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="tgl_pelunasan" class="form-control datepicker" id="tgl_pelunasan" autocomplete="off" required="" placeholder="Masukkan tanggal pelunasan">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Rek. Pengirim <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select class="form-control select2" name="rek_pengirim" required="" width="100%">
                                    <option value="">- Pilih Salah Satu -</option>
                                    <?php 
                                        if(!empty($group_bank)){
                                            foreach($group_bank as $row){
                                                echo "<optgroup label='".$row['bank']."'></optgroup>";
                                                foreach($bank as $row2){
                                                    if($row2['bank'] == $row['bank']){
                                                        echo '<option value="'.$row2['id'].'">'.$row2['nama'].' | '.$row2['no_rek'].'</option>';
                                                    }
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Rek. Tujuan <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select class="form-control select2" name="rek_tujuan" required="" width="100%">
                                    <option value="">- Pilih Salah Satu -</option>
                                    <?php 
                                        if(!empty($group_bank)){
                                            foreach($group_bank as $row){
                                                echo "<optgroup label='".$row['bank']."'></optgroup>";
                                                foreach($bank as $row2){
                                                    if($row2['bank'] == $row['bank']){
                                                        echo '<option value="'.$row2['id'].'">'.$row2['nama'].' | '.$row2['no_rek'].'</option>';
                                                    }
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Total <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control autonumeric" name="nominal" autocomplete="off" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Catatan</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <textarea name="catatan" class="form-control" placeholder="Masukkan catatan tambahan"></textarea>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.theme.min.css">
<script src="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_periode').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar',
    });

    $('#tabel_detail').bootstrapTable({});
    $('[data-toggle="tooltip"]').tooltip();
    $('.select2').select2();
    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
});

$(".datepicker").datepicker({
    todayHighlight: !0,
    dateFormat: 'yy-mm-dd',
    autoclose: !0,
});

function detail(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('pembelian/json_get_detail_transaksi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#tabel_detail > tbody').empty();
                var no = 1;
                var total = 0;
                $.each(result, function(key, val) {
                    total += parseInt(val.subtotal);
                    $("#tabel_detail").find('tbody').append('\
                        <tr><td class="text-center">' + no + '</td>\
                        <td>' + val.barang.toUpperCase() + '</td>\
                        <td>' + monefy(val.kuantitas) + '</td>\
                        <td>' + val.satuan + '</td>\
                        <td>Rp ' + monefy(val.harga) + '</td>\
                        <td>Rp ' + monefy(val.subtotal) + '</td>\
                        <td>' + monefy(val.catatan) + '</td>\
                        </tr>');
                    no++;
                });
                $('#total').empty().html('Rp ' + monefy(total));
                $('#modal_detail').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function bayar(el) {
    var id = $(el).data().id;
    var total = $(el).data().total;
    if (id != '') {
        $('#modal_form form').trigger('reset');
        $('[name=url]').val("<?= $_SERVER['QUERY_STRING']?>");
        $('[name=id_pembelian]').val(id);
        $('[name=rek_pengirim').val('').change();
        $('[name=rek_tujuan').val('').change();
        $('[name=nominal').val(total);
        $('[name=nominal]').autoNumeric('destroy');
        $('[name=nominal]').autoNumeric('init', {
            'mDec': 0
        });
        $('#modal_form').modal('show');
    }
}

function view_data(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('pelunasan/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#modal_form form').trigger('reset');
                $('[name=url]').val("<?= $_SERVER['QUERY_STRING']?>");
                $('[name=id]').val(result.id);
                $('[name=tgl_pelunasan]').val(result.tgl_pelunasan);
                $('[name=id_pembelian]').val(result.id_pembelian);
                $('[name=rek_pengirim').val(result.rek_pengirim).change();
                $('[name=rek_tujuan').val(result.rek_tujuan).change();
                $('[name=nominal').val(result.nominal);
                $('[name=catatan').val(result.catatan);
                $('[name=nominal]').autoNumeric('destroy');
                $('[name=nominal]').autoNumeric('init', {
                    'mDec': 0
                });
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function monefy(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
</script>
@end