@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Stok Tersedia
                </a>
            </h6>
        </div><!-- card-header -->
        <div class="card-body">

        <div id="toolbar" class="mg-b-10">
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_periode">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-sortable="true">Barang</th>
                    <th data-sortable="true">Satuan</th>
                    <th data-sortable="true">Stok Sisa</th>
                    <th data-sortable="true">Nilai Stok Sisa</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $nilai_stok = 0;
                    $nilai_terpakai = 0;
                    if($stok){
                        foreach($stok as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            if($row['total_stok'] == 0){
                                echo "<td>".$row['barang']."<br><label class='badge badge-danger'>Habis</label></td>";
                            }else{
                                echo "<td>".$row['barang']."</td>";
                            }
                            echo "<td>".$row['satuan']."</td>";
                            echo "<td>".monefy($row['total_sisa'], false)."</td>";
                            $nilai_stok += $row['nilai_stok'];
                            echo "<td>Rp ".monefy($row['nilai_stok'], false)."</td>";
                            echo "</tr>";
                        }

                    }
				?>
            </tbody>
        </table>
        <h4 class="float-right tx-black"><small>Total Stok Sisa: Rp </small><b>{{monefy($nilai_stok, false)}}</b></h4>
    </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>


@end

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_periode').bootstrapTable({
        pagination: true,
        toolbar: '#toolbar',
    });
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
@end