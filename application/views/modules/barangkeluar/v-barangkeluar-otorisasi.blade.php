@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('otorisasi/barangkeluar?')?>">
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Lokasi</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="lokasi" class="form-control" id="filter_lokasi">
                                <option value="all">All</option>
                                <?php
                                    $get = $this->input->get();
                                    foreach((array)$lokasi as $row){
                                        if(isset($get['lokasi']) && $get['lokasi'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Periode</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="periode" class="form-control" id="filter_periode">
                                <option value="all">All</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $val){
                                        if(!empty($this->input->get('status')) && intval($this->input->get('status')) === intval($key)){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Barang Keluar</h6>
    </div>
    <div class="card-body">

        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_transaksi">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Tgl Keluar</th>
                    <th data-sortable="true">Lokasi</th>
                    <th data-sortable="true">Periode</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Input By</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if($barangkeluar){
                        foreach($barangkeluar as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='right' title='Detail Transaksi' data-id='".$row['id']."' onclick='detail(this)'><i class='icon-eye'></i></button> ";
                            if($row['is_acc'] != 2){
                                echo "<button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='right' title='Setujui' data-id='".$row['id']."' onclick='accept(this)'><i class='icon-check'></i></button> ";
                            }
                            if($row['is_acc'] != 3){
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='right' title='Tolak' data-id='".$row['id']."' onclick='reject(this)'><i class='icon-close'></i></button>
                                ";
                            }
                            echo "</td>";
                            echo "<td>".$row['tgl_keluar']."</td>";
                            echo "<td>".ucwords($row['lokasi'])."</td>";
                            echo "<td>".ucwords($row['periode'])."</td>";
                            echo "<td>";
                            if($row['is_acc'] == 1){
                                # Menunggu
                                echo "<label class='badge badge-secondary'>Menunggu</label>";
                            }else if($row['is_acc'] == 2){
                                # Disetujui
                                echo "<label class='badge badge-success'>Disetujui</label>";
                                if(!empty($row['is_acc_remark'])){
                                    echo "<br><label class='badge badge-light'>".$row['is_acc_remark']."</label>";
                                }
                            }else{
                                # Ditolak
                                echo "<label class='badge badge-danger'>Ditolak</label>";
                            }
                            echo "</td>";
                            echo "<td><label class='badge badge-light'>".$row['input']."</label><br><label class='badge badge-light'>".$row['timestamp']."</label></td>";
                            echo "</tr>";
                        }

                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data barangkeluar</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_detail" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Detail Barang Keluar</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped mg-t-10 table-white" id="tabel_detail">
                    <thead>
                        <tr>
                            <th data-formatter="reformat_number" class="text-center">No.</th>
                            <th data-sortable="true">Nama Barang</th>
                            <th data-sortable="true">Kuantitas</th>
                            <th data-sortable="true">Satuan</th>
                            <th data-sortable="true">Catatan</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_transaksi').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar',
    });

    $('#tabel_detail').bootstrapTable({});
    $('[data-toggle="tooltip"]').tooltip();
    init_periode();
});

$('#filter_lokasi').change(function() {
    var id = $(this).val();
    if (id != 'all') {
        $.ajax({
            url: "<?= base_url('periode/get_periode_by_lokasi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#filter_periode').empty().append('<option value="all">All</option>');
                $.each(result, function(key, val) {
                    $('#filter_periode').append('<option value="' + val.id + '">' + val.nama + '</option>');
                });
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    } else {
        $('#filter_periode').empty().append('<option value="all">All</option>');
    }
});

$('#form_lokasi').change(function() {
    init_periode_form();
});

function init_periode() {
    var id = $('#filter_lokasi').val();
    if (id != 'all') {
        $.ajax({
            url: "<?= base_url('periode/get_periode_by_lokasi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                var periode = "{{(!empty($_GET['periode'])) ? $_GET['periode'] : 'all'}}";
                $('#filter_periode').empty().append('<option value="all">All</option>');
                $.each(result, function(key, val) {
                    $('#filter_periode').append('<option value="' + val.id + '">' + val.nama + '</option>');
                });
                $('#filter_periode').val(periode).change();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    } else {
        $('#filter_periode').empty().append('<option value="all">All</option>');
    }
}

function detail(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('barangkeluar/json_get_detail_transaksi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#tabel_detail > tbody').empty();
                var no = 1;
                var total = 0;
                $.each(result, function(key, val) {
                    $("#tabel_detail").find('tbody').append('\
                        <tr><td class="text-center">' + no + '</td>\
                        <td>' + val.barang.toUpperCase() + '</td>\
                        <td>' + monefy(val.kuantitas) + '</td>\
                        <td>' + val.satuan + '</td>\
                        <td>' + monefy(val.catatan) + '</td>\
                        </tr>');
                    no++;
                });
                $('#modal_detail').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function accept(el) {
    var id = $(el).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Setujui?',
            text: 'Jika tidak ada catatan khusus, field input bisa di kosongi',
            input: 'text',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            inputValidator: (value) => {
                $.ajax({
                    url: "<?= base_url('otorisasi/approve_barangkeluar')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                        'remark': value
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}

function reject(el) {
    var id = $(el).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Tolak?',
            text: "Data transaksi akan diubah statusnya, lanjutkan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('otorisasi/decline_barangkeluar')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}

function monefy(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
</script>
@end