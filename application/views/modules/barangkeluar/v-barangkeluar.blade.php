@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('barangkeluar?')?>">

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Lokasi</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="lokasi" class="form-control" id="filter_lokasi">
                                <option value="all">All</option>
                                <?php
                                    $get = $this->input->get();
                                    foreach((array)$lokasi as $row){
                                        if(isset($get['lokasi']) && $get['lokasi'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Periode</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="periode" class="form-control" id="filter_periode">
                                <option value="all">All</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $val){
                                        if(!empty($this->input->get('status')) && intval($this->input->get('status')) === intval($key)){
                                            echo '<option value="'.$key.'" selected>'.$val.'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Barang Keluar</h6>
    </div>
    <div class="card-body">

        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_periode">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Tgl Keluar</th>
                    <th data-sortable="true">Lokasi</th>
                    <th data-sortable="true">Periode</th>
                    <th data-sortable="true">Nilai Barang</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Input By</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if($barangkeluar){
                        foreach($barangkeluar as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<a href='".base_url('barangkeluar/detail/'.$row['id'])."'><button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='right' title='Detail Transaksi'><i class='icon-eye'></i></button></a> ";
                            if($row['is_acc'] != 2){
                                echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='right' title='Edit' data-id='".$row['id']."' onclick='edit(this)'><i class='icon-pencil'></i></button> ";
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='right' title='Hapus' data-id='".$row['id']."' onclick='hapus(this)'><i class='icon-trash'></i></button>
                                ";
                            }
                            echo "</td>";
                            echo "<td>".$row['tgl_keluar']."</td>";
                            echo "<td>".ucwords($row['lokasi'])."</td>";
                            echo "<td>".ucwords($row['periode'])."</td>";
                            echo "<td>Rp ".monefy($row['total'], false)."</td>";
                            echo "<td>";
                            if($row['is_acc'] == 1){
                                # Menunggu
                                echo "<label class='badge badge-secondary'>Menunggu</label>";
                            }else if($row['is_acc'] == 2){
                                # Disetujui
                                echo "<label class='badge badge-success'>Disetujui</label>";
                                if(!empty($row['is_paid_remark'])){
                                    echo "<br><label class='badge badge-light'>".$row['is_acc_remark']."</label>";
                                }
                            }else{
                                # Ditolak
                                echo "<label class='badge badge-danger'>Ditolak</label>";
                            }
                            echo "</td>";
                            echo "<td><label class='badge badge-light'>".$row['input']."</label><br><label class='badge badge-light'>".$row['timestamp']."</label></td>";
                            echo "</tr>";
                        }

                    }
				?>
            </tbody>
        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data barangkeluar</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('barangkeluar/submit_form')?>">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Formulir Data Barang Keluar</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                    <input type="hidden" name="id" value="">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Lokasi <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_lokasi" class="form-control" required="" id="form_lokasi">
                                    <option value="">- Pilih Salah Satu -</option>
                                    <?php
                                    foreach((array)$lokasi as $row){
                                        echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Periode <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_periode" class="form-control" required="" id="form_periode">
                                    <option value="">- Pilih Salah Satu -</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Tanggal Keluar <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="tgl_keluar" class="form-control datepicker" autocomplete="off" required="">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" id="modal_pelunasan" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Data Pembayaran Tempo</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-layout form-layout-4">
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Tanggal Pelunasan <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="tgl_pelunasan" class="form-control datepicker" id="tgl_pelunasan" autocomplete="off" required="" placeholder="Masukkan tanggal pelunasan" readonly="">
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Rek. Pengirim <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control select2" name="rek_pengirim" required="" width="100%" readonly="">
                                <option value="">- Pilih Salah Satu -</option>
                                <?php 
                                        if(!empty($group_bank)){
                                            foreach($group_bank as $row){
                                                echo "<optgroup label='".$row['bank']."'></optgroup>";
                                                foreach($bank as $row2){
                                                    if($row2['bank'] == $row['bank']){
                                                        echo '<option value="'.$row2['id'].'">'.$row2['nama'].' | '.$row2['no_rek'].'</option>';
                                                    }
                                                }
                                            }
                                        }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Rek. Tujuan <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select class="form-control select2" name="rek_tujuan" required="" width="100%" readonly="">
                                <option value="">- Pilih Salah Satu -</option>
                                <?php 
                                        if(!empty($group_bank)){
                                            foreach($group_bank as $row){
                                                echo "<optgroup label='".$row['bank']."'></optgroup>";
                                                foreach($bank as $row2){
                                                    if($row2['bank'] == $row['bank']){
                                                        echo '<option value="'.$row2['id'].'">'.$row2['nama'].' | '.$row2['no_rek'].'</option>';
                                                    }
                                                }
                                            }
                                        }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Total <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input type="text" class="form-control autonumeric" name="nominal" autocomplete="off" required="" readonly="">
                            </div>
                        </div>

                    </div>
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Catatan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <textarea name="catatan" class="form-control" placeholder="Masukkan catatan tambahan" readonly=""></textarea>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.theme.min.css">
<script src="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_periode').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar',
    });
    init_periode();
    $('[data-toggle="tooltip"]').tooltip();
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('#modal_form form').trigger('reset');
    $("#modal_form").modal('show');
    $('[name=url]').val("{{$_SERVER['QUERY_STRING']}}");
});

$('#filter_lokasi').change(function() {
    var id = $(this).val();
    if (id != 'all') {
        $.ajax({
            url: "<?= base_url('periode/get_periode_by_lokasi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#filter_periode').empty().append('<option value="all">All</option>');
                $.each(result, function(key, val) {
                    $('#filter_periode').append('<option value="' + val.id + '">' + val.nama + '</option>');
                });
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    } else {
        $('#filter_periode').empty().append('<option value="all">All</option>');
    }
});

$('#form_lokasi').change(function() {
    init_periode_form();
});

$(".datepicker").datepicker({
    todayHighlight: !0,
    dateFormat: 'yy-mm-dd',
    autoclose: !0,
});

function init_periode() {
    var id = $('#filter_lokasi').val();
    if (id != 'all') {
        $.ajax({
            url: "<?= base_url('periode/get_periode_by_lokasi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                var periode = "{{(!empty($_GET['periode'])) ? $_GET['periode'] : 'all'}}";
                $('#filter_periode').empty().append('<option value="all">All</option>');
                $.each(result, function(key, val) {
                    $('#filter_periode').append('<option value="' + val.id + '">' + val.nama + '</option>');
                });
                $('#filter_periode').val(periode).change();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    } else {
        $('#filter_periode').empty().append('<option value="all">All</option>');
    }
}

function init_periode_form() {
    var id = $('#form_lokasi').val();
    if (id != '') {
        $.ajax({
            url: "<?= base_url('periode/get_periode_by_lokasi')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#form_periode').empty().append('<option value="">- Pilih Salah Satu -</option>');
                $.each(result, function(key, val) {
                    $('#form_periode').append('<option value="' + val.id + '">' + val.nama + '</option>');
                });
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    } else {
        $('#form_periode').empty().append('<option value="">- Pilih Salah Satu -</option>');
    }
}

function set_periode(id) {
    $('#form_periode').val(id).change();
}

function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('barangkeluar/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#modal_form form').trigger('reset');
                $('[name=url]').val("{{$_SERVER['QUERY_STRING']}}");
                $('#modal_form [name=id]').val(result.id);
                $('#modal_form [name=id_lokasi]').val(result.id_lokasi).change();
                $('#modal_form [name=tgl_keluar]').val(result.tgl_keluar);
                $('#modal_form').modal('show');
                setTimeout(function() {
                    set_periode(result.id_periode)
                }, 1000);
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus(el) {
    var id = $(el).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data akan dihapus, lanjutkan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('barangkeluar/delete_data')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}

function view_pelunasan(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('pelunasan/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#modal_pelunasan form').trigger('reset');
                $('#modal_pelunasan [name=tgl_pelunasan]').val(result.tgl_pelunasan);
                $('#modal_pelunasan [name=rek_pengirim').val(result.rek_pengirim).change();
                $('#modal_pelunasan [name=rek_tujuan').val(result.rek_tujuan).change();
                $('#modal_pelunasan [name=nominal').val(result.nominal);
                $('#modal_pelunasan [name=catatan').val(result.catatan);
                $('#modal_pelunasan [name=nominal]').autoNumeric('destroy');
                $('#modal_pelunasan [name=nominal]').autoNumeric('init', {
                    'mDec': 0
                });
                $('#modal_pelunasan').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function set_periode(id) {
    $('#form_periode').val(id).change();
}
</script>
@end