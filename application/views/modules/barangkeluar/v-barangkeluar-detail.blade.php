@layout('commons/index')

@section('content')
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Detail Barang Keluar</h6>
    </div>
    <div class="card-body">
        <div id="accordion" class="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">
                <div class="card-header" role="tab" id="headingOne">
                    <h6 class="mg-b-0">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                            Informasi
                        </a>
                    </h6>
                </div><!-- card-header -->

                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                    <div class="card-block pd-20">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Lokasi</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= $barangkeluar['lokasi']?>" readonly>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Periode </label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= $barangkeluar['periode']?>" readonly>
                            </div>
                        </div>
                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Tanggal Keluar </label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" readonly="" value="<?= $barangkeluar['tgl_keluar']?>" readonly>
                            </div>
                        </div>

                    </div>
                </div>
            </div><!-- card -->
            <!-- ADD MORE CARD HERE -->
        </div><!-- accordion -->
        <div id="toolbar" class="mg-b-10">
            @if($barangkeluar['is_acc'] != 2)
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
            @endif
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_barangkeluar">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Nama Barang</th>
                    <th data-sortable="true">Kuantitas</th>
                    <th data-sortable="true">Satuan</th>
                    <th data-sortable="true">Catatan</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if($detail){
                        foreach($detail as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            if($barangkeluar['is_acc'] != 2){
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='right' title='Hapus data' onclick='delete_data(".$row['id'].")'><i class='icon-trash'></i></button>";        
                            }
                            echo "</td>";
                            echo "<td>".$row['barang']."</td>";
                            echo "<td>".monefy($row['kuantitas'], false)."</td>";
                            echo "<td>".$row['satuan']."</td>";
                            echo "<td>".$row['catatan']."</td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="false" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('barangkeluar/submit_detail')?>">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Tambah Barang Keluar</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id_outstock" value="{{$barangkeluar['id']}}">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Nama Barang <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select class="form-control select2" name="id_barang" id="id_barang" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($barang as $row)
                                    <option value="{{$row['id']}}" data-satuan="{{$row['satuan']}}">{{$row['barang'].' ('.$row['satuan'].')'}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mg-t-10 div-detail">
                            <label class="col-sm-4 form-control-label">Kuantitas <span class="tx-danger">*</span></label>
                            <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                                <input type="number" class="form-control" name="kuantitas" autocomplete="off" required="" min="0">
                                <div id="sisa_stok">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="satuan" readonly="">
                            </div>
                        </div>
                        <div class="row mg-t-10 div-detail">
                            <label class="col-sm-4 form-control-label">Catatan <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="catatan" autocomplete="off" value="">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end
@section('js')
<link href="<?= base_url()?>assets/plugins/select2/css/select2.min.css">
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script src="<?= base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_barangkeluar').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });

    $('.select2').select2();
    $('.div-detail').hide();
});

$('#tambah').click(function() {
    init_form();
    $("#modal_form").modal('show');
});

$('[name=kuantitas]').on('keyup', function() {
    cek_kuantitas();
});

$('#id_barang').on('change', function() {
    init_databarang();
});


$('#id_barang').change(function() {
    if ($(this).val() != "") {
        var satuan = $(this).find(':selected').data().satuan;
    } else {
        var satuan = "";
    }
    $('[name=satuan]').val(satuan);
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function cek_kuantitas() {
    if (parseInt($('[name=kuantitas').val()) > parseInt($('[name=kuantitas').attr('max'))) {
        $('[name=kuantitas]').addClass('is-invalid');
    } else {
        $('[name=kuantitas]').removeClass('is-invalid');
    }
}

function delete_data(id) {
    if (id != '') {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data akan dihapus, lanjutkan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('barangkeluar/delete_detail')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}


function init_databarang() {
    if ($('#id_barang').val() != '') {
        $('[name=kuantitas]').val('');
        $('[name=kuantitas]').removeClass('is-invalid');
        $.ajax({
            url: "<?= base_url('barangkeluar/json_get_available_stok')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': $('#id_barang').val(),
            },
            success: function(result) {
                var el = '#id_barang';
                var satuan = $(el).find(':selected').data().satuan;
                $('[name=satuan]').val(satuan);
                $('[name=kuantitas]').attr('max', result);
                if(result > 0){
                    $('#sisa_stok').empty().append('<label class="badge badge-primary">Sisa stok : ' + result + '</label>');
                }else{
                    $('#sisa_stok').empty().append('<label class="badge badge-danger">Stok tidak tersedia</label>');
                }
                $('.div-detail').slideDown();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });

    } else {
        $('.div-detail').slideUp();
    }

}

function init_form() {
    $('#modal_form form').trigger('reset');
    $('[name=id_outstock]').val("{{$barangkeluar['id']}}");
}
</script>
@end