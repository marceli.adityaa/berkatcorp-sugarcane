@layout('commons/index')

@section('content')

<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('stok?')?>">
                    <div class="row">
                        <label class="col-sm-4 form-control-label">Barang</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="barang" class="form-control">
                                <option value="all">All</option>
                                @foreach ($barang as $row)
                                    @if($this->input->get('barang') == $row['id']){
                                        <option value="{{$row['id']}}" selected>{{$row['barang']}}</option>
                                    @else
                                        <option value="{{$row['id']}}">{{$row['barang']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai Entry</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="start" class="form-control datepicker" autocomplete="off" value="{{(!empty($_GET['start']) ? $_GET['start'] : '')}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="text" name="end" class="form-control datepicker" autocomplete="off" value="{{(!empty($_GET['end']) ? $_GET['end'] : '')}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control">
                                @foreach ($status as $row)
                                @if($this->input->get('status') == $row)
                                <option value="{{$row}}" selected>{{ucwords($row)}}</option>
                                @else
                                <option value="{{$row}}">{{ucwords($row)}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>

<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Stok</h6>
    </div>
    <div class="card-body">

        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
            <!-- <button class="btn btn-danger" id="kurang">- Kurangi Data</button> -->
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_periode">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Barang</th>
                    <th data-sortable="true">Tgl Entry</th>
                    <th data-sortable="true">Stok Awal</th>
                    <th data-sortable="true">Stok Sisa</th>
                    <th data-sortable="true">Satuan</th>
                    <th data-sortable="true">Harga Satuan</th>
                    <th data-sortable="true">Nilai Stok</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    $nilai_stok = 0;
                    if($stok){
                        foreach($stok as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                                echo "<button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='right' title='Detail' data-id='".$row['id']."' onclick='detail(this)'><i class='icon-eye'></i></button> ";
                            if($row['id_purchase_detail'] == 0){
                                echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='right' title='Edit' data-id='".$row['id']."' onclick='edit(this)'><i class='icon-note'></i></button> ";
                            }
                            echo "</td>";
                            if($row['stok_sisa'] == 0){
                                echo "<td>".$row['barang']."<br><label class='badge badge-danger'>Habis</label></td>";
                            }else{
                                echo "<td>".$row['barang']."</td>";
                            }
                            echo "<td>".$row['tgl_masuk']."<br><label class='badge badge-light'>By ".ucwords($row['input_by'])."</label></td>";
                            echo "<td>".monefy($row['stok_awal'], false)."</td>";
                            echo "<td>".monefy($row['stok_sisa'], false)."</td>";
                            echo "<td><label class='badge badge-light'>".$row['satuan']."</label></td>";
                            echo "<td>Rp ".monefy($row['harga'], false)."</td>";
                            $nilai_stok += $row['harga'] * $row['stok_sisa'];
                            echo "<td>Rp ".monefy($row['harga'] * $row['stok_sisa'], false)."</td>";
                            echo "</tr>";
                        }

                    }
				?>
            </tbody>
        </table>
        <h3 class="float-right"><small>Grandtotal Nilai Stok : Rp </small><b>{{monefy($nilai_stok, false)}}</b></h3>
        @else
        <p class="text-center">Klik filter untuk menampilkan data stok</p>
        @endif
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('stok/submit_form')?>" id="form_data">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Tambah Data</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                    <input type="hidden" name="id" value="">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Barang <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_barang" class="form-control" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($barang as $row)
                                    <option value="{{$row['id']}}" data-satuan="{{$row['satuan']}}">{{$row['barang'].' ('.$row['satuan'].')'}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Tanggal Entry <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="tgl_masuk" class="form-control datepicker" autocomplete="off" required="">
                            </div>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Stok Awal <span class="tx-danger">*</span></label>
                            <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control autonumeric" name="stok_awal" autocomplete="off" required="">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="satuan" readonly="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Harga Satuan <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" name="harga" class="form-control autonumeric" autocomplete="off" required="" value="">
                                </div>
                            </div>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Subtotal <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" name="subtotal" class="form-control autonumeric" autocomplete="off" required="" value="">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="modal_detail" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Detail Barang Keluar</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped mg-t-10 table-white" id="tabel_detail">
                    <thead>
                        <tr>
                            <th data-formatter="reformat_number" class="text-center">No.</th>
                            <th data-sortable="true">Lokasi</th>
                            <th data-sortable="true">Periode</th>
                            <th data-sortable="true">Tgl Keluar</th>
                            <th data-sortable="true">Kuantitas</th>
                            <th data-sortable="true">Subtotal</th>
                            <th data-sortable="true">Input by</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <h5 class="pull-right mg-t-10 tx-light">Total : <span id="total" class="tx-bold"></span></h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
            </div>
        </div>
    </div>
</div>
@end


@section('js')
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.theme.min.css">
<script src="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_periode').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar',
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('#modal_form form').trigger('reset');
    $("#modal_form").modal('show');
    init_form();
});

$('[name=stok_awal], [name=harga]').on('keyup', function() {
    sum_subtotal();
});

$('[name=subtotal]').on('keyup', function() {
    sum_harga();
});

$('[name=id_barang]').on('change', function() {
    if ($(this).val() != "") {
        var satuan = $(this).find(':selected').data().satuan;
    } else {
        var satuan = "";
    }
    $('[name=satuan]').val(satuan);
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function sum_subtotal() {
    var subtotal = parseInt($('[name=stok_awal]').autoNumeric('get')) * parseInt($('[name=harga]').autoNumeric('get'));
    if (subtotal >= 0) {
        $('[name=subtotal').val(numberWithCommas(subtotal));
    } else {
        $('[name=subtotal').val(0);
    }
}

function sum_harga() {
    var harga = parseInt($('[name=subtotal]').autoNumeric('get')) / parseInt($('[name=stok_awal]').autoNumeric('get'));
    if (harga >= 0) {
        $('[name=harga').val(numberWithCommas(harga));
    } else {
        $('[name=harga').val(0);
    }
}

$(".datepicker").datepicker({
    todayHighlight: !0,
    dateFormat: 'yy-mm-dd',
    autoclose: !0,
});

function init_form() {
    var url = "{{$_SERVER['QUERY_STRING']}}";
    $('[name=url]').val(url);
}

function edit(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('stok/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('#modal_form form').trigger('reset');
                init_form();
                $('#modal_form [name=id]').val(result.id);
                $('#modal_form [name=tgl_masuk]').val(result.tgl_masuk);
                $('#modal_form [name=id_barang]').val(result.id_barang).change();
                $('#modal_form [name=stok_awal]').val(result.stok_awal);
                $('#modal_form [name=harga]').val(result.harga);
                // $('#modal_form [name=satuan]').val(result.satuan);
                $('[name=stok_awal], [name=harga], [name=subtotal]').autoNumeric('destroy');
                $('[name=stok_awal], [name=harga], [name=subtotal]').autoNumeric('init', {
                    'mDec': 0
                });
                $('#modal_form').modal('show');
                sum_subtotal();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function hapus(el) {
    var id = $(el).data().id;
    if (id != '') {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data akan dihapus, lanjutkan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('pendapatan/delete_data')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id': id,
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })

    }
}

function detail(el) {
    var id = $(el).data().id;
    if (id != '') {
        $.ajax({
            url: "<?= base_url('stok/json_get_riwayat_stok')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                if(result != false){
                    $('#tabel_detail > tbody').empty();
                    var no = 1;
                    var total = 0;
                    $.each(result, function(key, val) {
                        total += parseInt(val.subtotal);
                        $("#tabel_detail").find('tbody').append('\
                            <tr><td class="text-center">' + no + '</td>\
                            <td>' + val.lokasi + '</td>\
                            <td>' + val.periode + '</td>\
                            <td>' + val.tgl_keluar + '</td>\
                            <td>' + monefy(val.kuantitas) + '</td>\
                            <td>Rp ' + monefy(val.subtotal) + '</td>\
                            <td>' + val.input + '</td>\
                            </tr>');
                        no++;
                    });
                    $('#total').empty().html('Rp ' + monefy(total));
                    $('#modal_detail').modal('show');
                }else{
                    swal.fire('Pesan', 'Tidak ada riwayat transaksi', 'info');
                }
                
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function monefy(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
</script>
@end