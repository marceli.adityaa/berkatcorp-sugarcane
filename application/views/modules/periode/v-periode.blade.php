@layout('commons/index')

@section('content')
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Periode - {{$lokasi['nama']}}</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Data</button>
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_periode">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-searchable="false">Lokasi</th>
                    <th data-sortable="true">Periode</th>
                    <th data-sortable="true">Deskripsi</th>
                    <th data-sortable="true">Tgl Mulai</th>
                    <th data-sortable="true">Tgl Berakhir</th>
                    <th data-sortable="true" data-searchable="false">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if($periode){
                        foreach($periode as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                            echo "<button type='button' class='btn btn-warning' data-toggle='tooltip' data-placement='right' title='Edit' onclick='edit(this)' data-id='".$row['id']."'><i class='icon-pencil'></i></button>";
                            if($row['selesai'] == 1){
                                echo " <button type='button' class='btn btn-primary' data-toggle='tooltip' data-placement='right' title='Set Aktif' onclick='nonaktif(this)' data-id='".$row['id']."'><i class='fa fa-play'></i></button>";
                            }else{
                                echo " <button type='button' class='btn btn-success' data-toggle='tooltip' data-placement='right' title='Set Selesai' onclick='aktif(this)' data-id='".$row['id']."'><i class='fa fa-check'></i></button>";
                            }
                            
                            echo "</td>";
                            echo "<td>".$row['lokasi']."</td>";
                            echo "<td>".$row['nama']."</td>";
                            echo "<td>".$row['deskripsi']."</td>";
                            echo "<td>".$row['tgl_mulai']."</td>";
                            echo "<td>".$row['tgl_berakhir']."</td>";
                            if($row['selesai'] == 1){
                                echo "<td><label class='badge badge-success'>Selesai</label></td>";
                            }else{
                                echo "<td><label class='badge badge-primary'>Aktif</label></td>";
                            }
                            echo "</tr>";
                        }

                    }
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('periode/submit_form')?>">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Formulir Data periode</h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="id_lokasi" value="{{$lokasi['id']}}">
                    <div class="form-layout form-layout-4">
                        <div class="row">
                            <label class="col-sm-4 form-control-label">Lokasi <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" autocomplete="off" required="" value="{{$lokasi['nama']}}" readonly="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Periode <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="nama" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Deskripsi <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" name="deskripsi" autocomplete="off">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Tgl Mulai <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control datepicker" name="tgl_mulai" autocomplete="off" required="">
                            </div>
                        </div>
                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Tgl Berakhir <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control datepicker" name="tgl_berakhir" autocomplete="off">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.theme.min.css">
<script src="<?= base_url()?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript">
var lokasi = "{{$lokasi['id']}}";
$(document).ready(function() {
    $('#tabel_periode').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('[data-toggle="tooltip"]').tooltip();
});
$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});
$('#tambah').click(function() {
    $('form').trigger('reset');
    $('[name=id_lokasi]').val(lokasi);
    $("#modal_form").modal('show');
});

$(".datepicker").datepicker({
    todayHighlight: !0,
    dateFormat: 'yy-mm-dd',
    autoclose: !0,
});


function edit(el) {
    var id = $(el).data().id;
    if (id !== '') {
        $.ajax({
            url: "<?= base_url('periode/json_get_detail')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                $('form').trigger('reset');
                $('[name=id]').val(result.id);
                $('[name=id_lokasi]').val(result.id_lokasi);
                $('[name=nama]').val(result.nama);
                $('[name=deskripsi]').val(result.deskripsi);
                $('[name=tgl_mulai]').val(result.tgl_mulai);
                $('[name=tgl_berakhir]').val(result.tgl_berakhir);
                $('#modal_form').modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function aktif(el) {
    var id = $(el).data().id;
    if (id !== '') {
        $.ajax({
            url: "<?= base_url('periode/set_aktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}

function nonaktif(el) {
    var id = $(el).data().id;
    console.log(id);
    if (id !== '') {
        $.ajax({
            url: "<?= base_url('periode/set_nonaktif')?>",
            type: "POST",
            dataType: "json",
            data: {
                'id': id,
            },
            success: function(result) {
                reload_page();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }
}
</script>
@end