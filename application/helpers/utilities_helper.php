<?php
if (!function_exists('monefy')) {
	function monefy($text = "", $is_decimal = TRUE)
	{
		if ($is_decimal) {
			return number_format($text, 2, '.', ',');
		}
		return number_format($text, 0, '.', ',');
	}
}

if (!function_exists('trim_array')) {
	function trim_array($data)
	{
		foreach ($data as $index => $item) {
			if (empty($item) && $item !== '0') {
				unset($data[$index]);
			}
		}
		return $data;
	}
}

if (!function_exists('zerofy')) {
	function zerofy($string, $digit = 2)
	{
		return str_pad($string, $digit, '0', STR_PAD_LEFT);
	}
}

if (!function_exists('datify')) {
	function datify($string, $format = 'd-m-Y')
	{
		return date($format, strtotime($string));
	}
}

if (!function_exists('unmonefy')) {
	function unmonefy($string)
	{
		$string = str_replace('.', '', $string);
		return str_replace(',', '.', $string);
	}
}

if (!function_exists('array_has_empty')) {
	function array_has_empty($array = array())
	{
		foreach ($array as $key => $value) {
			if (empty($value)) {
				return TRUE;
			}
		}
		return FALSE;
	}
}

if (!function_exists('uniqchar')) {
	function uniqchar($len = 4)
	{
		$char = 'abcdefghjklmnpqrstuvwxyz123456789';
		$max  = strlen($char) - 1;

		$string = '';
		for ($i = 0; $i < $len; $i++) {
			$string .= $char[mt_rand(0, $max)];
		}
		return $string;
	}
}

if (!function_exists('dump')) {
	function dump($array = array())
	{
		echo '<pre>';
		print_r($array);
		echo '</pre>';
		die();
	}
}

if (!function_exists('img_compress')) {
	function img_compress($source, $destination, $quality, $replace = FALSE)
	{

		$info = getimagesize($source);

		if ($info['mime'] == 'image/jpeg')
			$image = imagecreatefromjpeg($source);

		elseif ($info['mime'] == 'image/gif')
			$image = imagecreatefromgif($source);

		elseif ($info['mime'] == 'image/png')
			$image = imagecreatefrompng($source);

		imagejpeg($image, $destination, $quality);

		if ($replace) {
			unlink($source);
		}

		return $destination;
	}

	if (!function_exists('generate_save_url')) {
		function generate_save_url($url = '')
		{
			if (empty($url)) {
				return '';
			}

			$url = parse_url(strtolower(urldecode($url)));
			return isset($url['path']) ? prep_url($url['host'] . $url['path']) : prep_url($url['host']);
		}
	}

	if (!function_exists('authenticate_token')) {
		function authenticate_token($link = '')
		{
			$result = json_decode(file_get_contents($link));
			return $result->result;
		}
	}

	if (!function_exists('time_ago')) {
		function time_ago($datetime)
		{
			$since = time() - strtotime($datetime);
			$chunks = array(
				array(60 * 60 * 24, 'hari'),
				array(60 * 60, 'jam'),
				array(60, 'menit'),
				array(1, 'detik')
			);

			for ($i = 0; $i < count($chunks); $i++) {
				$seconds = $chunks[$i][0];
				$name = $chunks[$i][1];
				if (($count = floor($since / $seconds)) != 0) {
					break;
				}
			}

			$print = ($count <= 31) ? "$count {$name} lalu" : datify($datetime, 'd-m-Y, h:i');
			return $print;
		}
	}
}

if ( ! function_exists('setNewDateTime')){
    function setNewDateTime(){
      date_default_timezone_set("Asia/Jakarta");
      $date = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
      return $date->format('Y-m-d H:i:s');
    }
  }

if (!function_exists('captcha')) {
    function captcha($width = 320, $height = 40)
    {
        $CI = &get_instance();
        $prevCap = $CI->session->userdata('captcha');

        if (!empty($prevCap)) {
            if (file_exists('assets/captcha/' . $prevCap['filename'])) {
                unlink('assets/captcha/' . $prevCap['filename']);
            }

            $CI->session->unset_userdata('captcha');
        }

        $CI->load->helper('captcha');
        $vals = array(
            'word' => rand(1001, 9999),
            'img_path' => './assets/captcha/',
            'img_url' => base_url() . 'assets/captcha/',
            'img_width' => $width,
            'img_height' => $height,
            'expiration' => 7200,
            'font_size' => 16,
            'colors' => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 140, 140)),
        );

        $cap = create_captcha($vals);
        $CI->session->set_userdata('captcha', array('word' => $cap['word'], 'filename' => $cap['filename']));

        return $cap;
    }
}

if (!function_exists('verifikasi_captcha')) {
    function verifikasi_captcha($captcha)
    {
        $CI = &get_instance();
        if ($captcha == $CI->session->userdata('captcha')['word']) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('flashdata')) {
    function flashdata($tipe, $message)
    {
        #tipe pesan
        #1. info
        #2. warning
        #3. danger
        #4. success
        $CI = &get_instance();
        $CI->session->set_flashdata($tipe, $message);
    }
}

if (!function_exists('set_session')) {
    function set_session($name, $value)
    {
        $CI = &get_instance();
        $CI->session->set_userdata($name, $value);
    }
}

if (!function_exists('get_session')) {
    function get_session($name)
    {
        $CI = &get_instance();
        return $CI->session->userdata($name);
    }
}

if (!function_exists('set_activemenu')) {
    function set_activemenu($submenu = '', $menu = '')
    {
        $CI = &get_instance();
        $CI->session->set_userdata('submenu', $submenu);
        $CI->session->set_userdata('menu', $menu);
    }
}

if (!function_exists('secure')) {
    function secure($input)
    {
        $input = trim($input);
        $input = htmlentities($input);
        return $input;
    }
}

if (!function_exists('multisecure')) {
    function multisecure($array)
    {
        foreach ($array as $key => $value) {
            $array[$key] = secure($value);
        }
        return $array;
    }
}
